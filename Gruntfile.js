/*global console, module */

module.exports = function (grunt) {
    'use strict';
    
    const dir = grunt.option('dir'),
     dialects = grunt.file.readJSON('dialects.json'),
         root = (~grunt.cli.tasks.indexOf('app-debug') ||
                 ~grunt.cli.tasks.indexOf('app-release')) ? '/android_asset/www/' : '/',

        // Source paths to CDN-hosted fonts
        Fonts = {
            FontAwesome: {
                use: true,
                version: '4.4.0',
                formats: {
                    woff2: '//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/fonts/fontawesome-webfont.woff2',
                    woff: '//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/fonts/fontawesome-webfont.woff',
                    ttf: '//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/fonts/fontawesome-webfont.ttf'
                }
            },

            Gentium: {
                use: true,
                formats: {
                    woff2: '//fonts.gstatic.com/s/gentiumbasic/v7/KCktj43blvLkhOTolFn-MQgYcthoNQJTwaSsmU2sQE0.woff2',
                    woff: '//fonts.gstatic.com/s/gentiumbasic/v7/KCktj43blvLkhOTolFn-MU3vq9dAc3DuCNWjMJNKvGE.woff'
                }
            }
        },

        // Source paths to CDN-hosted library scripts
        Libs = {
            hammer: {
                use: false,
                version: '2.0.8',
                global: 'Hammer',
                cdn: '//cdn.jsdelivr.net/hammerjs/2.0.8/hammer.min.js',
                hash: 'sha384-Cs3dgUx6+jDxxuqHvVH8Onpyj2LF1gKZurLDlhqzuJmUqVYMJ0THTWpxK5Z086Zm',
                cdn2: '//cdnjs.cloudflare.com/ajax/libs/hammer.js/2.0.8/hammer.min.js',
                hash2: 'sha384-Cs3dgUx6+jDxxuqHvVH8Onpyj2LF1gKZurLDlhqzuJmUqVYMJ0THTWpxK5Z086Zm'
            },

            howler: {
                use: false,
                version: '1.1.28',
                global: 'Howler',
                cdn: '//cdn.jsdelivr.net/howler.js/1.1.28/howler.min.js',
                hash: 'sha384-dkbNXoWkpNJaPNxA+iLjBKBHjbNifvb6HDWXg0urjZ+Vf1sfMtrlEToP1+sZQvVg',
                cdn2: '//cdnjs.cloudflare.com/ajax/libs/howler/1.1.28/howler.min.js',
                hash2: 'sha384-dkbNXoWkpNJaPNxA+iLjBKBHjbNifvb6HDWXg0urjZ+Vf1sfMtrlEToP1+sZQvVg'
            },

            jquery: {
                use: true,
                version: '3.1.1',
                global: 'jQuery',
                cdn: '//ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js',
                hash: 'sha384-3ceskX3iaEnIogmQchP8opvBy3Mi7Ce34nWjpBIwVTHfGYWQS9jwHDVRnpKKHJg7',
                cdn2: '//cdn.jsdelivr.net/jquery/3.1.1/jquery.min.js',
                hash2: 'sha384-3ceskX3iaEnIogmQchP8opvBy3Mi7Ce34nWjpBIwVTHfGYWQS9jwHDVRnpKKHJg7'
            },

            'jquery.panzoom': {
                use: false,
                version: '3.2.2',
                global: 'jQuery.Panzoom',
                cdn: '//cdnjs.cloudflare.com/ajax/libs/jquery.panzoom/3.2.2/jquery.panzoom.min.js',
                hash: 'sha384-Dg5o5NeBLb1HXWJM5g+IzqLaVRXRr87Uc8oTro/hrWZ+7uIk8OoYbTKmH4kFij/a'
            },

            moment: {
                use: true,
                version: '2.17.0',
                global: 'moment',
                cdn: '//cdn.jsdelivr.net/momentjs/2.17.0/moment.min.js',
                hash: 'sha384-i7jiWeRVvJOmjgipKoheFNbbt6AXocX+BuVYuHScgQQWItQRZ66GzcGyJlQHYZK7',
                cdn2: '//cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.0/moment.min.js',
                hash2: 'sha384-i7jiWeRVvJOmjgipKoheFNbbt6AXocX+BuVYuHScgQQWItQRZ66GzcGyJlQHYZK7'
            },

            slick: {
                use: true,
                version: '1.5.5',
                global: 'jQuery.fn.slick',
                cdn: '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js',
                hash: 'sha384-ZULtytbCZdmL8PeKalcAKnseGOqrCiPBi3DiB7s4JJmS8gjSbfw0w8SPKpt9WemG',
                cdn2: '//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js',
                hash2: 'sha384-ZULtytbCZdmL8PeKalcAKnseGOqrCiPBi3DiB7s4JJmS8gjSbfw0w8SPKpt9WemG'
            },

            velocity: {
                use: false,
                version: '1.3.2',
                global: 'jQuery.Velocity',
                cdn: '//cdn.jsdelivr.net/velocity/1.3.2/velocity.min.js',
                hash: 'sha384-21By3Gp2UaTMW8pu7FcTDuQUwfJdeOLf1WcWaZ+Q8EHANpokjnq4GH7cvaCt0E6F',
                cdn2: '//cdnjs.cloudflare.com/ajax/libs/velocity/1.3.2/velocity.min.js',
                hash2: 'sha384-37dgu/TPfHpC3cLj2D8LaslDZz9638h/9uH2uzUjjtSYJZ7enXtmhssdFoa4pQlX'
            },

            'velocity.ui': {
                use: false,
                version: '5.1.2',
                global: 'jQuery.Velocity.RegisterEffect',
                cdn: '//cdn.jsdelivr.net/velocity/1.3.2/velocity.ui.min.js',
                hash: 'sha384-vNxA+b6yW/DSrAmZ3+d98a1cOeVooQVBNsCF1+7UnyVdbeIDkHKAOID24NyldBNG'
            }
        };
    
    if (!dir) {
        console.log('Build dir not specified!');
        return;
    }
    
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        
        // Converts story sections from markdown to HTML
        markdown: {
            all: {
                files: [{
                    expand: true,
                    cwd: dir,
                    src: 'stories/**/*.md',
                    dest: dir,
                    ext: '.html'
                }],
                options: {
                    template: 'section.jst',
                    
                    // Wraps regional and advanced words in span tags
                    // Runs for every file that matches src above
                    preCompile: function (src, context) {
                        var word, american, british, americanRegex,
                            britishRegex, americanisedSrc, taggedSrc,
                            style = /<style>[\s\S]*?<\/style>/m.exec(src),
                            script = /<script>[\s\S]*?<\/script>/m.exec(src),
                            suffixes = '(able|al|ance|ant|bound|d|dom|ed|en|ence|ency|ent|eous|er|ern|ery|ess|fare|ful|fully|fy|hood|ier|ify|ile|ing|ion|ish|ism|ist|ite|ity|ive|land|le|less|like|ling|ly|man|ment|n|ness|nce|ncy|nt|or|ous|phone|rn|ry|s|s’|’s|scope|sh|ship|some|ss|ster|t|th|ty|ulent|ward|wise)?';
                        
                        // To avoid the risk of altering styles and scripts, remove them from
                        // src now, store them separately then concatentate them back later
                        style = ((style && style.length) ? style[0] : '');
                        script = ((script && script.length) ? script[0] : '');
                        style && (src = src.split(style).join(''));
                        script && (src = src.split(script).join(''));

                        // Word substitutions are stored in arrays inside the dialects array.
                        // Search for each American word in src and wrap it a span tag that
                        // includes the British substitution stored in a data attribute.
                        for (word in dialects) {
                            american = dialects[word][0];
                            british = dialects[word][1];
                            americanRegex = new RegExp(american + suffixes, 'ig');
                            britishRegex = new RegExp(british + suffixes, 'ig');

                            // Check src for British words and replace with American versions
                            americanisedSrc = src.replace(britishRegex, american);
                            if (americanisedSrc !== src) {
                                src = americanisedSrc;
                                console.log('WARNING: Source prose should be written in American English');
                                console.log(`Changed '${british}' (British) to '${american}' (American)`);
                            }
                            // Tag American words with their British versions
                            taggedSrc = src.replace(americanRegex, `<span class="dialect" data-us="${american}$1" data-gb="${british}$1">${american}$1</span>`);
                            if (taggedSrc !== src) {
                                src = taggedSrc;
                                console.log(`American: ${american}, British: ${british}`);
                            }
                        }
                        return src + style + script;
                    },
                    //postCompile: function (src, context) {},
                    markdownOptions: {
                        gfm: false
                    }
                }
            }
        },
        
        'string-replace': {
            // Changes references to .md files to .html
            markdown: {
                files: [{
                    expand: true,
                    cwd: dir,
                    dest: dir,
                    src: ['scripts/story.js']
                }],
                options: {
                    replacements: [{
                        pattern: '${st.Env.currentPath}sections/${section}.md', 
                        replacement: '${st.Env.currentPath}sections/${section}.html'
                    }]
                }
            },
            
            decisions: {
                files: [{
                    expand: true,
                    cwd: dir,
                    dest: dir,
                    src: [
                        'stories/*/*.html',
                        'stories/*/decisions/*.html'
                    ]
                }],
                options: {
                    replacements: [{
                        pattern: /{{(.*?)}}/ig,
                        replacement: function (match, option) {
                            var spanId, colonIndex = option.indexOf(':');
                            if (colonIndex > 0) {
                                spanId = option.substring(colonIndex + 1, option.length);
                                option = option.substring(0, colonIndex);
                            } else {
                                spanId = option.replace(' ', '-');
                            }
                            spanId = spanId.toLowerCase();
                            return `<span id="${spanId}" class="option">${option}</span>`;
                        }
                    }]
                }
            },

            // Removes code used only in web build and sets root path
            app: {
                files: [{
                    expand: true,
                    cwd: dir,
                    dest: dir,
                    src: [
                        'styles/*.css',
                        'scripts/data/*.js',
                        'scripts/*.js',
                        '**/*.html'
                    ]
                }],
                options: {
                    replacements: [{
                        pattern: /<!--WEB_BEGIN-->[\s\S]*?<!--WEB_END-->/gm,
                        replacement: ''
                    }, {
                        pattern: /\/\*WEB_BEGIN\*\/[\s\S]*?\/\*WEB_END\*\//gm,
                        replacement: ''
                    }, {
                        pattern: /\/\/WEB_BEGIN[\s\S]*?\/\/WEB_END/gm,
                        replacement: ''
                    }, {
                        pattern: /.*?\/\/WEB/g,
                        replacement: ''
                    }, {
                        pattern: /.*?\/\*WEB\*\//g,
                        replacement: ''
                    }, {
                        pattern: /href="\//g,
                        replacement: 'href="' + root
                    }, {
                        pattern: /src="\//g,
                        replacement: 'src="' + root
                    }, {
                        pattern: /url\(\//g,
                        replacement: 'url(' + root
                    }, {
                        pattern: /\$\.get\(`\//g,
                        replacement: '$.get(`' + root
                    }, {
                        pattern: /\$\.get\(\'\//g,
                        replacement: '$.get(\'' + root
                    }, {
                        pattern: /\$\.cachedScript\(`\//g,
                        replacement: '$.cachedScript(`' + root
                    }, {
                        pattern: /\$\.cachedScript\(\'\//g,
                        replacement: '$.cachedScript(\'' + root
                    }, {
                        pattern: 'currentPath:  \'/\',',
                        replacement: 'currentPath:  \'/android_asset/www/\','
                    }]
                }
            },

            // Adds cordova.js reference to index.html and removes web code
            'app-index': {
                files: [{
                    expand: true,
                    cwd: dir,
                    dest: dir,
                    src: ['index.html'],
                }],
                options: {
                    replacements: [{
                        pattern: '<!--cordova.js-->',
                        replacement: '<script src="cordova.js"></script>'
                    }, {
                        pattern: /.*?<!--WEB-->/g,
                        replacement: ''
                    }, {
                        pattern: 'body class="init"',
                        replacement: 'body'
                    }]
                }
            },

            // Changes @font-face sources to use CDN
            'cdn-font': {
                files: [{
                    expand: true,
                    cwd: dir,
                    dest: dir,
                    src: [
                        'index.html',
                        'styles/fonts.css'
                    ]
                }],
                options: {
                    replacements: [{
                        pattern: `font-src 'self'`,
                        replacement: `font-src 'self' https://fonts.gstatic.com https://maxcdn.bootstrapcdn.com`
                    }, {
                        pattern: /font-family:\s(.+);\s*?src:\s/igm,
                        replacement: function (match, name) {
                            const font = Fonts[name] || null;
                            let format, formats, replacement = match;
                            if (font && font.use && font.formats) {
                                formats = Object.keys(font.formats);
                                replacement += `local(${name}), `;
                                for (format in formats) {
                                    format = formats[format];
                                    console.log(`Font: ${name}, format: ${format}`);
                                    replacement += `url('https:${font.formats[format]}') format('${format}'), `;
                                }
                            }
                            return replacement;
                        }
                    }]
                }
            },

            // Changes script library sources to use CDN
            'cdn-lib': {
                files: [{
                    expand: true,
                    cwd: dir,
                    dest: dir,
                    src: ['index.html']
                }],
                options: {
                    replacements: [{
                        pattern: `script-src 'self'`,
                        replacement: `script-src 'self' https://ajax.googleapis.com https://cdn.jsdelivr.net https://cdnjs.cloudflare.com https://glosbe.com 'unsafe-eval'`
                    }, {
                        pattern: /<link class="custom" rel="subresource" href="\/scripts\/lib\/(.+)\.min\.js">/ig,
                        replacement: function (match, name) {
                            const library = Libs[name] || null;
                            let replacement;
                            if (library && library.use) {
                                replacement = `<link class="custom" rel="subresource" href="https:${library.cdn}">`;
                            }
                            return replacement || match;
                        }
                    }, {
                        pattern: /(<script src="\/scripts\/lib\/(.+)\.min\.js">)<\/script>/ig,
                        replacement: function (match, script, name) {
                            const library = Libs[name] || null;
                            let replacement; 
                            if (library && library.use) {
                                replacement = `<script src="https:${library.cdn}" integrity="${library.hash}" crossorigin="anonymous"></script><script> window.${library.global} || document.write('${script}<\\/script>') </script>`;
                            }
                            return replacement || match;
                        }
                    }]
                }
            },

            // Removes code used in app build
            web: {
                files: [{
                    expand: true,
                    cwd: dir,
                    dest: dir,
                    src: [
                        'styles/*.css',
                        'scripts/data/*.js',
                        'scripts/*.js',
                        '**/*.html'
                    ],
                }],
                options: {
                    replacements: [{
                        pattern: /<!--APP_BEGIN-->[\s\S]*?<!--APP_END-->/gm,
                        replacement: ''
                    }, {
                        pattern: /\/\*APP_BEGIN\*\/[\s\S]*?\/\*APP_END\*\//gm,
                        replacement: ''
                    }, {
                        pattern: /\/\/APP_BEGIN[\s\S]*?\/\/APP_END/gm,
                        replacement: ''
                    }, {
                        pattern: /.*?\/\/APP/g,
                        replacement: ''
                    }, {
                        pattern: /.*?\/\*APP\*\//g,
                        replacement: ''
                    }]
                }
            },

            // Removes comments and code used in release build
            debug: {
                files: [{
                    expand: true,
                    cwd: dir,
                    dest: dir,
                    src: [
                        'styles/*.css',
                        'scripts/data/*.js',
                        'scripts/*.js',
                        '**/*.html'
                    ]
                }],
                options: {
                    replacements: [{
                        pattern: /<!--RELEASE_BEGIN-->[\s\S]*?<!--RELEASE_END-->/gm,
                        replacement: ''
                    }, {
                        pattern: /\/\*RELEASE_BEGIN\*\/[\s\S]*?\/\*RELEASE_END\*\//gm,
                        replacement: ''
                    }, {
                        pattern: /\/\/RELEASE_BEGIN[\s\S]*?\/\/RELEASE_END/gm,
                        replacement: ''
                    }, {
                        pattern: /.*?\/\/RELEASE/g,
                        replacement: ''
                    }, {
                        pattern: /.*?\/\*RELEASE\*\//g,
                        replacement: ''
                    }/*, {
                        pattern: /st\.debug\(/g,
                        replacement: '//st.debug('
                    }*/]
                }
            },

            // Removes comments and code used in debug build
            release: {
                files: [{
                    expand: true,
                    cwd: dir,
                    dest: dir,
                    src: [
                        'styles/*.css',
                        '!styles/debug.css',
                        'scripts/data/*.js',
                        'scripts/*.js',
                        '**/*.html'
                    ]
                }],
                options: {
                    replacements: [{
                        pattern: '<!--st.critical.min.css-->',
                        replacement: `<link rel="stylesheet" href="${root}styles/st.critical.min.css">`
                    }, {
                        pattern: '<!--st.critical.min.js-->',
                        replacement: `<script src="${root}scripts/st.critical.min.js"></script>`
                    }, {
                        pattern: /<!--DEBUG_BEGIN-->[\s\S]*?<!--DEBUG_END-->/gm,
                        replacement: ''
                    }, {
                        pattern: /\/\*DEBUG_BEGIN\*\/[\s\S]*?\/\*DEBUG_END\*\//gm,
                        replacement: ''
                    }, {
                        pattern: /\/\/DEBUG_BEGIN[\s\S]*?\/\/DEBUG_END/gm,
                        replacement: ''
                    }, {
                        pattern: /.*?\/\/DEBUG/g,
                        replacement: ''
                    }, {
                        pattern: /.*?\/\*DEBUG\*\//g,
                        replacement: ''
                    }, {
                        pattern: /st\.debug\([\s\S]*?\);/gm,
                        replacement: ''
                    }, {
                        pattern: /st\.initDebug[\s\S]*?\);/gm,
                        replacement: ''
                    }, {
                        pattern: /#debug-.*$/gm,
                        replacement: ''
                    }]
                }
            },

            // Removes newlines created by transpiling template strings to ES5
            newlines: {
                files: [{
                    expand: true,
                    cwd: dir,
                    dest: dir,
                    src: [
                        'scripts/data/*.js',
                        'scripts/*.js'
                    ],
                }],
                options: {
                    replacements: [{
                        pattern: /\\n\s*/gm,
                        replacement: ''
                    }]
                }
            }
        },

        // Adds CSS prefixes needed by older browsers
        autoprefixer: {
            app: {
                src: dir + 'styles/*.css',
                options: {
                    browsers: ['Chrome >= 24']
                }
            },
            web: {
                src: dir + 'styles/*.css',
                options: {
                    browsers: ['> 5%', 'last 2 versions']
                }
            }
        },
        
        // Lints JS files before transpiling to ES5
        jshint: {
            all: {
                files: {
                    src: [
                        dir + 'scripts/*.js',
                        dir + 'scripts/data/*.js'
                    ]
                },
                options: {
                    jshintrc: true
                }
            }
        },

        // Lints prefixed CSS files before minifying
        csslint : {
            all : {
                src: [dir + 'styles/*.css'],
                options: {
                    csslintrc: '.csslintrc'
                },
            }
        },

        // The three next tasks below all squish down resources
        'json-minify': {
            release: {
                files: dir + '**/*.json'
            }
        },
        
        cssmin: {
            'release-critical': {
                dest: dir + 'styles/st.critical.min.css',
                src: [
                    dir + 'styles/fonts.css',
                    dir + 'styles/misc.css',
                    dir + 'styles/header.css',
                    dir + 'styles/sky.css',
                    dir + 'styles/flow.css',
                    dir + 'styles/view.css',
                    dir + 'styles/catalog.css',
                    dir + 'styles/ribbon.css',
                    dir + 'styles/slider.css',
                    dir + 'styles/story.css',
                    dir + 'styles/fa.css'
                ]
            },
            'release-lazy': {
                dest: dir + 'styles/st.lazy.min.css',
                src: [
                    dir + 'styles/mascot.css',
                    dir + 'styles/modal.css',
                    dir + 'styles/menu.css',
                    dir + 'styles/message.css',
                    dir + 'styles/glass.css',
                    dir + 'styles/anim.css'
                ]
            }
        },
        
        htmlmin: {
            release: {
                files: [{
                    expand: true,
                    cwd: dir,
                    dest: dir,
                    src: ['**/*.html']
                }],
                options: {
                    collapseWhitespace: true,
                    minifyCSS: true,
                    minifyJS: true,
                    removeAttributeQuotes: true,
                    removeComments: true,
                    removeRedundantAttributes: true
                }
            }
        },
        
        babel: {
            debug: {
                files: [{
                    expand: true,
                    cwd: dir,
                    dest: dir,
                    src: [
                        'scripts/data/*.js',
                        'scripts/*.js'
                    ]
                }],
                options: {
                    compact: false,
                    loose: 'all',
                    sourceMaps: 'inline'
                }
            },

            release: {
                files: [{
                    expand: true,
                    cwd: dir,
                    dest: dir,
                    src: [
                        'scripts/data/*.js',
                        'scripts/*.js',
                        '!scripts/debug.js'
                    ]
                }],
                options: {
                    comments: false,
                    compact: true,
                    loose: 'all'
                }
            }
        },

        // Combines and minifies StoryTime JS files
        uglify: {
            'release-critical': {
                dest: dir + 'scripts/st.critical.min.js',
                src: [
                    dir + 'scripts/data/cache.js',
                    dir + 'scripts/data/catalog.js',
                    dir + 'scripts/data/msgs.js',
                    dir + 'scripts/catalog.js',
                    dir + 'scripts/common.js',
                    dir + 'scripts/data.js',
                    dir + 'scripts/glass.js',
                    dir + 'scripts/init.js',
                    dir + 'scripts/listen.js',
                    dir + 'scripts/prefs.js',
                    dir + 'scripts/sky.js',
                    dir + 'scripts/sync.js',
                    dir + 'scripts/story.js',
                    dir + 'scripts/util.js',
                    dir + 'scripts/view.js',
                    dir + 'scripts/last.js'
                ],
                options: {
                    mangle: true,
                    preserveComments: false,
                    enclose: true,
                    compress: {
                        drop_console: false
                    }
                }
            },

            'release-lazy': {
                dest: dir + 'scripts/st.lazy.min.js',
                src: [
                    dir + 'scripts/data/modals.js',
                    dir + 'scripts/define.js',
                    dir + 'scripts/mascot.js',
                    dir + 'scripts/message.js',
                    dir + 'scripts/modal.js',
                    dir + 'scripts/profile.js'
                ],
                options: {
                    mangle: true,
                    preserveComments: false,
                    enclose: true,
                    compress: {
                        drop_console: false
                    }
                }
            }
        },
        
        // Gets rid of files not needed to run
        clean: {
            markdown: [dir + 'stories/**/*.md'],
            app: [
                dir + 'favicon.ico',
                dir + 'manifest.json',
                dir + '404.html',
                dir + 'noscript.html',
                dir + 'images/icons',
                dir + 'images/gears.svg',
                dir + 'sounds/*.ogg',
                dir + 'sounds/*.mp3',
                dir + 'stories/**/*.ogg',
                dir + 'stories/**/*.mp3'
            ],
            web: [
                dir + 'sounds/*.wav',
                dir + 'stories/**/*.wav'
            ],
            release: [
                dir + 'styles/*.css',
                dir + 'scripts/*.js',
                dir + 'scripts/data',
                `!${dir}styles/st.critical.min.css`,
                `!${dir}styles/st.lazy.min.css`,
                `!${dir}scripts/st.critical.min.js`,
                `!${dir}scripts/st.lazy.min.js`
            ]
        }
    });
    
    grunt.loadNpmTasks('grunt-markdown');
    grunt.loadNpmTasks('grunt-string-replace');
    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-csslint');
    grunt.loadNpmTasks('grunt-babel');
    grunt.loadNpmTasks('grunt-json-minify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-clean');
    
    grunt.registerTask('app-debug', [
        'markdown',
        'string-replace:markdown',
        'string-replace:decisions',
        'string-replace:app-index',
        'string-replace:app',
        'string-replace:debug',
        'autoprefixer:app',
        'jshint',
        'csslint',
        'babel:debug',
        'string-replace:newlines',
        'clean:markdown',
        'clean:app'
    ]);
    
    grunt.registerTask('app-release', [
        'markdown',
        'string-replace:markdown',
        'string-replace:decisions',
        'string-replace:app-index',
        'string-replace:app',
        'string-replace:release',
        'autoprefixer:app',
        'jshint',
        'csslint',
        'json-minify',
        'cssmin:release-critical',
        'cssmin:release-lazy',
        'htmlmin',
        'babel:release',
        'string-replace:newlines',
        'uglify:release-critical',
        'uglify:release-lazy',
        'clean:markdown',
        'clean:app',
        'clean:release'
    ]);
    
    grunt.registerTask('web-debug', [
        'markdown',
        'string-replace:markdown',
        'string-replace:decisions',
        'string-replace:web',
        'string-replace:cdn-font',
        'string-replace:cdn-lib',
        'string-replace:debug',
        'autoprefixer:web',
        'jshint',
        'csslint',
        'babel:debug',
        'string-replace:newlines',
        'clean:markdown',
        'clean:web'
    ]);
    
    grunt.registerTask('web-release', [
        'markdown',
        'string-replace:markdown',
        'string-replace:decisions',
        'string-replace:web',
        'string-replace:cdn-font',
        'string-replace:cdn-lib',
        'string-replace:release',
        'autoprefixer:web',
        'jshint',
        'csslint',
        'json-minify',
        'cssmin:release-critical',
        'cssmin:release-lazy',
        'htmlmin',
        'babel:release',
        'string-replace:newlines',
        'uglify:release-critical',
        'uglify:release-lazy',
        'clean:markdown',
        'clean:web',
        'clean:release'
    ]);
};
