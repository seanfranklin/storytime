/*jshint -W079 */
'use strict';

const http = require('http'),
    fs = require('fs'),
    url = require('url'),
    moment = require('moment'),
    mongojs = require('mongojs'),
    nodemailer = require('nodemailer'),
    sendmail = require('nodemailer-sendmail-transport'),
    beautify = require('js-beautify').js_beautify,

    pages = {
        confirmed: fs.readFileSync('confirmed.html'),
        expired: fs.readFileSync('expired.html')
    },
    
    headers = {
        html: {
            'access-control-allow-origin': '*',
            'content-type': 'text/html'
        },
        
        json: {
            'access-control-allow-origin': '*',
            'content-type': 'application/json'
        },
        
        plain: {
            'access-control-allow-origin': '*',  // 'http://localhost',
            'content-type': 'text/plain'
        },
        
        options: {
            'access-control-allow-origin': '*',
            'access-control-allow-methods': 'GET,POST,OPTIONS',
            'access-control-allow-headers': 'content-type,accept,x-requested-with',
            'access-control-max-age': 10,  // Seconds
            'content-length': 0
        }
    },

    collections = ['Auth', 'Confirm', 'Errors', 'Feedback', 'Profile', 'Stats',
                   'User', 'Balloons', 'Biggalo', 'Mower', 'Queen', 'Zombie'],

    email = 'wordsandmagic@gmx.com',
    fqdn = 'storytime.happyforever.com',
    port = 60806,
    divider = '----------------------------------------------------',
    sessions = {},  // Holds cached tokens to avoid excess database queries
    database = mongojs('StoryTime', collections),
    transport = nodemailer.createTransport(sendmail({ path: '/usr/lib/sm.bin/sendmail' }));
    
let logDate = moment().format('YYYY-MM-DD'),
    logFile = fs.createWriteStream(`../log/sync_${logDate}.log`, { flags: 'w' });
    

/**
 *  Executes the first argument if it’s a function.
 */

function callback(fn, arg) {
    (typeof fn === 'function') && fn(arg);
}


/**
 *  Logs information to both console and file.
 */

function log(string, type = 'log') {
    const filePre = (type === 'log') ? '' : type.toUpperCase() + ': ';
    logFile.write(`${filePre}${string}\n`);
    console[type || 'log'](string);
}


/**
 *  Escapes and returns the passed string.
 */

function escapeRegExp(string) {
    return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, '\\$1');
}


/**
 *  Replaces characters in a string and returns it.
 */

function replaceAll(string, find, replace) {
    return string.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}


/**
 *  Strips characters from a string and returns it.
 */

function stripChars(string, chars) {
    for (let char in chars) {
        if (typeof chars[char] === 'string') {
            string = replaceAll(string, chars[char], '');
        }
    }
    return string;
}


/**
 *  Deletes tokens in sessions object that are more than 10 minutes old.
 */

function clearSessions() {
    //log('clearSessions()');
    const now = moment.utc();
    for (let token in sessions) {
        if (now.diff(sessions[token], 'seconds') > 600) {
            delete sessions[token];
        }
    }
}


/**
 *  Sets status to 'expired' on Confirm documents more than 24 hours old.
 */

function setExpired() {
    //log('setExpired()');
    const date = moment.utc().subtract(24, 'hours').format();
    updateData({
        query: { timestamp: { $lte: date }, status: 'sent' },
        data: { $set: { status: 'expired' } },
        options: { multi: true },
        collection: 'Confirm',
        skipLogging: true
    });
}


/**
 *  Maintain log file, sessions and the status of documents.
 */

function maintenance() {
    const date = moment().format('YYYY-MM-DD');
    if (date !== logDate) {
        logDate = date;
        logFile = fs.createWriteStream(`../log/sync_${logDate}.log`, { flags: 'w' });
    }
    clearSessions();
    if (Object.keys(sessions).length) {
        log(divider);
        log('Sessions: ' + JSON.stringify(sessions));
    }
    setExpired();
}


/**
 *  Looks for data in a database collection and returns it via callback if found.
 *  @param {object} query - Data object containing property values to match
 *  @param {string} collection - Database collection to look in
 *  @param {function} [fail] - Callback to run if find fails
 *  @param {function} [success] - Callback to run if find succeeds
 */

function findData(query, collection, fail, success) {
    log(`findData(${JSON.stringify(query)}, ${collection})`);

    database[collection].findOne(query, (error, foundData) => {
        if (error || !foundData) {
            log(`Data not found in ${collection} collection`, 'warn');
            callback(fail);
        } else {
            log(`Data found in ${collection} collection`);
            callback(success, foundData);
        }
    });
}


/**
 *  Looks for token in session array or Auth database collection.
 *  @param {string} token - Token to look for
 *  @param {function} [fail] - Callback to run if find fails
 *  @param {function} [success] - Callback to run if find succeeds
 */

function findToken(token, fail, success) {
    log(`findToken(${token})`);

    if(sessions[token]) {
        log('Token found in session array');
        callback(success);
    } else {
        findData({ _id: mongojs.ObjectId(token) }, 'Auth', () => {
            callback(fail);
        }, foundData => {
            if (foundData.status === 'confirmed') {
                sessions[token] = moment.utc();
                callback(success);
            } else {
                callback(fail);
            }
        });
    }
}


/**
 *  Saves data in new document in database collection and runs callback based on result.
 *  @param {object} data - Data to save in database collection
 *  @param {string} collection - Database collection to save data in
 *  @param {function} [fail] - Callback to run if save fails
 *  @param {function} [success] - Callback to run if save succeeds
 */

function saveData(data, collection, fail, success) {
    log(`saveData(data, ${collection})`);
    
    database[collection].save(data, (error, savedData) => {
        if (error || !savedData) {
            log(`Data not saved to ${collection} collection`, 'error');
            callback(fail);
        } else {
            log(`Data saved to ${collection} collection`);
            callback(success, savedData);
        }
    });
}


/**
 *  Saves data in a database collection and runs a callback based on result.
 *  @param {object} query - Data object describing document(s) to match
 *  @param {object} data - Data to save in database collection
 *  @param {object} [options] - Data object specifying options such as upsert and multi
 *  @param {string} collection - Database collection to save data in
 *  @param {function} [fail] - Callback to run if save fails
 *  @param {function} [success] - Callback to run if save succeeds
 */

function updateData({ query, data, options = { upsert: false, multi: false }, collection, fail, success, skipLogging }) {
    skipLogging || log(`updateData(${JSON.stringify(query)}, data, ${collection})`);
    
    database[collection].update(query, data, options, (error, updatedData) => {
        if (error || !updatedData) {
            log(`Data not updated in ${collection} collection`, 'error');
            callback(fail);
        } else {
            skipLogging || log(`Updated data in ${collection} collection`);
            callback(success, updatedData);
        }
    });
}


/**
 *  Send an email containing a validation link.
 *  @param {string} to - Email address of recipient
 *  @param {string} token - Validation token
 *  @param {function} [fail] - Callback to run if send fails
 *  @param {function} [success] - Callback to run if send succeeds
 */

function sendValidationEmail(to, token, fail, success) {
    log(`sendValidationEmail(${to}, ${token})`);
    const subject = 'StoryTime: Confirm Email Address',
        intro = 'Hi! To activate StoryTime’s sync capabilities, please confirm your email address using the link below.',
        link = `https://${fqdn}/sync/Confirm?token=${token}`,
        disclaimer = 'If this email is unexpected then it’s likely that some naughty person used your email address when trying to register for our service.',
        text = `${intro}\n\n${link}\n\n${disclaimer}`;
    
    sendEmail({ to, subject, text,
        fail: () => {
            updateData({
                query: { _id: mongojs.ObjectId(token) },
                data: { $set: { status: 'error' } },
                collection: 'Confirm'
            });
            callback(fail);
        },
        success: () => {
            updateData({
                query: { _id: mongojs.ObjectId(token) },
                data: { $set: { status: 'sent' } },
                collection: 'Confirm'
            });
            callback(success);
        }
    });
}


/**
 *  Sends an email using nodemailer sendmail transport.
 *  @param {string} [to] - Email address of recipient
 *  @param {string} [subject] - Email subject
 *  @param {string} [text] - Body of email (plain text or HTML)
 *  @param {function} [fail] - Callback to run if send fails
 *  @param {function} [success] - Callback to run if send succeeds
 */

function sendEmail({ to = email, subject = '', text = '', fail, success }) {
    log(`sendEmail(${to}, ${subject})`);

    if (validateEmail(to)) {
        transport.sendMail({ from: email, to, subject, text }, error => {
            if (error) {
                log('Failed to send email to ' + to, 'error');
                callback(fail);
            } else {
                log('Successfully sent email to ' + to);
                callback(success);
            }
        });
    } else {
        callback(fail);
    }
}


/**
 *  Validates an email address.
 *  @param {string} email
 *  @return {boolean} Email is valid
 */

function validateEmail(email) {
    const regex = /^(([^<>()\[\]\\.,;:\s@\"]+(\.[^<>()\[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/igm;
    return ((typeof email === 'string') && regex.test(email)) ? true : false;
}


// The server that services all incoming requests with an appropriate response
const server = http.createServer((request, response) => {
    const requestTime = moment.utc().format('YYYY-MM-DDThh:mm:ss'),
        requestUrl = url.parse(request.url, true),
        collection = requestUrl.pathname.substring(6),
        query = requestUrl.query;

    const service = {
        OPTIONS() {
            log('service.OPTIONS()');
            response.writeHead(204, 'No Content', headers.options);
            response.end();
            log('Sent response: 204 No Content');
        },

        GET(databaseQuery = { _id: query.token }, serviceCallback = null) {
            log(`service.GET(${JSON.stringify(databaseQuery)})`);
            findData(databaseQuery, collection, send404, foundData => {
                if (query.timestamp) {
                    const tsDiff = moment(query.timestamp).diff(moment(foundData.updated), 'seconds');
                    log(`Timestamp difference: ${tsDiff} seconds`);
                    /* If collection data newer then send to client
                    if (moment(query.timestamp).diff(foundData.updated) < 0) {
                        delete foundData._id;
                        sendResponse(200, JSON.stringify(foundData));
                    } else { send404(); } */
                }
                if (serviceCallback) {
                    callback(serviceCallback, foundData);
                } else {
                    delete foundData._id;
                    sendResponse(200, JSON.stringify(foundData));
                }
            });
        },

        // The databaseQuery argument is not used in a POST request
        POST(databaseQuery, serviceCallback) {
            log(`service.POST(${JSON.stringify(databaseQuery)})`);
            let receivedData = '';

            // Request emits multiple events, each with string chunk
            request.addListener('data', chunk => {
                receivedData += chunk;
            });

            // Listen for when all data is received
            request.addListener('end', () => {
                log(`Received ${collection} data from client`);
                receivedData = JSON.parse(receivedData);
                receivedData._id = query.token;
                
                saveData(receivedData, collection, send500, savedData => {
                    if (serviceCallback) {
                        callback(serviceCallback, savedData);
                    } else {
                        send200();
                    }
                });
            });
        }
    };


    // Calls the function corresponding to the request method
    function serviceRequest(databaseQuery, serviceCallback) {
        if (typeof service[request.method] === 'function') {
            service[request.method](databaseQuery, serviceCallback);
        } else {
            sendResponse(405);
        }
    }


    // Sends specified response to client
    function sendResponse(statusCode, json, html) {
        const responseHeaders = headers[json ? 'json' : html ? 'html' : 'plain'];
        response.writeHead(statusCode, responseHeaders);
        json && response.write(json);
        response.end(html ? html : '\n');
        const withData = json ? ' (with JSON)' : html ? ' (with HTML)' : '';
        log(`Sent response: ${statusCode} ${http.STATUS_CODES[statusCode]}${withData}`);
    }


    // Send specific responses without data
    function send200() { sendResponse(200); }   // OK
    function send201() { sendResponse(201); }   // Created
    function send400() { sendResponse(400); }   // Bad Request
    function send401() { sendResponse(401); }   // Unauthorized
    function send404() { sendResponse(404); }   // Not Found
    function send500() { sendResponse(500); }   // Server Error


    // Returns the client’s IPv4 address
    function getClientIP() {
        const clientIP = (request.headers['x-forwarded-for'] || '').split(',')[0] || request.connection.remoteAddress;
        return clientIP.substring(clientIP.lastIndexOf(':') + 1, clientIP.length);
    }


    // Write request info to console
    log(divider);
    log(`Connection from ${getClientIP()} at ${requestTime}`);

    if (request.method === 'OPTIONS') {
        log('Received OPTIONS request');
    } else {
        log(`Received request to ${request.method} data in ${collection} collection`);
        Object.keys(query).length && log('Query: ' + JSON.stringify(query));
    }
    
    // Make sure collection from URL request is valid
    if (!collections.includes(collection)) {
        log('Invalid collection in URL request');
        send404();
            
    // Client wants OPTIONS
    } else if (request.method === 'OPTIONS') {
        serviceRequest();

    // Client wants to POST errors or feedback
    } else if ((collection === 'Errors' || collection === 'Feedback') && request.method === 'POST') {
        serviceRequest(null, savedData => {
            if (collection && savedData) {
                send201();
                savedData = stripChars(beautify(JSON.stringify(savedData)), ['",', '},', '"', '{', '}']);
                
                // Improve formatting of feedback email
                if (collection === 'Feedback') {
                    savedData = savedData.replace(/\s*.+:\s/g, match => {
                        return '\n' + match.toUpperCase();
                    });
                }
                sendEmail({
                    subject: 'StoryTime ' + collection, 
                    text: savedData
                });
            }
        });
        
    // Client wants to confirm email address
    } else if (collection === 'Confirm') {
        if (request.method === 'POST') {
            serviceRequest(null, savedData => {
                const uuid = { $in: [savedData.uuid] };
                findData({ email: savedData.email, uuids: uuid  }, 'Auth', () => {
                    sendValidationEmail(savedData.email, savedData._id, send400, send201);
                }, foundData => {
                    updateData({
                        query: { _id: foundData._id },
                        data: { $pop: { uuids: savedData.uuid } },
                        collection: 'Auth', 
                        fail: send500, 
                        success: () => {
                            sendValidationEmail(savedData.email, savedData._id, send400, send201);
                        }
                    });
                });
            });
        } else if (request.method === 'GET') {
            serviceRequest({ _id: mongojs.ObjectId(query.token) }, foundData => {
                if (foundData.status === 'expired') {
                    sendResponse(200, null, pages.expired);
                } else if (foundData.status === 'sent') {
                    const uuid = { $push: { uuids: foundData.uuid } },
                        addUuid = (uuid, id) => {
                            log(`addUuid(${uuid}, ${id})`);
                            updateData({
                                query: { _id: id }, 
                                data: uuid,
                                collection: 'Auth',
                                fail: send500,
                                success: () => {
                                    sendResponse(200, null, pages.confirmed);
                                }
                            });
                        };

                    foundData.status = 'confirmed';
                    delete foundData._id;
                    delete foundData.timestamp;
                    delete foundData.uuid;
                    log('Auth: ' + JSON.stringify(foundData));
                    
                    findData({ email: foundData.email }, 'Auth', () => {
                        saveData(foundData, 'Auth', send500, updatedData => {
                            addUuid(uuid, updatedData._id);
                        });
                    }, foundData => {
                        addUuid(uuid, foundData._id);
                    });
                } else {
                    log('Problem with Auth status');
                    send500();
                }
            });
        }
    
    // Client wants to GET sync token
    } else if (query.email && query.uuid && collection === 'Auth' && request.method === 'GET') {
        serviceRequest({ email: query.email, uuids: { $in: [query.uuid] } }, data => {
            const syncToken = { token: data._id };
            sendResponse(200, JSON.stringify(syncToken));
        });
    
    // Client wants to sync data
    } else if (query.token && collection !== 'Errors' && collection !== 'Feedback' && collection !== 'Auth') {
        findToken(query.token, send401, serviceRequest);
    
    // Don’t know what client wants
    } else { send400(); }
});


// Let’s get things rolling
server.listen(port, 'localhost');
log(divider);
log(`Listening on localhost:${port}`);
setInterval(maintenance, 60000);
