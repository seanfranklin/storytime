#!/bin/sh
# This is the StoryTime multi-purpose script that
# automates backup, build, cleaning and server tasks.

LOGS="$ST/logs"
ANDROID="/$ST/app/platforms/android"


# Exits script early with error details
build_error() {
    echo "$1 error code $4 at $(date +"%I:%M %p") while building $2 $3 :(" &&
    exit $4
}


# Remove garbage chars from specified log and write final file
clean_log() {
    tr -cd '\11\12\15\40-\176' < $LOGS/build-$1-$2.log.tmp | sed 's/\[4m/*** /g' | sed 's/\[24m/ ***/g' | sed 's/\[32m//g' | sed 's/\[39m//g' > $LOGS/build-$1-$2.log

    if [ -f "$LOGS/build-$1-$2.log.tmp" ]; then
        rm $LOGS/build-$1-$2.log.tmp
    fi
}


# Replicates src in app/www folder
refresh_app_www() {
    echo "Cleaning 'app/www'..."
    rm -rf $ST/app/www/*

    if [ "$1" = "debug" -o "$1" = "release" ]; then
        echo "Copying 'src' to 'app/www'..."
        cp -a $ST/src/ $ST/app/www/

        if [ "$1" = "release" ]; then
            rm $ST/app/www/scripts/debug.js
        fi
    fi
}


# Replicates web build in server/www folder
refresh_server() {
    echo "Cleaning 'server/www'..."
    rm -rf $ST/server/www/*

    if [ "$1" = "debug" -o "$1" = "release" ]; then
        echo "Copying 'web/$1' to 'server/www'..."
        cp -a $ST/web/$1/ $ST/server/www/
    fi
}


# Replicates src in specified web subfolder
refresh_web() {
    if [ "$1" = "debug" -o "$1" = "release" ]; then
        echo "Cleaning 'web/$1'..."
        rm -rf $ST/web/$1/*
        
        echo "Copying 'src' to 'web/$1'..."
        cp -a $ST/src/ $ST/web/$1/

        if [ "$1" = "release" ]; then
            rm $ST/web/$1/scripts/debug.js
        fi
        
    else
        echo "Cleaning 'web'..."
        rm -rf $ST/web/debug/*
        rm -rf $ST/web/release/*
    fi
}



# Script parameter logic starts here

# Perform all automated cleaning, backup, build and server tasks
if [ "$1" = "all" ]; then
    st clean &&
    st backup all &&
    st build all &&
    st server deploy &&
    st server test
    

# Create dated tar backup of specified folders
elif [ "$1" = "backup" ]; then
    cd $ST
	NOW=$(date +"%Y-%m-%d-%H").tar.gz

    # Perform all backup tasks
    if [ "$2" = "all" ]; then
        st backup full &&
        st backup app &&
        st backup server &&
        st backup src &&
        st backup web &&
        
        echo "All backups completed successfully" ||
        echo "Error encountered during a backup"
        
    # Backup everything except backup archives and mViewer
    elif [ "$2" = "full" ]; then
        remove_junk $ST
        echo "Backing up entire project folder..."
		tar -c -z --exclude=backups --exclude=mViewer \
            -f $ST/backups/full/st_$NOW -C $ST *
			
    # Backup specified folder (including subfolders)
	elif [ "$2" = "app" -o "$2" = "server" -o "$2" = "src" -o "$2" = "web" ]; then
        remove_junk $ST
        echo "Backing up '$2' folder..."
		tar czf $ST/backups/$2/$2_$NOW -C $ST $2

	else
		echo "Usage: st backup all|app|full|server|src|web"
	fi
	

# Perform specified build by calling Grunt (and maybe Cordova)
elif [ "$1" = "build" ]; then

    # Perform both debug and release builds for all platforms
    if [ "$2" = "all" ] && [ -z "$3" ]; then
        st build web debug &&
        st build web release &&
        st build app debug &&
        st build app release &&
        
        echo "All builds completed successfully" ||
        echo "Error encountered during a build"
    
    # Perform both debug and release builds for specified platform
    elif [ "$2" = "app" -o "$2" = "web" ] && [ "$3" = "all" ]; then
        st build $2 debug &&
        st build $2 release &&

        echo "All builds completed successfully" ||
        echo "Error encountered during a build"
    
    # Perform single build for Android platform
    elif [ "$2" = "app" ]; then
        if [ "$3" = "debug" -o "$3" = "release" ]; then
            echo "Starting $2 $3 build tasks at $(date +"%Y-%m-%d %H:%M:%S")" 2>&1 | tee $LOGS/build-$2-$3.log.tmp
            cd $ST/ &&
            remove_junk ST &&
            refresh_app_www $3 &&

            echo "Running Grunt tasks..." &&
            grunt $2-$3 --dir=$2/www/ 2>&1 | tee -a $LOGS/build-$2-$3.log.tmp
            RC=${PIPESTATUS[0]}

            if [[ $RC != 0 ]]; then
                build_error "Grunt" $2 $3 $RC 2>&1 | tee -a $LOGS/build-$2-$3.log.tmp
                exit $RC
            fi

            echo "Running Cordova build..." &&
            cd $ST/$2/ &&
            cordova build --$3 --verbose 2>&1 | tee -a $LOGS/build-$2-$3.log.tmp
            RC=${PIPESTATUS[0]}
            
            if [[ $RC != 0 ]]; then 
                build_error "Cordova" $2 $3 $RC 2>&1 | tee -a $LOGS/build-$2-$3.log.tmp
                exit $RC
            fi

            echo "Copying apk to shared folder..." &&
            cp $ANDROID/build/outputs/apk/android-$3.apk ~/Public/StoryTime-$3.apk &&

            echo "Completed all $2 $3 build tasks at $(date +"%I:%M %p")" 2>&1 | tee -a $LOGS/build-$2-$3.log.tmp
            clean_log $2 $3

        else
            echo "Usage: st build app all|debug|release"
        fi
        
    # Perform single build for web platform
    elif [ "$2" = "web" ]; then
        if [ "$3" = "debug" -o "$3" = "release" ]; then
            echo "Starting $2 $3 build tasks at $(date +"%Y-%m-%d %H:%M:%S")" 2>&1 | tee $LOGS/build-$2-$3.log.tmp
            cd $ST/ &&
            remove_junk $ST &&
            refresh_web $3 &&
        
            echo "Running Grunt tasks..." &&
            grunt $2-$3 --dir=$2/$3/ 2>&1 | tee -a $LOGS/build-$2-$3.log.tmp
            RC=${PIPESTATUS[0]}

            if [[ $RC != 0 ]]; then
                build_error "Grunt" $2 $3 $RC 2>&1 | tee -a $LOGS/build-$2-$3.log.tmp
                exit $RC
            fi

            echo "Completed all $2 $3 build tasks at $(date +"%I:%M %p")" 2>&1 | tee -a $LOGS/build-$2-$3.log.tmp
            clean_log $2 $3

        else
            echo "Usage: st build web all|debug|release"
        fi
        
	else
		echo "Usage: st build all|app|web"
	fi


# Clean up temporary build files
elif [ "$1" = "clean" ]; then
    echo "Removing apks from shared folder..."
	rm ~/Public/StoryTime-*
    
    remove_junk $ST
    refresh_app_www
    refresh_web
    
    echo "Running Cordova's cleaning script..."
    cd $ANDROID/cordova/
    ./clean


# Interact with an Android device
elif [ "$1" = "device" ]; then

    # Inspect device webviews using developer tools
    if [ "$2" = "inspect" ]; then
        adb forward tcp:9222 localabstract:chrome_devtools_remote

    # Run latest build on device
    elif [ "$2" = "run" ]; then
        cd $ST/app/
        cordova run android --nobuild

    else
        echo "Usage: st device inspect|run"
    fi


# Generate or view JS documentation
elif [ "$1" = "docs" ]; then

    # Generate JSDocs to docs folder
    if [ "$2" = "gen" ]; then
        cd $ST/src/scripts/
        jsdoc --recurse --verbose --destination $ST/docs/ *

    # View generated JSDocs in browser
    elif [ "$2" = "view" ]; then
        cd $ST/docs/
        open index.html

    else
        echo "Usage: st docs gen|view"
    fi


# Emulate app using specified virtual device
elif [ "$1" = "emulate" ]; then

	if [ "$2" = "ripple" ]; then
        cd $ST/app/
		cp -v config.xml ./www/
		ripple emulate --port 8080
		
	elif [ ! -z "$2" ]; then
        echo "Starting logcat..." &&
        adb logcat Cordova:D CordovaActivity:D CordovaLog:D CordovaWebViewClient:D CordovaWebView:D CordovaNetworkManager:D PluginManager:D *:F &

        echo "Starting emulator..." &&
        cd $ST/app/ &&
		cordova run --emulator --nobuild --target=$2 &&
        
        echo "Emulator started successfully" ||
        echo "Error running app on emulator"

    else
        echo "Usage: st emulate <AVD>|ripple"
    fi


# Work with remote server
elif [ "$1" = "server" ]; then

    # Perform MongoDB administration via browser
    if [ "$2" = "dbadmin" ]; then
        if ( ! ps ax | grep -v -e grep | grep mongoui > /dev/null ); then
            mongoui &
            sleep 4
        fi
        
        open http://localhost:3001/

    # Test server with GET and POST requests
	elif [ "$2" = "test" ]; then
		curl --verbose --request GET --insecure \
            --url https://151.236.24.29:60806/User?token=101010101010101010101010
            
        sleep 1
        
        curl --verbose --request POST --insecure \
            --url https://151.236.24.29:60806/User?token=101010101010101010101010 \
            --header "Content-Type: application/json" \
            --data '{ "updated": "2014-00-00T00:00:00.000Z", "test": true, "version": 0 }'

    # Deploy files from local server folder to remote server
	elif [ "$2" = "deploy" ]; then
            
        # Refresh local server folder with latest web build
        if [ "$3" = "debug" -o "$3" = "release" ]; then
            remove_junk $ST
            refresh_server $3
        fi
        
        echo "Deploying 'server' to remote server..."
        rsync --delete --recursive --times --exclude="node_modules" --exclude="*.gz" --human-readable --verbose \
            --rsh="ssh -p 44 -l $USER" $ST/server/ $SERVER:/home/sean/st/ &&

        echo "Completed all server deployment tasks at $(date +"%I:%M %p")" ||
        echo "Failed to deploy 'server' to remote server"
        
    else
        echo "Usage: st server dbadmin|test|deploy [debug|release]"
    fi


# If no parameters recognised then output brief usage message
else
    echo "Usage: st all|backup|build|clean|device|docs|emulate|server"
fi
