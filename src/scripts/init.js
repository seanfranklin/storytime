'use strict';


/**
 *  Checks network connection type.
 *  @return {boolean|string} Network connection state or type
 */

st.checkConnection = function () {
    st.debug('checkConnection()');
    
    var connection;
    
    if (typeof navigator.connection !== 'undefined') {
        connection = navigator.connection.type;

    } else if (typeof navigator.onLine !== 'undefined') {
        connection = navigator.onLine;

    } else {
        connection = 'unknown';
    }

    return connection === 'none' ? false : connection;
};



/**
 *  Checks if localStorage is available for use.
 *  @return {boolean} localStorage available
 */

st.checkStorage = function () {
    st.debug('checkStorage()');
    
    try {
        localStorage.setItem('test', 'test');
        localStorage.removeItem('test');
        return true;
        
    } catch (e) {
        return false;
    }
};



/**
 *  Checks if device supports touch interaction.
 *  @return {boolean} Touch supported
 */

st.checkTouch = function () {
    st.debug('checkTouch()');
    
    return typeof document.ontouchstart !== 'undefined';
};



/**
 *  Checks if device can vibrate.
 *  @return {boolean} Vibration supported
 */

st.checkVibrate = function () {
    st.debug('checkVibrate()');
    
    return typeof navigator.vibrate !== 'undefined';
};



/**
 *  Checks if browser/device supports CSS vmin unit.
 *  @return {boolean} False if vmin supported
 */

st.checkVmin = function () {
    st.debug('st.checkVmin()');

    // Creates a hidden elem to use for vmin check
    function createTestElem() {
        $('body').append('<div id="vmin-test" class="custom" style="width: 50vmin"></div>');
        return document.getElementById('vmin-test');
    }

    const AGENT = navigator.userAgent,
        ONE_VW = window.innerWidth / 100,
        ONE_VH = window.innerHeight / 100,
        ELEM = document.getElementById('vmin-test') || createTestElem(),

        COMP_WIDTH = parseInt((window.getComputedStyle ?
                getComputedStyle(ELEM, null) : ELEM.currentStyle).width, 10);

    var vminSupported = parseInt(Math.min(ONE_VW, ONE_VH) * 50, 10) === COMP_WIDTH;

    st.debug('vminSupported: ' + vminSupported, 'var', true);

    if (vminSupported && AGENT && AGENT.includes('Android')) {
        const VERSION = parseFloat(AGENT.slice(AGENT.indexOf('Android') + 8));

        st.debug('Running on Android ' + VERSION, 'info');

        if (VERSION < 5.0) {
            vminSupported = false;
            st.debug('vminSupported: ' + vminSupported, 'var', true);
        }
    }

    return !vminSupported;
};



/**
 *  Initializes platform-specific settings using the Cordova APIs.
 *  For running on device as Cordova app. Not needed for Web platform.
 */

//APP_BEGIN
st.deviceReady = function () {
    st.debug('deviceReady()');
    
    if (typeof device === 'object') {
        st.Env.platform = device.platform;
        st.Env.uuid = device.uuid;

    } else {
        st.handleError('Failed to load device plugin');
    }

    st.Env.connection = st.checkConnection();
    st.Env.hasVibration = st.checkVibrate();
    
    document.addEventListener('resume', () => {
        st.debug('resume', 'event');
        st.resume();
    }, false);
    
    document.addEventListener('pause', () => {
        st.debug('pause', 'event');
        st.pause();
    }, false);
    
    /*  The two hardware buttons below don’t work
    https://issues.apache.org/jira/browse/CB-8921
    
    document.addEventListener('searchbutton', () => {
        st.debug('searchbutton', 'event');
    }, false);
    
    document.addEventListener('menubutton', () => {
        st.debug('menubutton', 'event');
        st.toggleMenu();
    }, false);  */
    
    document.addEventListener('backbutton', () => {
        st.debug('backbutton', 'event');
        st.navBack();
    }, false);
    
    /* Vibrate when an icon is selected
    $('#header .icon').on('click.vibrate', () => {
        navigator.notification.vibrate(20);
    }); */
        
    // inappbilling.init(success, error, options, skus)
    if (typeof inappbilling === 'object') {
        inappbilling.init(() => {
            st.debug('inappbilling: initialized', 'info');

            st.Env.connection && inappbilling.getPurchases(data => {
                var storeUser = false;

                st.User.GoogleIAP = data;

                // Ensure purchased stories are stored in the User space
                $.each(st.User.GoogleIAP, (index, purchase) => {
                    if (st.Catalog.hasOwnProperty(purchase) && !st.User.purchases.includes(purchase)) {
                        st.User.purchases.push(purchase);
                        storeUser = true;
                    }
                });

                storeUser && st.storeData('User', true);
                st.debug('inappbilling: verified purchases', 'info');

            }, () => {
                st.handleError('Failed to verify purchases');
            });

        }, () => {
            st.handleError('Failed to initialize inappbilling');
        });

    } else {
        st.handleError('Failed to load inappbilling');
    }
    
    // Sounds use Cordova plugin
    st.initSounds();

    // Cordova device components loaded so show webview and continue init
    st.Init.device.resolve();
};
//APP_END



/**
 *  Initializes constant properties used within various functions.
 */

st.initConsts = function () {
    st.debug('initConsts()');
    
    const CONSTS = [{
        name:  'REPORT',
        value: ['Env', 'User', 'Prefs']
    }, {
        name:  'VIEWS',
        value: ['catalog', 'legal', 'prefs', 'profile']
    }, {
        name:  'SYNC',
        value: ['User', 'Profile']
    }, {
        name:  'EMAIL',
        value: 'wordsandmagic@gmx.com'
    }, {
        name:  'SERVER',
        value: 'https://storytime.happyforever.com/sync/'
    }];

    $.each(CONSTS, (index, constant) => {
        Object.defineProperty(st, constant.name, {
            value: constant.value,
            writable: false,
            configurable: false
        });

        typeof st[constant.name] === 'object' && Object.freeze(st[constant.name]);
    });
};



/**
 *  Initializes data (properties) for a namespace.
 *  Gets props from localStorage or calls an 'init' function.
 *  @param {string} space - Namespace to initialize
 *  @param {boolean} [defaults] - Force default data
 *  @param {boolean} [sync] - Try sync data with server
 */

st.initData = function (space, defaults, sync) {
    st.debug(`initData(${space}, ${defaults}, ${sync})`);
        
    // Resolve deferred used for sync
    function resolveSync() {
        st.Sync.Init[space] && st.Sync.Init[space].resolve();
    }
    
    // Init namespace defaults
    function initDefaults() {

        // Use space’s own init function if available
        if (st['init' + space]) {
            st.debug(space + ': using default data', 'info');
            st['init' + space]();
            st.storeData(space, false, true);

        // Otherwise create empty space
        } else {
            st[space] = {};
        }
        
        resolveSync();
    }
    
    // Load namespace defaults if forced
    if (defaults) {
        initDefaults();
        
    // Load namespace from localStorage if possible
    } else if (st.Env.hasStorage && localStorage[space]) {
        st[space] = JSON.parse(localStorage[space]);
        st.debug(space + ': loaded data from localStorage', 'info');

        // There might be newer data on server
        if (sync && st.Prefs.sync) {
            st.retrieveData(space, false, () => {
                resolveSync();
            });

        } else {
            resolveSync();
        }
    
    // If no localStorage maybe get data from server
    } else if (sync && st.Prefs.sync) {
        st.retrieveData(space, true, () => {
            if (st[space]) {
                resolveSync();

            } else {
                initDefaults();
            }
        });
        
    // If couldn’t get data from storage or server use defaults
    } else {
        initDefaults();
    }
};



/**
 *  Initializes the Env namespace, which holds stateful info about environment.
 */

st.initEnv = function () {
    st.debug('initEnv()');

    st.Env = {

        // {?string} State identifiers
        authToken:    null,            // Sync authorization token
        current:      null,            // Current story or view
        currentPath:  '/',             // Path to current’s files
        modal:        null,            // Currently open modal
        uuid:         null,            // Device or browser uuid

        // {boolean} State flags and directives
        doClear:      false,           // Story needs clearing
        doDeduct:     false,           // Deduct credit on first save
        doResize:     false,           // Do resize now or timeout object to debounce  //WEB
        doSave:       false,           // Save story progress
        doSyncOld:    false,           // Use server data that is older
        isBottom:     false,           // Scroll position is at bottom of page
        isMascot:     false,           // Mascot is visible
        isScroll:     true,            // User scrolling is enabled
        isSlider:     false,           // Use slider mode for catalog view
        isStory:      false,           // Loaded and displaying a story (not a view)
        isTouchmove:  false,           // User is performing a touch movement
        isUpdated:    false,           // First run since version updated
        isView:       false,           // Loaded and displaying a view (not a story)

        // {boolean} Features supported by platform
        hasStorage:   st.checkStorage(),  // User-agent supports localStorage
        hasTouch:     st.checkTouch(), // Device supports touch interaction
        vmin:         st.checkVmin(),  // Number if vmin unit NOT supported

        // These features are checked later as they may use Cordova plugins that aren’t ready
        connection:   false,           // Network connection state or type
        hasVibration: false,           // Device supports vibration

        // Miscellaneous numeric counters
        cacheCounter: 0,               // Images successfully lazy-loaded
        syncAttempts: 0,               // Maximum remaining checkAuth attempts
        skyCounter:   0,               // Track and id sky objects
        version:      0.99,            // App version number

        // {object} Close modal function hooks
        beforeClose:  [],              // Contained functions called before modal is closed
        afterClose:   [],              // Contained functions called after modal closed

        // Miscellaneous stuff
        loading:      ['init'],        // List of things currently loading
        messages:     [],              // Important messages for user
        platform:     'Web',           // OS platform running on device
        timestamp:    '2015-00-00T00:00:00.000Z'  // Default timestamp
    };
};



/**
 *  Initializes a new Errors namespace with no properties.
 */

st.initErrors = function () {
    st.debug('initErrors()');
    
    st.Errors = {};
};



/**
 *  Initializes a namespace with properties from JSON file.
 *  @param {string} space - Namespace to initialize
 *  @param {function} [callback] - Called if init succeeds
 */

//DEBUG_BEGIN
st.initJSON = function (space, callback) {
    st.debug(`initJSON(${space})`);
    
    // If this function is to be used then FILE needs root path set 
    const FILE = space.toLowerCase() + '.json';
    
    $.getJSON(FILE, data => {
        st[space] = data;
        typeof callback === 'function' && callback();
        
    }).fail(() => {
        st.handleError(st.Msgs.Errors.file, FILE);
    });
};
//DEBUG_END



/**
 *  Initializes custom jQuery methods used throughout the app.
 */

st.initMethods = function () {
    st.debug('initMethods()');
    
    // Custom jQuery selector of displayed elements
    $.extend($.expr[':'], {
        displayed: function (e) {
            return $(e).css('display') !== 'none';
        }
    });
    
    // Custom jQuery method like getScript() but allows user
    // to set any option except dataType, cache, and url
    $.cachedScript = function (url, options) {
        options = $.extend(options || {}, {
            dataType: 'script',
            cache: true,
            url
        });
     
        // Return jqXHR object to allow chaining callbacks
        return jQuery.ajax(options);
    };

    // Custom jQuery method to shuffle DOM elements
    $.fn.shuffle = function () {
        return this.each(function () {
            const ITEMS = $(this).children();
            
            return (ITEMS.length) ? $(this).html($.shuffle(ITEMS)) : this;
        });
    };

    // Custom jQuery method to shuffle array items
    $.shuffle = function (array) {
        var i, j, x;
        
        for (j, x, i = array.length; i;
                j = parseInt(Math.random() * i, 10),
                x = array[--i], array[i] = array[j], array[j] = x) {
            st.debug('shuffle countdown: ' + i, 'info');
        }
        
        return array;
    };
};



/**
 *  Initializes Prefs namespace with default properties.
 */

st.initPrefs = function () {
    st.debug('initPrefs()');
  
    st.Prefs = {
        scroll:   false,              // Automatically scroll down to next section
        sound:    true,               // Play sound effects throughout the app
        serif:    false,              // Display story text using serif font
        british:  false,              // Prefer British words and spelling
        labels:   true,               // Show labels in drop-down menu
        sync:     false,              // Synchronize user data with server
        text:     'medium'            // Size to display story text
    };
};



/**
 *  Initializes Profile namespace with default properties.
 */

st.initProfile = function () {
    st.debug('initProfile()');

    st.Profile = {
        updated:  st.Env.timestamp,   // When namespace was last updated
        name:     null,               // User’s preferred name
        age:      null,               // User’s age in years
        gender:   null,               // User’s gender
        color:    null,               // User’s preferred color
        food:     null,               // User’s preferred food
        animal:   null,               // User’s preferred animal
        region:   null,               // Region of the world the user lives in
        density:  null,               // Density of neighborhood the user lives in
        building: null                // Type of building the user lives in
    };
};



/**
 *  Initializes Sounds namespace and loads universal sound effects.
 */

st.initSounds = function () {
    st.debug('initSounds()');
    
    st.Sounds = {
        click:   st.loadSound('click', '/sounds/'),    // Monosyllabic click
        error:   st.loadSound('error', '/sounds/'),    // Chipmunk 'uh-oh' voice
        glass:   st.loadSound('glass', '/sounds/'),    // Short wet metallic boing
        invalid: st.loadSound('invalid', '/sounds/'),  // Harsh buzz
        menu:    st.loadSound('menu', '/sounds/'),     // Wet-sounding lick
        zoom:    st.loadSound('zoom', '/sounds/')      // Subtle atmospheric whoosh
    };
};



/**
 *  Initializes User namespace with default properties.
 */

st.initUser = function () {
    st.debug('initUser()');
    
    st.User = {
        updated:   st.Env.timestamp,  // When namespace was last updated
        email:     null,              // User’s email address
        version:   0,                 // Last version of app used
        credits:   0,                 // Credits available to user
        seen:      [],                // Modals/messages user has seen
        reading:   [],                // Stories user is reading
        restarted: [],                // Stories user has restarted
        rewarded:  [],                // Stories user earned a credit reward for
        purchases: []                 // Stories user has purchased
    };
};



/**
 *  Initializes settings necessary for running in a web browser.
 *  Not needed when running as app on mobile platforms but retained
 *  in case Cordova fails to load properly.
 */

st.initWeb = function () {
    st.debug('initWeb()');

    $('html').addClass('scroll-y');

    st.Env.connection = st.checkConnection();
    st.Env.hasVibration = st.checkVibrate();

    st.Init.device.resolve();
};



/**
 *  Loads a sound file using the Howler.js library.
 *  @param {string} sound - Name of file to load
 *  @param {string} [path] - Default is 'sounds' sub-folder of current Env path
 *  @return {Howl}
 */

st.loadHowl = function (sound, path = st.Env.currentPath + 'sounds/') {
    st.debug(`loadHowl(${sound}, ${path})`);

    var mp3File = `${path}${sound}.mp3`,
        oggFile = `${path}${sound}.ogg`;
    
    return new Howl({
        urls: [mp3File, oggFile],

        onloaderror: () => {
            st.handleError(st.Msgs.Errors.sound, sound);
        }
    });
};



/**
 *  Loads a sound file using the Cordova Media API.
 *  @param {string} sound - Name of file to load
 *  @param {string} [path] - Default is 'sounds' sub-folder of current Env path
 *  @return {Media}
 */

//APP_BEGIN
st.loadMedia = function (sound, path = st.Env.currentPath + 'sounds/') {
    st.debug(`loadMedia(${sound}, ${path})`);
    
    if (st.Env.platform === 'Android') {
        path = '/android_asset/www' + path;
    }
    
    return new Media(`${path}${sound}.wav`, () => {
        st.debug('sound: ' + sound, 'var');
        
    }, () => {
        st.handleError(st.Msgs.Errors.sound, sound);
    });
};
//APP_END



/**
 *  Loads a sound file by calling either loadMedia or loadHowl.
 *  @param {string} sound - Name of sound file without extension
 *  @param {string} [path] - Default is 'sounds' sub-folder of current Env path
 */

st.loadSound = function (sound, path) {
    st.debug(`loadSound(${sound}, ${path})`);
    
    var Sound;
    
    //APP_BEGIN
    if (typeof Media !== 'undefined') {
        Sound = st.loadMedia(sound, path);
        
    } else
    //APP_END

    if (typeof Howler !== 'undefined') {
        Sound = st.loadHowl(sound, path);
        
    } else {
        st.handleError('No sound method available');
    }
    
    return Sound;
};
