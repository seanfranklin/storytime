'use strict';


/**
 *  Clears Sky and removes all elements in sky div.
 */

st.clearSky = function () {
    st.debug('clearSky()');

    $.each(st.Sky, prop => {
        st.Sky[prop].interval && clearInterval(st.Sky[prop].interval);
        st.Sky[prop].timer && st.Sky[prop].timer.pause();
        delete st.Sky[prop];
    });

    st.Cache.sky.find('.cloud, .flyer').remove();
    st.Env.skyCounter = 0;
};



/**
 *  Creates a moving cloud at a random position.
 *  Calls self to create another cloud once the cloud has disappeared.
 *  @param {number} [cloudNum] - Integer denoting cloud to create (7 is twilight, 8 is night)
 */

st.createCloud = function (cloudNum = st.rndInt(1, 6)) {
    st.debug(`createCloud(${cloudNum})`, 'function', true);
    
    const IS_DAY = cloudNum < 7,
        CLOUD = 'cloud' + (++st.Env.skyCounter),
        SKY_HEIGHT = st.Cache.sky.height() / 2,
        FLY_TIME = [240, 320, 400][st.rndInt(0, 2)],
        CLOUD_WIDTH = [346, 241, 262, 381, 156, 438, 993, 701][cloudNum - 1],
        TIMER_DURATION = FLY_TIME * ((window.innerWidth + CLOUD_WIDTH) / 2048) * 1000;

    var topPos = IS_DAY ? st.rndInt(0, SKY_HEIGHT) : 0;

    // If top of cloud is low then chance to re-calculate top
    if (topPos > (SKY_HEIGHT / 1.5) && st.rndInt(0, 1)) {
        topPos = st.rndInt(0, SKY_HEIGHT / 2);
    }

    st.Cache.sky.append(`<img id="${CLOUD}" class="cloud cloud-type-${cloudNum}" style="top: ${topPos}px; left: -${CLOUD_WIDTH}px" src="/images/clouds/cloud${cloudNum}.png" alt="" role="presentation" onload="this.className += ' cloud-time-${FLY_TIME}'">`);

    st.Sky[CLOUD] = {};

    st.Sky[CLOUD].timer = new st.Timer(TIMER_DURATION, () => {
        const $CLOUD = $('#' + CLOUD);

        st.Sky[CLOUD].interval = setInterval(() => {
            if ($CLOUD.offset().left > window.innerWidth) {
                $CLOUD.remove();
                clearInterval(st.Sky[CLOUD].interval);
                st.Sky[CLOUD].timer.pause();
                delete st.Sky[CLOUD];
                st.debug('Interval: deleted ' + CLOUD, 'info');

                // Check skyCounter to avoid continuous high CPU usage
                st.Env.isView && st.Env.skyCounter < 10 && st.createCloud(IS_DAY ? undefined : cloudNum);

            } else {
                st.debug(`Interval: ${CLOUD} still visible`, 'info');
            }
        }, 5000);
    });

    st.Sky[CLOUD].timer.start();
};



/**
 *  Creates a moving bird or plane near the top of the screen.
 *  Calls self to create another once the bird or plane has disappeared.
 *  @param {string} [flyerType] - Type of flyer to create (bird or plane)
 */

st.createFlyer = function (flyerType = st.rndItem('bird', 'plane')) {
    st.debug(`createFlyer(${flyerType})`, 'function', true);

    const IS_POWERED = (flyerType === 'plane'),
        //COUNT = Object.keys(st.Sky).length,
        FLYER = flyerType + (++st.Env.skyCounter),
        START_SIDE = IS_POWERED ? 'right' : 'left',
        TOP_POS = st.rndInt(0, st.Cache.sky.height() / 10),
        FLYER_NUM = st.rndInt(1, 2),
        FLYER_TIMES = IS_POWERED ? [70, 80, 90, 100] : [100, 110, 120, 130],
        FLY_TIME = FLYER_TIMES[st.rndInt(0, 3)],
        TIMER_DURATION = (FLY_TIME * (window.innerWidth / 2048) + st.rndInt(5, 15)) * 1000;

    st.Cache.sky.append(`<img id="${FLYER}" class="flyer ${flyerType}-type-${FLYER_NUM}" style="top: ${TOP_POS}px; ${START_SIDE}: -150px" src="/images/flyers/${flyerType}${FLYER_NUM}.png" alt="" role="presentation" onload="this.className += ' ${flyerType}-time-${FLY_TIME}'">`);

    st.Sky[FLYER] = {};

    st.Sky[FLYER].timer = new st.Timer(TIMER_DURATION, () => {
        $('#' + FLYER).remove();
        st.Sky[FLYER].timer.pause();
        delete st.Sky[FLYER];

        // Check skyCounter to avoid continuous high CPU usage
        st.Env.isView && st.Env.skyCounter < 10 && st.createFlyer();
    });

    st.Sky[FLYER].timer.start();
};



/**
 *  Initializes sky according to hour of day.
 *  @param {number} [hour] - Hour of day
 *  @param {boolean} [doAnim] - Create/show animations
 */

st.initSky = function (hour = moment().hour(), doAnim = true) {
    st.debug('initSky()');

    // Only do extra animation in scenarios where it shouldn’t be too taxing on processors
    const DO_EXTRA_ANIM = !st.Env.isSlider && window.innerWidth < 1153;

    // Creates clouds and flyers
    function initDay(cloudNum) {
        st.Cache.twinkle.addClass('hide');

        if (doAnim) {
            if (cloudNum) {
                st.createCloud(cloudNum);

            } else {
                st.Sky.init = new st.Timer(3000, () => {
                    if (st.Env.isView && DO_EXTRA_ANIM) {
                        st.createCloud();
                    }

                    st.Env.isView && st.createFlyer();
                    delete st.Sky.init;
                });

                st.createCloud();
                st.Sky.init.start();
            }
        }
    }

    // Set the color gradient and brightness of the sky
    st.Cache.sky.hasClass('sky-' + hour) || st.Cache.sky.removeClass().addClass('sky-' + hour);

    if (hour < 5 || hour > 20) {
        if (doAnim && DO_EXTRA_ANIM) {
            st.Cache.twinkle.removeClass('hide');
            st.rndInt(1, 3) === 2 && st.createCloud(8);
        }

    } else if (hour > 8 && hour < 17 ) {
        initDay();

    } else if (hour === 5 || hour === 6 || hour === 17 || hour === 18) {
        initDay(7);
    
    } else {
        initDay(8);
    }
};



/**
 *  Pauses animation of elements in sky div.
 */

st.pauseSky = function () {
    st.debug('pauseSky()');

    $.each(st.Sky, prop => {
        st.Sky[prop].timer && st.Sky[prop].timer.pause();
    });

    st.Cache.sky.find('.cloud, .flyer').css('animationPlayState', 'paused');
    st.Cache.twinkle.css('animationPlayState', 'paused');
};



/**
 *  Resumes animation of elements in sky div.
 */

st.resumeSky = function () {
    st.debug('resumeSky()');

    st.Cache.twinkle.css('animationPlayState', 'running');
    st.Cache.sky.find('.cloud, .flyer').css('animationPlayState', 'running');

    $.each(st.Sky, prop => {
        st.Sky[prop].timer && st.Sky[prop].timer.start();
    });
};
