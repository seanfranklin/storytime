'use strict';


/**
 *  Shows the welcome modal that introduces changes and new features.
 */

st.showWelcome = function () {
    st.debug('showWelcome()');

    const WELCOME = '<p>Welcome to StoryTime! There are currently three full stories available to read. Stories are listed by difficulty.</p>';
    let title = `<span class="modal-title">StoryTime</span>`;
    title += `<p>Debug Version ${st.Env.version}</p>`;  //DEBUG

    st.showModal('welcome', {
        message: `${title}${WELCOME}`,
        speech: st.Msgs.Speech['r0' + st.rndInt(1, 3)],

        afterClose: () => {
            if (!st.User.seen.includes('welcome')) {
                st.User.seen.push('welcome');
                st.storeData('User');
            }
        }
    });
};



/**
 *  Loads and initializes assets that aren’t critical to initial render.
 *  Called once critical initialization is complete.
 */

st.lazyLoad = function () {
    st.debug('lazyLoad()');

    // Loads library script and signals success/failure via deferred
    function loadLib(scriptName, scriptDef) {
        //WEB_BEGIN
        $.cachedScript(scriptDef.src).done(() => {
            st.debug(`Loaded ${scriptName} script from CDN`, 'info');
            typeof scriptDef.done === 'function' && scriptDef.done();
            scriptDef.loaded.resolve();

        // If CDN load failed then try local copy
        }).fail(() => {
            st.debug(`Failed to load ${scriptName} script from CDN`, 'warning');
        //WEB_END

            $.cachedScript(`/scripts/lib/${scriptName}.min.js`).done(() => {
                st.debug(`Loaded ${scriptName} script from same-origin`, 'info');
                typeof scriptDef.done === 'function' && scriptDef.done();
                scriptDef.loaded.resolve();

            // If neither copy loads it is a fatal error
            }).fail(() => {
                scriptDef.loaded.reject();
                st.handleError('Failed to load lib script', scriptName);
                $('#message-reload').length || st.showMessage('reload');
            });
        });  //WEB
    }


    // Loads StoryTime’s lazy scripts and returns promise
    function loadST() {
        /* jshint ignore:start */

        //DEBUG_BEGIN
        return $.when(
            $.cachedScript('/scripts/data/modals.js'),
            $.cachedScript('/scripts/define.js'),
            $.cachedScript('/scripts/mascot.js'),
            $.cachedScript('/scripts/message.js'),
            $.cachedScript('/scripts/modal.js'),
            $.cachedScript('/scripts/profile.js')
        );
        //DEBUG_END

        //RELEASE_BEGIN
        return $.cachedScript('/scripts/st.lazy.min.js').done(() => {
            st.debug('Loaded StoryTime lazy script', 'info');

        }).fail(() => {
            st.handleError(st.Msgs.Errors.file, 'st.lazy');
        });
        //RELEASE_END

        /* jshint ignore:end */
    }


    // Library script definitions
    const LIB_SCRIPTS = {
        hammer: {
            src: 'https://cdn.jsdelivr.net/hammerjs/2.0.8/hammer.min.js',  //WEB
            loaded: $.Deferred(),

            done: () => {
                st.initGestures();
                st.initDebugGestures();
            }
        },

        howler: {
            src: 'https://cdn.jsdelivr.net/howler.js/1.1.28/howler.min.js',  //WEB
            loaded: $.Deferred(),
            done: st.initSounds
        },

        'jquery.panzoom': {
            src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery.panzoom/3.2.2/jquery.panzoom.min.js',  //WEB
            loaded: $.Deferred()
        },

        velocity: {
            src: 'https://cdn.jsdelivr.net/velocity/1.3.2/velocity.min.js',  //WEB
            loaded: $.Deferred(),

            done: () => {
                loadLib('velocity.ui', LIB_SCRIPTS['velocity.ui']);
            }
        },

        'velocity.ui': {
            requires: 'velocity',
            src: 'https://cdn.jsdelivr.net/velocity/1.3.2/velocity.ui.min.js',  //WEB
            loaded: $.Deferred()
        }
    };


    //DEBUG_BEGIN
    st.Cache.head.append(
       `<link rel="stylesheet" href="/styles/mascot.css">
        <link rel="stylesheet" href="/styles/modal.css">
        <link rel="stylesheet" href="/styles/menu.css">
        <link rel="stylesheet" href="/styles/message.css">
        <link rel="stylesheet" href="/styles/glass.css">
        <link rel="stylesheet" href="/styles/anim.css">`
    );
    //DEBUG_END


    st.Cache.head.append('<link rel="stylesheet" href="/styles/st.lazy.min.css">');  //RELEASE
    $('body').css('background-color', '#fcfcfc');

    const IMAGES_LOADED = $.Deferred(),
        MASCOT = st.Profile.animal || 'owl';


    (function checkImages() {
        var intervalCounter = 0,
            numOfImages = 8,

            checkInterval = setInterval(() => {
                var isFinalCheck = (++intervalCounter === 20);

                if (isFinalCheck || st.Env.cacheCounter === numOfImages) {
                    //DEBUG_BEGIN
                    if (isFinalCheck) {
                        st.debug('Images did not lazy-load in allowed time', 'warning');

                    } else {
                        st.debug('Lazy-loaded all images', 'info');
                    }
                    //DEBUG_END

                    IMAGES_LOADED.resolve();
                    clearInterval(checkInterval);
                }
            }, 200);
    }());


    $('#image-cache').append(
       `<div style="font: normal 0 KomikaDisplay">A</div>
        <img src="/images/animal/${MASCOT}.svg" alt="" onload="st.Env.cacheCounter++">
        <img src="/images/bubble-left.svg" alt="" onload="st.Env.cacheCounter++">
        <img src="/images/bubble-right.svg" alt="" onload="st.Env.cacheCounter++">
        <img src="/images/ellipsis.svg" alt="" onload="st.Env.cacheCounter++">
        <img src="/images/catalog.png" alt="" onload="st.Env.cacheCounter++">
        <img src="/images/arrow-curved.png" alt="" onload="st.Env.cacheCounter++">
        <img src="/images/arrow-pinch.png" alt="" onload="st.Env.cacheCounter++">
        <img src="/images/arrow-up.png" alt="" onload="st.Env.cacheCounter++">`
    );


    $.each(LIB_SCRIPTS, (scriptName, scriptDef) => {
        scriptDef.requires || loadLib(scriptName, scriptDef);
    });

    $.when(
        LIB_SCRIPTS.hammer.loaded,
        LIB_SCRIPTS.howler.loaded,
        LIB_SCRIPTS['jquery.panzoom'].loaded,
        LIB_SCRIPTS.velocity.loaded,
        LIB_SCRIPTS['velocity.ui'].loaded,
        IMAGES_LOADED

    ).done(() => {
        loadST().done(() => {
            st.debug('Lazy load complete', 'info');

            // Set mascot to user’s favourite animal or owl
            st.Cache.mascot.css('background', `url(/images/animal/${MASCOT}.svg) 0% / 100% no-repeat`);
            
            if (st.Env.isUpdated) {
                st.restoreDefaults();  //DEBUG
                st.User.version = st.Env.version;
                st.User.seen = [];  //DEBUG
            }

            if (st.VIEWS.includes(st.Env.current)) {
                $('html .custom').remove();  //RELEASE
                st.updateBadge('catalog');

                if (!st.User.seen.includes('welcome')) {
                    const GESTURE = st.Env.hasTouch ? 'Tap' : 'Click',
                        DISMISS = st.Env.hasTouch ? ' or swipe it horizontally to dismiss' : '',

                        afterClose = () => {
                            st.User.seen.push('welcome');
                            st.storeData('User');
                        };

                    st.showMessage({
                        id: 'welcome',
                        type: 'info',
                        message: `Welcome to StoryTime! ${GESTURE} this box for more information${DISMISS}.`,
                        animate: true,
                        afterClose,

                        click: () => {
                            st.closeMessage('welcome', null, () => {
                                afterClose();

                                setTimeout(() => {
                                    st.showWelcome();
                                }, 200);
                            });
                        }
                    });
                }

                !st.Env.hasStorage && st.showMessage('storage');
                window.innerWidth < 320 && st.showMessage('width');

            } else {
                $('#scripts, script .custom').remove();
                st.updateBadge(st.Env.current);
                !st.Env.hasStorage && st.queueMessage('storage');
                window.innerWidth < 320 && st.queueMessage('width');
            }

            st.removeFromArray('init', st.Env.loading);
        });
    });
};



/**
 *  Shows the app body.
 *  Called once critical initialization is complete.
 */

st.showApp = function ($body) {
    st.debug('showApp()');

    $body.removeClass('init');  //WEB

    // Fix occasional centering issue in slider mode
    st.Env.isSlider && st.resizecatalog();
    
    if (st.Env.platform === 'Web') {
        $.each(['Android', 'iPad', 'iPhone', 'iPod'], (index, device) => {
            if (navigator.userAgent.includes(device)) {
                $('#menu-popup').remove();
                return false;
            }
        });
    } 
    //APP_BEGIN
    else {
        navigator.splashscreen.hide();
    }
    //APP_END

    $body.css('opacity', 1);
};



/**
 *  Manages loading the app and initializing its data.
 *  Must be first function to run and called after all critical JS is loaded.
 */

st.initialize = function () {
    const $BODY = $('body');

    $BODY.on('contextmenu', () => false);  //RELEASE

    st.Init = {
        app:    $.Deferred(),
        device: $.Deferred(),
        window: $.Deferred()
    };
    
    // Bring debug messages up to point
    st.debug('StoryTime 0.99 Beta Debug Messages', 'heading');
    st.debug('----------------------------------', 'heading');
    st.debug('Long–pressing the bug icon toggles', 'info');
    st.debug('between log messages and Env props', 'info');
    st.debug('----------------------------------', 'heading');
    st.debug('initialize()');

    st.Decisions = {};      // Holds simple story decisions
    st.Defs      = {};      // Holds word definitions marked in stories
    st.Sky       = {};      // Holds Timers for sky objects

    st.Sync      = {};      // Holds Deferreds to track sync
    st.Sync.Init = {};      // Deferreds to track sync on init
    st.Sync.Old  = {};      // Deferreds to track sync of old data

    st.initMethods();       // Initialize custom jQuery methods
    st.initConsts();        // Initialize frozen constants
    st.initEnv();           // Initialize Environment state properties
    
    st.initDebugListeners();

    // If cordova.js failed to load then init for Web platform
    if (typeof cordova === 'undefined') {  //APP
        st.initWeb();

    //APP_BEGIN
    // If cordova.js loaded then listen for Cordova APIs ready
    } else {
        document.addEventListener('deviceready', st.deviceReady, false);
    }
    //APP_END

    // Maybe use sync token and uuid from localStorage
    if (st.Env.hasStorage) {
        st.Env.token = localStorage.token || null;

        st.Init.device.done(() => {
            st.Env.uuid = st.Env.uuid || localStorage.uuid || null;
        });
    }

    // Manually resize elements using vmin (if necessary)
    st.Env.vmin && st.resizeVmin();

    // Initialize namespaces that might be in localStorage
    st.initData('Feedback');  //DEBUG
    st.initData('Errors');  
    st.initData('Prefs');

    // Initialize data namespaces that might be synced
    $.each(st.SYNC, (index, space) => {
        st.Sync.Init[space] = new $.Deferred();
        st.initData(space, false, st.Env.token ? true : false);
    });

    // Load preferences that don’t rely on User namespace
    st.toggleSound();
    st.Prefs.serif && st.toggleSerif(true);
    st.Prefs.labels || st.toggleLabels();

    st.initDocListeners();
    st.initMenuListener();
    st.initWindowListeners();

    st.resizeFont();
    st.initDebugSelect();

    // This part of init needs all namespaces to be loaded
    $.when(st.Sync.Init.User, st.Sync.Init.Profile).done(() => {
        st.Env.isUpdated = st.User.version < st.Env.version;

        // Initialize data for stories in progress
        $.each(st.User.reading, index => {
            st.initData(st.User.reading[index], false, true);
        });

        $.isEmptyObject('Feedback') || st.sendFeedback();  //DEBUG

        st.loadView('catalog');  //APP
        st.loadState();  //WEB
        //st.Stats.inits++;
    });

    // When everything critical has initialized then show app and lazy load
    $.when(st.Init.app, st.Init.device, st.Init.window).done(() => {
        st.debug('Critical init complete', 'info');
        st.showApp($BODY);
        
        // If showing a view then init animated sky objects
        st.VIEWS.includes(st.Env.current) && st.initSky();

        st.lazyLoad();
        st.debug('----------------------------------', 'heading');

    }).fail(() => {
        st.handleError('Critical init failed');
    });
};
