'use strict';


/**
 *  Clears the Feedback namespace of all properties.
 */

st.clearFeedback = function () {
    st.debug('clearFeedback()');
    
    st.User.feedback = false;
    st.storeData('User', false);

    $.each(st.Feedback, prop => {
        delete st.Feedback[prop];
    });
};



/**
 *  Populates the Errors namespace with appropriate data.
 */

st.prepareErrors = function () {
    st.debug('prepareErrors()');
    
    // Timestamp the Errors object
    st.Errors.updated = moment.utc();
    
    // Add namespaces to Errors object
    $.each(st.REPORT, prop => {
        var space = st.REPORT[prop];
        
        if (!$.isEmptyObject(st[space])) {
            st.Errors[space] = $.extend(true, {}, st[space]);
        }
    });
};



/**
 *  Restores the default properties of a namespace.
 *  If no arg is passed the default action is to restore
 *  the User, Profile, Prefs and Errors namespaces.
 *  @param {string} [space] - Namespace to restore
 */

st.restoreDefaults = function (space) {
    st.debug(`restoreDefaults(${space})`);
    
    // If space was passed then restore its defaults
    if (space) {
        // Remove space from localStorage
        st.Env.hasStorage && localStorage[space] && delete localStorage[space];

        // Clear all properties
        st[space] && (st[space] = {});

        // Initialize space defaults
        st['init' + space] && st['init' + space]();
    
    // If no space specified then restore most of them
    } else {
        //Restore the story spaces
        st.Catalog && $.each(st.Catalog, story => {
            st.restoreDefaults(st.Catalog[story].key);
        });
        
        // Restore the main app spaces
        let spaces = ['User', 'Profile', 'Prefs', 'Errors'];

        $.each(spaces, (index, space) => {
            st.restoreDefaults(space);
        });
    }
};



/**
 *  Retrieves namespace data from server.
 *  @param {string} space - Namespace
 *  @param {boolean} [useOld] - Allow use of older server data
 *  @param {function} [cb] - Called after this function
 */

st.retrieveData = function (space, useOld, cb) {
    st.debug(`retrieveData(${space}, ${useOld})`);
    
    var timestamp = st[space] ? st[space].updated : st.Env.timestamp,
        query = `${space}?token=${st.Env.authToken}&timestamp=${timestamp}`,
        callback = () => { typeof cb === 'function' && cb(); };

    // Need token to retrieve data
    if (st.Env.authToken) {
        $.ajax(st.SERVER + query, {
            type: 'GET',
            dataType: 'json',

            success: data => {
                var diff = moment(timestamp).diff(data.updated);
                
                st.debug(`${space}: timestamp difference is ${diff / 1000} seconds`, 'info');
                
                // Check if retrieved data is older than existing
                if (diff > 0) {
                    if (useOld) {
                        st.Sync.Old[space] = $.Deferred();

                        if (st.Env.doSyncOld) {
                            st.Sync.Old[space].resolve();

                        } else {
                            st.showModal('sync-use');
                        }

                        st.Sync.Old[space].done(() => {
                            st.debug(space + ': using older data from server', 'info');
                            st[space] = data;
                            st.storeData(space, false);
                            
                            // Callback argument indicates whether server data used
                            callback(true);
                        });
                        
                    } else {
                        st.debug(space + ': server data is older', 'info');
                        callback(false);
                    }
                    
                } else if (diff === 0) {
                    st.debug(space + ': already synced with server', 'info');
                    callback(false);
                    
                } else if (diff < 0) {
                    st.debug(space + ': using newer data from server', 'info');
                    st[space] = data;
                    st.storeData(space, false);
                    callback(true);
                    
                } else {
                    st.handleError(st.Msgs.Errors.timestamp);
                    callback(false);
                }
            },
            
            statusCode: {
                400: () => {
                    st.handleError(st.Msgs.Errors.server);
                },
                401: () => {
                    st.Env.authToken = null;
                    st.Env.hasStorage && localStorage.token && delete localStorage.token;
                    st.handleError(st.Msgs.Errors.auth);
                },
                404: () => {
                    st.debug(space + ': no server data found', 'info');
                    st[space] && st.sendData(space);
                },
                500: () => {
                    st.debug('500: Server Error', 'error');
                }
            },
            
            error: () => {
                callback();
            }
        });
    
    // If no token then can’t get data
    } else {
        st.handleError(st.Msgs.Errors.token);
        callback();
    }
};



/**
 *  Sends namespace data to server via POST request.
 *  @param {string} space - Denotes namespace object
 *  @param {function} [success] - Callback to run if send succeeds
 *  @param {function} [error] - Callback to run if send fails
 */

st.sendData = function (space, success, error) {
    st.debug(`sendData(${space})`);
    
    function errorCallback() {
        typeof error === 'function' && error();
    }

    // If no network connection then inform
    if (!st.Env.connection) {
        st.debug(st.Msgs.Warnings.connection, 'warning');
        errorCallback();
    
    } else if (!st.Env.authToken) {
        st.handleError(st.Msgs.Errors.token);
        errorCallback();
    
    // Send data if network connection and token exist
    } else {
        $.ajax(`${st.SERVER}${space}?token=${st.Env.authToken}`, {
            type: 'POST',
            data: JSON.stringify(st[space]),
            contentType: 'application/json',
            success: success,
            
            statusCode: {
                400: () => {
                    st.handleError(st.Msgs.Errors.server);
                },
                401: () => {
                    st.Env.authToken = null;
                    st.Env.hasStorage && localStorage.token && delete localStorage.token;
                    st.handleError(st.Msgs.Errors.auth);
                },
                500: () => {
                    st.debug('500: Server Error', 'error');
                }
            },
            
            error: () => {
                //st.retrieveData(space);
                errorCallback();
            }
        });
    }
};



/**
 *  Sends the contents of the Errors namespace to the server.
 */

st.sendErrors = function () {
    st.debug('sendErrors()');
    
    if (st.Env.connection) {
        st.prepareErrors();

        $.ajax(st.SERVER + 'Errors', {
            type: 'POST',
            data: JSON.stringify(st.Errors),
            contentType: 'application/json',

            success: () => {
                st.debug('Errors sent to server', 'info');
                st.restoreDefaults('Errors');
            }
        });

    } else {
        st.debug(st.Msgs.Warnings.connection, 'warning');
    }
};



/**
 *  Stores namespace data to localStorage if possible.
 *  @param {string} [space] - Default is current story
 *  @param {boolean} [sync=false] - Try to sync data with server
 *  @param {boolean} [init=false] - Indicates data is default from init
 */

st.storeData = function (space = st.Env.current, sync = false, init = false) {
    st.debug(`storeData(${space}, ${sync}, ${init})`);
    
    // If not storing default data (initData)
    if (!init) {
        if (st.SYNC.includes(space) || st.User.reading.includes(space)) {
            
            // Update namespace timestamp
            st[space].updated = moment.utc();

            // Send namespace data to server if appropriate
            if (sync && st.Prefs.sync) {
                st.sendData(space);
            }
        }
    }
    
    // Store to localStorage if available
    if (st.Env.hasStorage) {
        localStorage[space] = JSON.stringify(st[space]);
    }
};
