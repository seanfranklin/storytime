'use strict';


/**
 *  Checks with server to see if user has confirmed email address.
 */

st.checkAuth = function (email, uuid, success, error) {
    st.debug(`checkAuth(${email}, ${uuid})`);
    
    $.ajax(`${st.SERVER}Auth?email=${email}&uuid=${uuid}`, {
        type: 'GET',
        success: data => success(data.token),
        error
    });
};



/**
 *  Manages checking sync authorization with server.
 */

st.checkSync = function () {
    st.debug('checkSync()');
    
    // If sync token exists or sync is already on then just toggle sync
    if (st.Env.authToken || st.Prefs.sync) {
        st.togglePref('sync', true);
        
    // If have email and uuid but no token then check auth with server to get token
    } else if (st.User.email && st.Env.uuid) {
        st.showModal('sync-wait', {
            message: '…'
        });
        
        st.checkAuth(st.User.email, st.Env.uuid, token => {
            st.Env.authToken = token;

            if (st.Env.hasStorage) {
                localStorage.token = token;
            }

            st.showModal('sync-done', {
                message: st.Msgs.Modal.d01,
                speech: 'Yeah!'
            });
            
        }, () => {
            st.showModal('sync-check');
            !st.Env.syncAttempts && st.multiCheck(st.User.email, st.Env.uuid, 30);
        });
    
    // If don’t have token or email and uuid then ask for email
    } else {
        st.showModal('sync-beta', {
            message: '<div style="font-size: 0.8em"><p>Sync is currently in beta. This means it is mostly stable but could break at any time.</p><p>Sync uses your email address and sends you a validation link — no password necessary.' +
                     (st.Env.platform === 'Web' ? ' If you clear your browser’s data you may need to validate again.</p>' : '</p>') +
                     (st.Env.platform === 'Android' ? '<p>Note: If you only use the Google Play Store version of the StoryTime app then you don’t need this feature to sync purchases across Android devices that use the same Google account.</p>' : '') +
                     '</div>',
            
            buttons: [{
                click: "st.showModal('email')",
                text: 'I Understand'
            }, {
                click: 'st.closeModal()',
                text: 'Cancel'
            }]
        });
    }
};



/**
 *  Starts process of confirming user’s email address.
 */

st.confirmEmail = function () {
    st.debug('confirmEmail()');
    
    const //$MODAL = $('#modal-email'),  // Why?
        //$EMAIL = $('#email'),  //Why?
        EMAIL = $('#email').val(),
        UUID = st.Env.uuid || st.generateUuid(true);
    
    st.updateSpeech(st.Msgs.Speech.aSec);
    st.postEmail(EMAIL, UUID);
};



/**
 *  Generates a standards-compliant uuid of 36 characters (including dashes).
 *  @param {boolean} [save] - Save uuid to localStorage
 *  @return {string} Generated uuid
 */

st.generateUuid = function (save) {
    st.debug(`generateUuid(${save})`);
    
    var date = new Date().getTime(),

        uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
            var rnd = (date + Math.random() * 16) % 16 | 0;
            date = Math.floor(date / 16);
            return (c === 'x' ? rnd : (rnd & 0x7 | 0x8)).toString(16);
        });
    
    st.Env.uuid = uuid;
    
    if (save && st.Env.hasStorage) {
        localStorage.uuid = uuid;
    }
    
    return uuid;
};



/**
 *  Manages the process of checking sync authorization with server multiple times.
 *  @param {string} email - User’s email address
 *  @param {string} uuid - Unique user identifier
 *  @param {number} attempts - Maximum number of attempts remaining
 */

st.multiCheck = function (email, uuid, attempts) {
    st.debug(`multiCheck(${email}, ${uuid}, ${attempts})`);
    
    st.Env.syncAttempts = attempts;
    
    attempts > 0 && setTimeout(() => {
        st.checkAuth(email, uuid, token => {
            st.debug('token: ' + token, 'var');
            st.Env.authToken = token;

            if (st.Env.hasStorage) {
                localStorage.token = token;
                localStorage.uuid && delete localStorage.uuid;
            }

            st.showModal('sync-done', {
                message: st.Msgs.Modal.d01
            });

            st.Env.isMascot && st.updateSpeech(st.Msgs.Speech.sync);
            st.togglePref('sync', true);
            st.Env.syncAttempts = 0;

        }, () => {
            if (attempts === 15 && st.Env.modal === 'sync-check') {
                st.updateSpeech('Did you check your spam folder?');
            }
            
            st.multiCheck(email, uuid, --attempts);
        });
    }, 10000);
};



/**
 *  Sends email address to server via a POST request.
 *  @param {string} [email=st.User.email] - User’s email address
 *  @param {string} [uuid=st.Env.uuid] - Unique user identifier
 */

st.postEmail = function (email = st.User.email, uuid = st.Env.uuid) {
    st.debug(`postEmail(${email}, ${uuid})`);
    
    const DATA = {
        email, uuid,
        timestamp: moment.utc()
    };
    
    function error() {
        st.showModal('sync-fail', {
            message: st.Msgs.Modal.d03,
            speech: st.Msgs.Speech.curse
        });
    }
    
    function success() {
        st.showModal('sync-wait', {
            message: st.Msgs.Modal.d02,
            speech: st.Msgs.Speech.wait
        });
        
        st.User.email = email;
        st.multiCheck(email, uuid, 30);
    }
    
    st.Env.syncAttempts = 0;
    
    // POST email address and accompanying data to server
    // and wait for user to confirm by clicking on link
    $.ajax(st.SERVER + 'Confirm', {
        type: 'POST',
        data: JSON.stringify(DATA),
        contentType: 'application/json',
        error, success
    });
};



/**
 *  Sets environment to allow use of older data from server.
 */
    
st.syncOld = function (useOld) {
    st.debug(`syncOld(${useOld})`);
    
    var message;
    
    st.Env.doSyncOld = useOld;
    
    if (useOld) {
        $.each(st.Sync.Old, prop => {
            st.Sync.Old[prop].resolve();
        });
        
        setTimeout(() => {
            st.Env.doSyncOld = false;
        }, 30000);
        
    } else {
        $.each(st.Sync.Old, prop => {
            st.Sync.Old[prop].reject();
        });
    }
    
    const $MODAL = $('#modal-sync-use');

    if ($MODAL.length) {
        message = useOld ? st.Msgs.Modal.d04 : st.Msgs.Modal.d05;
        
        $MODAL.find('.options')
                .addClass('hide')
                .end()

            .find('.modal-message')
                .html(message);
    }
};



/**
 *  Performs extra processing when sync preference is toggled.
 *  @param {boolean} [sync] - Preference state
 *  @param {boolean} [user] - Indicates pref toggled by user
 */

st.toggleSync = function (sync, user) {
    st.debug(`toggleSync(${sync}, ${user})`);
    
    // Check if there is a network connection
    if (sync && !st.Env.connection) {
        st.debug(st.Msgs.Warnings.connection, 'warning');

        st.showModal('network', {
            message: st.Msgs.Modal.c01,
            speech: 'Can’t sync!'
        });
        
    // Only proceed if sync preference is on and token exists
    } else if (sync && st.Env.authToken) {
        $.each(st.SYNC, index => {
            
            // If syncing User data then run callback afterwards
            if (st.SYNC[index] === 'User') {
                st.retrieveData('User', user, updated => {
                    
                    // Try get server data for stories in progress
                    $.each(st.User.reading, index => {
                        st.retrieveData(st.User.reading[index], user);
                    });

                    // Reload catalog view if updated User from server
                    if (!user && updated && st.Env.current === 'catalog') {
                        st.loadView('catalog');
                    }
                });
                
            // If syncing Profile data then run callback afterwards
            } else if (st.SYNC[index] === 'Profile') {
                st.retrieveData(st.SYNC[index], user, updated => {
                    
                    // Reload descriptions if updated Profile from server
                    if (!user && updated && st.Env.current === 'profile') {
                        st.profileBasics();
                        st.profileFav();
                        st.profileLive();
                    }
                });
                
            // No callback needed for other data spaces
            } else {
                st.retrieveData(st.SYNC[index], user);
            }
        });
    }
};



/**
 *  Validates an email address.
 *  @param {string} email
 *  @return {boolean} Email is valid
 */

st.validateEmail = function (email) {
    st.debug(`validateEmail(${email})`);

    const REGEX = /^(([^<>()\[\]\\.,;:\s@\"]+(\.[^<>()\[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/igm;
    return (email && REGEX.test(email)) ? true : false;
};
