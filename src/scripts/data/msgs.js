/*global st */

st.Msgs = {
    
    Errors: {
        auth: 'Server authorization failed',
        feedback: 'Couldn’t send feedback to server',
        file: 'Could not get file',
        modal: 'Couldn’t determine modal content',
        server: 'Server reported bad request',
        sound: 'Failed loading sound',
        timestamp: 'Couldn’t determine timestamp difference',
        token: 'Cannot send or receive data without a valid token'
    },
    
    Info: {},
    
    Modal: {
        c01: 'Please check your Internet connection and try again.',
        c02: 'Could not complete transaction. Please try again.',
        c03: 'Purchase simulated for testing purposes.',
        c04: 'Your purchase cannot be processed.<br><br>Can you access the Play Store?',
        c05: 'Thanks for your purchase.<br><br>I hope you enjoy the story.',
        d01: 'This device is now setup for sync.',
        d02: 'Please check your email for a confirmation link.',
        d03: 'I’m having trouble talking to the server. Please try again a bit later.',
        d04: 'Roger that, using server data.',
        d05: 'Ok, keeping device data.',
        d06: 'Error successfully reported.<br><br>Thanks for helping to improve StoryTime.',
        d07: 'Error not reported due to a network issue.'
    },
    
    Speech: {
        r01: 'Hello!',
        r02: 'Hi!',
        r03: 'How’s it going?',
        curse: '$#!%@!',
        gotIt: 'Got it?',
        aSec: 'Just a sec …',
        wait: 'I’ll wait …',
        yay: 'Yay!',
        sorry: 'Sorry, can’t help with this one',
        first: 'Try the first one by yourself',
        enjoyed: 'I hope you enjoyed the story',
        sync: 'I’m attempting sync now'
    },
    
    Warnings: {
        connection: 'No network connection',
        print: 'PRINTING IS NOT SUPPORTED.',
        reload: 'Something failed to load due to a connection problem. You might need to reload.',
        storage: 'Local data storage is not available. Progress will be lost if the tab is closed.',
        width: 'Screen width is less than 320 pixels. Some elements may not display properly.'
    }
    
};