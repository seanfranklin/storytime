/*global $, st */

st.Modals = {
    
    credits: {
        message: 'You have <span id="credits"></span> credits.<br>How many credits would you like to buy?',
        speech: '1 read = 1 credit',

        buttons: [{
            click: 'st.buyCredits(5)',
            text: '5 credits for $0.99'
        }, {
            click: 'st.buyCredits(10)',
            text: '10 credits for $1.69'
        }, {
            click: 'st.buyCredits(20)',
            text: '20 credits for $2.79'
        }, {
            click: 'st.closeModal()',
            text: 'Cancel'
        }]
    },
    
    //DEBUG_BEGIN
    feedback: {
        message: 'Some text was selected.',
        speech: 'Dodgy writing?',

        buttons: [{
            click: "$(this).addClass('no-pointer option-deactivated'); st.updateSpeech(st.Msgs.Speech.s04); st.storeFeedback(); st.sendFeedback()",
            icon: 'cloud-upload',
            text: 'Send Feedback'
        }, {
            click: 'st.closeModal()',
            icon: 'times',
            text: 'Cancel'
        }]
    },
    //DEBUG_END
    
    restart: {
        message: 'Restarting this story again will cost 1 credit.<br>You have <span id="credits"></span> credits left.',
        
        buttons: [{
            click: 'st.closeModal(); st.useCredit()',
            icon: 'money',
            text: 'Restart Story'
        }, {
            click: 'st.closeModal()',
            icon: 'times',
            text: 'Cancel'
        }]
    },
    
    'sync-use': {
        message: 'Server data is older than device data.',

        buttons: [{
            click: 'st.syncOld(true)',
            icon: 'cloud-download',
            text: 'Use Server Data'
        }, {
            click: 'st.syncOld(false)',
            icon: 'cloud-upload',
            text: 'Use Device Data'
        }]
    },
    
    email: {
        message: '<span id="instruction">Enter email address to use for sync:</span><br><input id="email" type="email" maxlength="64">',
        speech: 'I’ll keep it to myself',
        
        buttons: [{
            click: "if ($('#email').hasClass('green')) { st.confirmEmail(); } else { st.playSound('invalid'); st.updateSpeech('Um … Really?'); }",
            text: 'Confirm Email'
        }, {
            click: 'st.closeModal()',
            text: 'Cancel'
        }],

        beforeShow: () => {
            const $EMAIL = $('#email'),
            $CONFIRM = $('#modal-email .options .option:nth(0)').addClass('option-deactivated');

            st.Env.syncAttempts = 0;

            $EMAIL.on('input', () => {
                if (st.validateEmail($EMAIL.val())) {
                    $EMAIL.addClass('green');
                    $CONFIRM.removeClass('deactivated');

                } else {
                    $EMAIL.removeClass('green');
                    $CONFIRM.addClass('deactivated');
                }

            }).focus();
        }
    },
    
    'sync-check': {
        message: 'Please check your email for a confirmation link.',
        speech: 'I’ll wait',
        
        buttons: [{
            click: "st.showModal('email')",
            text: 'Use a different email address'
        }, {
            click: "st.postEmail(); st.updateSpeech('I’ve sent another one')",
            text: 'I didn’t receive the email'
        }]
    },

    name: {
        message: 'What’s your name?<br><input autofocus id="name" class="green" type="text" maxlength="64">',
        
        buttons: [{
            click: "st.updateProfile('name', st.escapeHtml($('#name').val()), 'age')",
            text: 'Next'
        }],

        beforeShow: () => { $('#name').focus(); }
    },
    
    age: {
        message: 'How old are you?<br><input autofocus id="age" class="green" type="number" max="64">',
        
        buttons: [{
            click: "if ($('#age').hasClass('green')) { st.updateProfile('age', Math.floor($('#age').val()), 'gender'); } else { st.playSound('invalid'); st.updateSpeech('No, you’re not!'); }",
            text: "Next"
        }],

        beforeShow: () => {
            const $AGE = $('#age'),
                $CONFIRM = $('#modal-age .options .option:nth(0)');

            $AGE.on('input', () => {
                const age = $AGE.val();

                if (age > 200) {
                    $AGE.removeClass('green');
                    $CONFIRM.addClass('deactivated');
                    st.updateSpeech('Yeah, right!');

                } else {
                    $AGE.addClass('green');
                    $CONFIRM.removeClass('deactivated');

                    if (age > 120) {
                        st.updateSpeech('Methuselah!');

                    } else {
                        st.Cache.speech.addClass('hide');
                    }
                }

            }).focus();
        }
    },
    
    gender: {
        message: 'Are you a boy or a girl?',
        
        buttons: [{
            click: "st.updateProfile('gender', 'boy')",
            icon: 'mars',
            text: 'boy'
        }, {
            click: "st.updateProfile('gender', 'girl')",
            icon: 'venus',
            text: 'girl'
        }]
    },
        
    density: {
        message: 'What’s your local area like?',
        
        buttons: [{
            click: "st.updateProfile('density', 'city', 'building')",
            text: 'city'
        }, {
            click: "st.updateProfile('density', 'suburb', 'building')",
            text: 'suburb'
        }, {
            click: "st.updateProfile('density', 'rural', 'building')",
            text: 'rural'
        }]
    },
    
    building: {
        message: 'What type of building do you live in?',
        
        buttons: [{
            click: "st.updateProfile('building', 'house')",
            text: 'house'
        }, {
            click: "st.updateProfile('building', 'apartment')",
            text: 'apartment'
        }, {
            click: "st.updateProfile('building', 'unit')",
            text: 'unit'
        }, {
            click: "st.updateProfile('building', 'hut')",
            text: 'hut'
        }, {
            click: "st.updateProfile('building', 'cottage')",
            text: 'cottage'
        }]
    },
    
    color: {
        mixButtons: true,
        speech: 'What color do you like?',
        
        buttons: [{
            click: "st.updateProfile('color', 'red', 'food')",
            text: 'red'
        }, {
            click: "st.updateProfile('color', 'green', 'food')",
            text: 'green'
        }, {
            click: "st.updateProfile('color', 'yellow', 'food')",
            text: 'yellow'
        }, {
            click: "st.updateProfile('color', 'blue', 'food')",
            text: 'blue'
        }, {
            click: "st.updateProfile('color', 'orange', 'food')",
            text: 'orange'
        }, {
            click: "st.updateProfile('color', 'purple', 'food')",
            text: 'purple'
        }, {
            click: "st.updateProfile('color', 'pink', 'food')",
            text: 'pink'
        }, {
            click: "st.updateProfile('color', 'brown', 'food')",
            text: 'brown'
        }],

        afterShow: () => {
            $('body').append(
               `<div id="image-cache" class="custom">
                    <img src="/images/food/burger.jpg" alt="">
                    <img src="/images/food/curry.jpg" alt="">
                    <img src="/images/food/fruit.jpg" alt="">
                    <img src="/images/food/ice-cream.jpg" alt="">
                    <img src="/images/food/nachos.jpg" alt="">
                    <img src="/images/food/noodles.jpg" alt="">
                    <img src="/images/food/salad.jpg" alt="">
                    <img src="/images/food/soup.jpg" alt="">
                </div>`
            );
        }
    },

    food: {
        mixButtons: true,
        speech: 'What food is yummy?',
        
        buttons: [{
            click: "st.updateProfile('food', 'burgers', 'animal')",
            text: 'burger',
            image: 'jpg'
        }, {
            click: "st.updateProfile('food', 'soup', 'animal')",
            text: 'soup',
            image: 'jpg'
        }, {
            click: "st.updateProfile('food', 'ice-cream', 'animal')",
            text: 'ice-cream',
            image: 'jpg'
        }, {
            click: "st.updateProfile('food', 'fruit', 'animal')",
            text: 'fruit',
            image: 'jpg'
        }, {
            click: "st.updateProfile('food', 'nachos', 'animal')",
            text: 'nachos',
            image: 'jpg'
        }, {
            click: "st.updateProfile('food', 'salad', 'animal')",
            text: 'salad',
            image: 'jpg'
        }, {
            click: "st.updateProfile('food', 'noodles', 'animal')",
            text: 'noodles',
            image: 'jpg'
        }, {
            click: "st.updateProfile('food', 'curry', 'animal')",
            text: 'curry',
            image: 'jpg'
        }],

        afterShow: () => {
            $('#image-cache').html(
               `<img src="/images/animal/owl.svg" alt="">
                <img src="/images/animal/cow.svg" alt="">
                <img src="/images/animal/elephant.svg" alt="">
                <img src="/images/animal/giraffe.svg" alt="">
                <img src="/images/animal/goat.svg" alt="">
                <img src="/images/animal/leopard.svg" alt="">
                <img src="/images/animal/lion.svg" alt="">
                <img src="/images/animal/monkey.svg" alt="">
                <img src="/images/animal/panda.svg" alt="">
                <img src="/images/animal/penguin.svg" alt="">
                <img src="/images/animal/rhino.svg" alt="">
                <img src="/images/animal/squirrel.svg" alt="">`
            );
        }
    },
    
    animal: {
        mixButtons: true,
        speech: 'Pick me!',
        
        buttons: [{
            click: "st.updateProfile('animal', 'cow')",
            text: 'cow',
            image: 'svg'
        }, {
            click: "st.updateProfile('animal', 'elephant')",
            text: 'elephant',
            image: 'svg'
        }, {
            click: "st.updateProfile('animal', 'giraffe')",
            text: 'giraffe',
            image: 'svg'
        }, {
            click: "st.updateProfile('animal', 'goat')",
            text: 'goat',
            image: 'svg'
        }, {
            click: "st.updateProfile('animal', 'leopard')",
            text: 'leopard',
            image: 'svg'
        }, {
            click: "st.updateProfile('animal', 'lion')",
            text: 'lion',
            image: 'svg'
        }, {
            click: "st.updateProfile('animal', 'monkey')",
            text: 'monkey',
            image: 'svg'
        }, {
            click: "st.updateProfile('animal', 'owl')",
            text: 'owl',
            image: 'svg'
        }, {
            click: "st.updateProfile('animal', 'panda')",
            text: 'panda',
            image: 'svg'
        }, {
            click: "st.updateProfile('animal', 'penguin')",
            text: 'penguin',
            image: 'svg'
        }, {
            click: "st.updateProfile('animal', 'rhino')",
            text: 'rhino',
            image: 'svg'
        }, {
            click: "st.updateProfile('animal', 'squirrel')",
            text: 'squirrel',
            image: 'svg'
        }],

        afterShow: () => {
            $('#image-cache').remove();
        }
    }
    
};