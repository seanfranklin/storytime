/*global st */

st.Catalog = {

    Balloons: {
        key:'Balloons',
        title: 'The Lost Balloons',
        shortDesc: 'Help Eric the elephant find his balloons.',
        longDesc: 'Eric the elephant wants to have a party but he’s lost his balloons.',
        ageRating: 3,
        readLevel: 5,
        readTime: 10,
        price: 0.00,
        credits: 0,
        free: true,
        ribbon: 'new',
        decisions: ['away', 'buy', 'coin', 'found', 'give-cracker', 'monkey', 'parrot', 'peanut', 'porcupine'],
        images: ['sobbing', 'man', 'cat', 'monkey', 'monkey-yellow', 'yellow', 'green', 'blue', 'parrot', 'rhino', 'porcupine11', 'porcupine12', 'buy-balloon']
    },
    
    Mower: {
        key: 'Mower',
        title: 'The Hungry Mower',
        shortDesc: 'Mr Mauve has a new mower. A hungry mower.',
        longDesc: 'Mr Mauve has a new mower. A very eager and hungry mower.',
        ageRating: 4,
        readLevel: 6,
        readTime: 20,
        price: 0.00,
        credits: 0,
        free: true,
        ribbon: 'new',
        decisions: ['fate', 'meeting', 'outcome', 'saturday']
    },

    Zombie: {
        key: 'Zombie',
        title: 'The Playful Zombie',
        shortDesc: 'The new zombie in town wants a friend.',
        longDesc: 'There’s a new zombie in town and he wants to make some friends.',
        ageRating: 5,
        readLevel: 6,
        readTime: 20,
        price: 0.00,
        credits: 0,
        free: true,
        ribbon: 'new',
        decisions: ['birthday', 'discuss', 'fail', 'football', 'go-after', 'makeup', 'practise', 'school', 'stream', 'yard']
    },
    
    Queen: {
        key: 'Queen',
        title: 'The Nubian Queen',
        shortDesc: 'A curious Wunkie journeys to a far-off land.',
        longDesc: 'Nuzha the Wunkie seeks adventure in the far-off land of Ashalor.',
        ageRating: 6,
        readLevel: 7,
        readTime: 40,
        price: 0.99,
        credits: 1,
        free: false,
        ribbon: 'soon',
        decisions: ['barge', 'die', 'dream', 'foot', 'goat', 'group', 'left', 'play', 'pull', 'route', 'stream', 'side', 'sleep', 'trail', 'water'],
        images: ['map', 'water']
    },
    
    Biggalo: {
        key: 'Biggalo',
        title: 'Legend of the Biggalo',
        shortDesc: 'Sir Danton is out to find the legendary Biggalo.',
        longDesc: 'Sir Mortias Danton is out to find the truth about the legendary Biggalo.',
        ageRating: 7,
        readLevel: 7,
        readTime: 40,
        price: 0.99,
        credits: 1,
        free: false,
        ribbon: 'soon',
        decisions: ['bar']
    }
    
};
