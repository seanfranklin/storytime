/*global $, st */

st.Cache = {
    
    head:          $('head'),
    badge:         $('#badge'),
    contextIcon:   $('#context-icon'),
    
    //DEBUG_BEGIN
    debugEnv:      $('#debug-env'),
    debugLog:      $('#debug-log'),
    debugScrIcon:  $('#debug-scroll-icon'), 
    debugMenu:     $('#debug-menu'),

    DebugMenu: {
        icon:      $('#debug-menu-icon'),
        verbose:   $('#debug-menu-verbose'),
        functions: $('#debug-menu-functions'),
        vars:      $('#debug-menu-vars'),
        info:      $('#debug-menu-info'),
        events:    $('#debug-menu-events'),
        warnings:  $('#debug-menu-warnings'),
        errors:    $('#debug-menu-errors'),
        scroll:    $('#debug-menu-scroll'),
        env:       $('#debug-menu-env')
    },
    //DEBUG_END

    mascot:        $('#mascot'),
    menu:          $('#menu'),
    shine:         $('#shine'),
    speech:        $('#speech'),
    sky:           $('#sky'),
    title:         $('#title'),
    titleBg:       $('#title-bg'),
    twinkle:       $('#twinkle'),
    
    Menu: {
        icon:      $('#menu-icon'),
        cont:      $('#menu-continue'),
        catalog:   $('#menu-catalog'),
        prefs:     $('#menu-prefs'),
        restart:   $('#menu-restart')
    }
    
};