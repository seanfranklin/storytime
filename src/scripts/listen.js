'use strict';

 
/**
 *  Initializes Hammer recognizers for gesture (touch) events.
 */

st.initGestures = function () {
    st.debug('initGestures()');

    st.Gestures = {
        Messages: {},
        badge:    new Hammer(document.getElementById('badge'), { touchAction: 'none' }),
        title:    new Hammer(document.getElementById('title-wrap')),
        menuIcon: new Hammer(document.getElementById('menu-icon')),
        flow:     new Hammer(document.getElementById('flow'), { touchAction: 'auto' })
    };
    
    
    // Respond to press and tap gestures on badge
    st.Gestures.badge.on('tap', () => {
        st.debug('tap on badge', 'event');

        if (st.Env.isStory) {
            if (st.Env.modal === 'map') {
                st.vibrate();
                st.Cache.map.panzoom('destroy').empty();
                st.showInventory();

            } else if (!st.Env.modal || st.Env.modal === 'inventory') {
                st.vibrate();
                st.showMap();
            }
        }
    
    //RELEASE_BEGIN
    }).on('press', () => {
        if (st.Env.isStory && !st.Env.modal) {
            st.vibrate(30);
            st.showInventory();
        }
    //RELEASE_END

    }).on('swiperight', () => {
        st.debug('swiperight on badge', 'event');
        st.Cache.titleBg.velocity('transition.slideLeftBigIn', { display: null });

    // Default velocity of swipe recognizer is too high for badge
    }).get('swipe').set({ velocity: 0.01 });
    
    
    // Respond to swipeleft gesture on title-wrap
    st.Gestures.title.on('swipeleft', () => {
        st.debug('swipeleft on title', 'event');
        st.Cache.titleBg.velocity('transition.slideLeftBigOut', { display: null });

    // Default velocity of swipe recognizer is too high for title
    }).get('swipe').set({ velocity: 0.01 });
    
    
    // Respond to press and tap gestures on menu-icon
    st.Gestures.menuIcon.on('press', () => {
        st.debug('press on menu icon', 'event');
        st.vibrate(30);
        st.tryView('catalog');

    }).on('tap', () => {
        st.debug('tap on menu icon', 'event');
        st.vibrate();
        st.playSound('menu');
        st.toggleMenu();
    });

    
    // Respond to press gestures on flow
    st.Gestures.flow.on('press', event => {
        st.debug('press on flow', 'event');

        /*DEBUG_BEGIN*/
        if (event.target.id === 'filler') {
            $('#filler').prop('contenteditable', true).focus()
                .blur(() => {
                    $('#filler').prop('contenteditable', false);
                });

        } else {
        /*DEBUG_END*/

            let word = st.wordAtPoint(event.target, event.center.x, event.center.y);  //window.getSelection().toString();
            
            if (word && st.Env.isStory) {
                st.vibrate();
                st.debug('word: ' + word, 'var');

                if (st.Env.connection) {
                    st.Env.platform === 'Android' && window.getSelection().removeAllRanges();
                    
                    st.showDef(word, false, () => {
                        st.defineWord(word);
                    });
                    
                } else {
                    st.showModal('network', {
                        message: 'Cannot look up word.<br><br>' + st.Msgs.Modal.c01
                    });
                }
            }
        }  //DEBUG

    // Catalog is first view to show and it doesn’t use press on flow
    }).get('press').set({ enable: false });


    // Respond to panup gestures on flow
    st.Gestures.flow.on('panup', event => {
        const PAN_X = event.center.x / window.innerWidth * 100,
            OFFSET = (window[st.vShorter()] / 10) / window.innerWidth * 100;

        event.preventDefault();
        st.debug(`panup at ${parseInt(PAN_X, 10)}%`, 'event');
        st.Gestures.flow.get('pan').set({ enable: false });
        st.Env.isMascot || st.showDecisionTip(PAN_X - OFFSET);

    // The default pan settings are not suitable for summoning mascot
    }).get('pan').set({
        direction: Hammer.DIRECTION_VERTICAL,
        threshold: 60,
        //velocity: 0.4,
        enable: false
    });
    

    // Respond to pinch gestures on flow if device is touch-enabled
    st.Env.hasTouch && st.Gestures.flow.on('pinchin', event => {
        event.preventDefault();
        
        if (event.eventType === Hammer.INPUT_END) {
            st.debug('pinchin on flow', 'event');
            st.Env.isStory && st.resizeText('smaller');
        }

    }).on('pinchout', event => {
        event.preventDefault();
        
        if (event.eventType === Hammer.INPUT_END) {
            st.debug('pinchout on flow', 'event');
            st.Env.isStory && st.resizeText('larger');
        }
        
    }).on('pinchstart', () => {
        st.debug('pinchstart on flow', 'event', true);
        st.userScroll(false);
    
    }).on('pinchend', () => {
        st.debug('pinchend on flow', 'event', true);
        st.userScroll(true);
      
    // Pinch is a blocking gesture so has to be enabled explicitly
    }).get('pinch').set({ enable: true });
};



/**
 *  Attach multiple event listeners to document.
 */

 st.initDocListeners = function () {
    st.debug('initDocListeners()');

    document.addEventListener('online', () => {
        st.debug('online', 'event');
        st.Env.connection = st.checkConnection();
        $.isEmptyObject(st.Feedback) || st.sendFeedback();  //DEBUG
        st.Env.hasTouch && st.Env.isStory && st.Cache.contextIcon.removeClass('hide');  //DEBUG
    }, false);
    
    document.addEventListener('offline', () => {
        st.debug('offline', 'event');
        st.Env.connection = st.checkConnection();
        st.Env.hasTouch && st.Env.isStory && st.Cache.contextIcon.addClass('hide');  //DEBUG
    }, false);

    //WEB_BEGIN
    document.addEventListener('visibilitychange', () => {
        st.debug('visibilitychange', 'event');
        
        if (document.hidden) {
            st.pause();

        } else {
            st.resume();
        }
    }, false);
    //WEB_END
};



/**
 *  Respond to click events on menu items.
 */

st.initMenuListener = function () {
    st.debug('initMenuListener()');

    st.Cache.menu.on('click', 'div', function (event) {
        var menuItem = this.id.substring(5, this.id.length);

        event.stopPropagation();
        st.debug(`click on menu-${menuItem}`, 'event');

        if (st.Env.loading.length) {
            st.invalidAction();

        } else {
            menuItem === 'sound' || st.playSound('click');

            switch (menuItem) {
                case 'continue':
                    st.loadStory(st.User.reading[st.User.reading.length - 1]);
                    break;

                case 'restart':
                    st.restartStory();
                    break;

                case 'catalog':
                case 'prefs':
                    st.loadView(menuItem);
                    break;

                case 'sound':
                    if (!st.Prefs.sound) { st.Sounds.click.play(); }
                    st.togglePref('sound');
                    break;

                //WEB_BEGIN
                case 'popup':
                    st.popupApp();
                    break;
                //WEB_END

                case 'close':
                    st.closeApp();
                    break;

                default:
                    st.handleError('Click on invalid menu item', menuItem);
            }
        }
    });
};



/**
 *  Attach multiple event listeners to window.
 */

st.initWindowListeners = function () {
    st.debug('initWindowListeners()');

    $(window).on('load', () => {
        st.debug('window load', 'event');
        st.Init.window.resolve();

    // User is discouraged from trying to print app content
    }).on('beforeprint', () => {
        st.debug('beforeprint', 'event');
        st[(st.Env.isView ? 'show' : 'queue') + 'Message']('print');

    //WEB_BEGIN
    }).on('popstate', (event) => {
        var state = event.originalEvent.state;
        st.debug('popstate', 'event');
        st.debug('state: ' + state, 'var', true);
        state && st.loadState(state);
    //WEB_END

    // Some fonts and elements need resizing when window size changes
    }).resize(() => {
        st.debug('window resize', 'event', true);

        if (st.Env.doResize === true) {  //WEB
            st.resize();

        //WEB_BEGIN
        // If resize not explicitly true then debounce
        } else {
            st.Env.doResize && clearTimeout(st.Env.doResize);
            st.Env.doResize = setTimeout(st.resize, 100);
        }
        //WEB_END

    // Respond to certain keydown events according to context
    }).keydown(event => {
        const $FOCUS = $(':focus');  //DEBUG

        if (event.which === 27) {
            st.debug('keydown (Esc)', 'event', true);
            
            //DEBUG_BEGIN
            if ($FOCUS.is('#filler')) {
                $FOCUS.blur();

            } else {
            //DEBUG_END
                st.navBack();
            }  //DEBUG
        }

        if ($(':focus').is('input')) {
            st.debug('keydown in input', 'event', true);

        //DEBUG_BEGIN
        } else if ($FOCUS.is('#filler')) {
            // Allow keyboard input into contenteditable element

        } else if (event.which === 68) {
            event.preventDefault();
            st.debug('keydown (D)', 'event', true);
            st.toggleDebug();
        //DEBUG_END

        } else if (event.which === 67) {
            event.preventDefault();
            st.debug('keydown (C)', 'event', true);
            st.tryView('catalog');

        } else if (event.which === 80) {
            event.preventDefault();
            st.debug('keydown (P)', 'event', true);
            st.tryView('prefs');

        } else if (event.which === 112) {
            event.preventDefault();
            st.debug('keydown (F1)', 'event', true);
            
            if (st.Env.modal === 'keyboard') {
                st.closeModal();

            } else {
                if (st.Modals.keyboard) {
                    st.showModal('keyboard');
                    
                } else {
                    st.loadModal('keyboard', () => {
                        st.showModal('keyboard');
                    });
                }
            }

        } else if (st.Env.isStory) {
            if (event.which === 72) {
                event.preventDefault();
                st.debug('keydown (H)', 'event');
                
                if (st.Env.isMascot) {
                    st.hideMascot();

                } else if (($('.decision:last').offset().top - $(window).scrollTop() - window.innerHeight) < -50) {
                    st.showDecisionTip();

                } else {
                    st.playSound('invalid');
                }

            } else if (event.which === 73) {
                event.preventDefault();
                st.debug('keydown (I)', 'event');
                
                if (st.Env.modal === 'inventory') {
                    st.closeModal();

                } else {
                    st.Env.modal === 'map' && st.Cache.map.panzoom('destroy').empty();
                    st.showInventory();
                }

            } else if (event.which === 77) {
                event.preventDefault();
                st.debug('keydown (M)', 'event');

                if (st.Env.modal === 'map') {
                    st.closeModal();

                } else {
                    st.showMap();
                }

            } else if (event.which === 173 || event.which === 109) {
                event.preventDefault();
                st.debug('keydown (-)', 'event');
                st.resizeText('smaller');

            } else if (event.which === 61 || event.which === 107) {
                event.preventDefault();
                st.debug('keydown (+)', 'event');
                st.resizeText('larger');
            }
        }
    });
};
