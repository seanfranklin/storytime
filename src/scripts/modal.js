'use strict';


/**
 *  Closes a modal (or all modals if no modal is passed).
 *  @param {object} [$modal] - jQuery DOM object of modal
 *  @param {function} [callback]
 */

st.closeModal = function ($modal = $('.modal'), callback = null) {
    st.debug('closeModal()');

    $.each(st.Env.beforeClose, (index, beforeCloseFunc) => {
        st.debug('beforeCloseFunc()', 'function', true);
        typeof beforeCloseFunc === 'function' && beforeCloseFunc();
    });

    st.Env.beforeClose = [];
    st.updateSpeech(null);  // Adds 'hide' class to bubble twice?
    st.hideMascot();
    
    /* Re-enable back button (causes reload of story/view)
    $(window).off('popstate.modal');
    history.back(); */

    $modal.velocity({ opacity: 0 }, {
        display: 'none',
        
        complete: () => {
            $modal.remove();
            st.Env.modal = false;
            !st.Env.isScroll && st.userScroll(true);

            $.each(st.Env.afterClose, (index, afterCloseFunc) => {
                st.debug('afterCloseFunc()', 'function', true);
                typeof afterCloseFunc === 'function' && afterCloseFunc();
            });

            st.Env.afterClose = [];
            typeof callback === 'function' && callback();
        }
    }, 200);
};



/**
 *  Loads data for the passed modal from its HTML file and saves it in the Cache.
 *  @param {string} modal - HTML file to load
 *  @param {function} [callback] - Called if load succeeds
 */

st.loadModal = function (modal, callback) {
    st.debug(`loadModal(${modal})`);
    
    $.get(`/modals/${modal}.html`, data => {
        st.Modals[modal] = {
            content: data
        };

        typeof callback === 'function' && callback();

    }).fail(() => {
        st.handleError(st.Msgs.Errors.file, modal);
    });
};



/**
 *  Mixes up buttons by randomly positioning them in the window.
 *  @param {string} buttons - HTML defining buttons
 *  @param {number} [mascotLeft] - Horizontal position of mascot in px
 *  @return {Object} jQuery object defining mixed up buttons
 */

st.mixButtons = function (buttons, mascotLeft) {
    st.debug(`mixButtons(buttons, ${mascotLeft})`);
    
    const MASCOT_POS = st.Cache.mascot.offset(),
        MASCOT_WIDTH = st.Cache.mascot.width(),
        MASCOT_HEIGHT = st.Cache.mascot.height(),
        LONGER = window.innerWidth > window.innerHeight ? 'innerWidth' : 'innerHeight',
        SHORTER = (LONGER === 'innerWidth') ? 'innerHeight' : 'innerWidth',
        V_UNIT = (LONGER === 'innerWidth') ? 'vw' : 'vh',
        $BUTTONS = $(buttons);

    var positions = [];
    
    st.debug('MASCOT_POS: ' + JSON.stringify(MASCOT_POS), 'var');
    
    // Add coordinates of mascot to positions array
    // At some point edit to add coordinates of speech bubble as well
    positions.push({
        width: MASCOT_WIDTH * 2,
        height: MASCOT_HEIGHT * 2,
        left: (mascotLeft || MASCOT_POS.left) - 20,

        // Calculate top depending on whether mascot is visible
        top: MASCOT_POS.top - (MASCOT_POS.top >= window.innerHeight ?
                MASCOT_HEIGHT * 2 : MASCOT_HEIGHT) - 20
    });
    
    // Place each button at a random position in the window
    $BUTTONS.each(function () {
        var tries = 0,
            placed = false,
            
            coords = {
                width: window[LONGER] * 0.15,
                height: window[LONGER] * 0.15
            };
        
        // Try to determine some coordinates where button can be
        // placed without it overlapping other buttons too much
        while (!placed && tries < 100) {
            coords.left = parseInt(Math.random() * ((window.innerWidth - coords.width) * 0.9));
            coords.top = parseInt(Math.random() * ((window.innerHeight - coords.height) * 0.9));
            placed = true;
            
            /*jshint loopfunc: true */
            $.each(positions, function () {
                if (coords.left <= (this.left + this.width + 20) &&
                        (coords.left + coords.width) >= this.left &&
                        coords.top <= (this.top + this.height + 20) &&
                        (coords.top + coords.height) >= this.top) {

                    placed = false;
                }
            });
            /*jshint loopfunc: false */
            
            tries++;
        }

        // If button coordinates could not be determined in a
        // reasonable number of tries then don’t display it
        if (tries === 100) {
            $(this).css('display', 'none');
        
        // If button coordinates are fine then record coordinates
        // and use them to set button's position via css
        } else {
            positions.push(coords);
        
            $(this).css({
                position: 'absolute',
                left: coords.left / window.innerWidth * 100 + '%',
                top: coords.top / window.innerHeight * 100 + '%',
                minWidth: 0,
                minHeight: 0,
                width: 15 + V_UNIT,
                height: 15 + V_UNIT,
                transform: `rotate(${st.rndInt(-30, 30)}deg)`
            });
        }
    });
    
    return $BUTTONS;
};



/**
 *  Shows a full–window modal that must be interacted with.
 *  Either creates a new modal or updates content of an existing modal.
 *  If an Args object is passed then its properties will override
 *  properties of the same name specified in the Modals namespace.
 *  @param {string} modal - Namespace object or ID
 *  @param {Object} [Args] - Object containing modal properties
*/

st.showModal = function (modal, Args) {
    st.debug(`showModal(${modal})`);
    
    var content,        // string of HTML content to display in modal
        message,        // string to display at top of modal content
        options,        // string of HTML to display as user options
        buttons,        // object of button definitions to display as options
        speech,         // string to appear in mascot’s speech bubble
        header,         // show header on top of modal (default is null)
        shuffle,        // shuffle button order (default is null)
        keepOptions,    // keep visible options when updating (default is null)
        mixButtons,     // randomly mix up button positions (default is null)
        beforeShow,     // function to run just before modal is made visible
        afterShow,      // function to run just after modal is made visible
        beforeClose,    // function to run just before modal is closed
        afterClose,     // function to run just after modal is closed
        leftPercent,    // number denoting horizontal position of mascot
        createNew,      // create new modal (default is true if no modal showing)
        $modal,         // jQuery DOM object of the entire modal
        $center,        // jQuery DOM object of div that holds modal content
        $options;       // jQuery DOM object of div that holds user options
        

    function lastly() {
        // A click on an option must not close modal
        $modal.find('.option, input').on('click.modal', event => {
            event.stopPropagation();    
            st.debug('click on modal option', 'event');
            
            // Should beforeClose run here?
            if (typeof beforeClose === 'function') {
                st.removeFromArray(beforeClose, st.Env.beforeClose);
                beforeClose();
            }
        });
        
        typeof beforeClose === 'function' && (st.Env.beforeClose.push(beforeClose));
        typeof afterClose === 'function' && (st.Env.afterClose.push(afterClose));
        typeof afterShow === 'function' && afterShow();
    }


    function display() {
        $modal.velocity({ opacity: 1 }, { display: 'table', complete: () => {
            
            // A click anywhere except an option closes modal
            $modal.one('click.modal', event => {
                event.stopPropagation();
                st.debug('click on modal', 'event');
                st.closeModal($modal, afterClose);      
            });

            speech && st.showMascot(leftPercent, () => {
                st.updateSpeech(speech);
            });

            lastly();
        }});
    }


    function modalDefault(prop) {
        return st.Modals[modal] ? st.Modals[modal][prop] : null;
    }


    Args = Args || {};
    createNew   = (Args.createNew === undefined) ? !st.Env.modal : Args.createNew;

    content     = Args.content     || modalDefault('content');
    message     = Args.message     || modalDefault('message');
    options     = Args.options     || modalDefault('options');
    buttons     = Args.buttons     || modalDefault('buttons');
    speech      = Args.speech      || modalDefault('speech');
    header      = Args.header      || modalDefault('header');
    shuffle     = Args.shuffle     || modalDefault('shuffle');
    keepOptions = Args.keepOptions || modalDefault('keepOptions');
    mixButtons  = Args.mixButtons  || modalDefault('mixButtons');
    beforeShow  = Args.beforeShow  || modalDefault('beforeShow');
    afterShow   = Args.afterShow   || modalDefault('afterShow');
    beforeClose = Args.beforeClose || modalDefault('beforeClose');
    afterClose  = Args.afterClose  || modalDefault('afterClose');

    st.debug('createNew: ' + createNew, 'var', true);
    st.debug('content: ' + (content ? content.substring(0, 19) : content), 'var', true);
    st.debug('message: ' + (message ? message.substring(0, 19) : message), 'var', true);
    st.debug('speech: ' + speech, 'var', true);
    st.debug('keepOptions: ' + keepOptions, 'var', true);
    st.debug('mixButtons: ' + mixButtons, 'var', true);

    if (createNew) {
        /* Disable back button while modal is showing
        history.pushState(null, null);

        $(window).on('popstate.modal', function () {
            history.pushState(null, null);
        }); */

        // Create modal div and center div that holds all content
        $('body').append(`<div id="modal-${modal}" class="modal" role="dialog"><div class="center"></div></div>`);
    }

    $modal = $('#modal-' + (createNew ? modal : st.Env.modal));
    $center = $modal.find('.center');

    if (!createNew) {
        if (!st.Env.modal) {
            st.debug('createNew is false and no modal to update', 'warning');
            return;
        }

        $modal.attr('id', 'modal-' + modal);
        keepOptions || $center.find('.options').addClass('hide').removeClass('options');
    }
    
    st.Env.modal = modal;
    content && $center.html(content);

    if (message) {
        if (createNew || !$center.find('.modal-message').length) {
            $center.append(`<div class="modal-message">${message}</div>`);
        
        } else {
            $center.find('.modal-message').html(message);
        }    
    }

    if (options) {
        $options = $center.append(`<div class="options">${options}</div>`).find('.options');
    }

    if (buttons) {
        $options = $options ? $options.append('<br>') : $center.append('<div class="options">').find('.options');

        if (mixButtons) {
            let leftPx;

            if (createNew) {
                // mixButtons depends on mascot’s position
                leftPx = st.rndInt(5, window.innerWidth * 0.80 - 5);
                leftPercent = leftPx / window.innerWidth * 100;
            }

            $options.append(st.mixButtons(st.createButtons(buttons, false), leftPx));

        } else if (shuffle) {
            $options.append(st.createButtons(buttons)).shuffle();

        } else {
            $options.append(st.createButtons(buttons));
        }
    }
    
    typeof beforeShow === 'function' && beforeShow();

    if (createNew) {
        
        // Scrolling is enabled again in closeModal
        st.userScroll(false);
        display();

    } else {
        st.updateSpeech(speech);
        lastly();
    }
};



/**
 *  Shows the world map (region) modal.
 */

st.showWorld = function () {
    st.debug('st.showWorld()');

    st.Init.world = $.Deferred();

    st.showModal('region', {
        speech: 'It’s a small world',

        afterShow: () => {
            st.Init.world.done(st.resizeRegion);
        }
    });
};
