'use strict';


/**
 *  Converts passed boolean into string denoting the preference state.
 *  @return {string} 'off' or 'on'
 */

st.prefState = function (state) {
    st.debug(`prefState(${state})`);
    
    return state ? 'on' : 'off';
};


    
/**
 *  Resizes story text.
 *  @param {string} [size=medium]
 */

st.resizeText = function (size = 'medium') {
    st.debug(`resizeText(${size})`);
    
    const SIZES = ['tiny', 'small', 'medium', 'large', 'huge'];
    
    if (size === 'smaller' && st.Prefs.text !== 'tiny') {
        size = SIZES[SIZES.indexOf(st.Prefs.text) - 1];
        st.debug('size: ' + size, 'var');
        
    } else if (size === 'larger' && st.Prefs.text !== 'huge') {
        size = SIZES[SIZES.indexOf(st.Prefs.text) + 1];
        st.debug('size: ' + size, 'var');
    }
    
    if (SIZES.includes(size)) {
        $('#flow').removeClass(st.Prefs.text).addClass(size);
        st.Prefs.text = size;
        st.storeData('Prefs');
    }
};



/**
 *  Toggles word(s) in element between US and GB English dialects.
 *  @param elem - HTML element to toggle
 *  @param {string} [dialect] - Dialect to change word(s) to
 *  @return {string} Word(s)
 */

st.toggleDialect = function (elem, dialect) {
    st.debug('toggleDialect(' + elem + ', ' + dialect + ')', 'function', true);
    
    elem = $(elem);
    return elem.data(dialect || (elem.html() === elem.data('us') ? 'gb' : 'us'));
};



/**
 *  Toggles display of labels in the menu.
 */

st.toggleLabels = function () {
    st.debug('toggleLabels()');
   
    st.Cache.menu.toggleClass('no-labels').find('div').toggleClass('icon');
    st.Cache.debugMenu.toggleClass('no-labels').find('div').toggleClass('icon');  //DEBUG
};



/**
 *  Toggles a preference between false (off) and true (on).
 *  Calls preference toggle function (if exists) for extra processing.
 *  @param {string} pref - Preference
 *  @param {boolean} [user] - Indicates pref toggled by user
 */

st.togglePref = function (pref, user) {
    st.debug(`togglePref(${pref}, ${user})`);

    const STATE = !st.Prefs[pref],
        $OPTION = $('#pref-' + pref + ' .option');
    
    st.Prefs[pref] = STATE;
    st.storeData('Prefs');
    
    $OPTION.toggleClass('chosen').html(st.prefState(STATE));
    pref = st.capitalize(pref);
    
    st['toggle' + pref] && st['toggle' + pref](STATE, user);
};



/**
 *  Performs extra processing when serif preference is toggled.
 *  @param {boolean} [serif] - Preference state
 */

st.toggleSerif = function (serif) {
    st.debug(`toggleSerif(${serif})`);
    
    if (serif) {
        $('body').css('font-family', 'Gentium').addClass('serif');
        
    } else {
        $('body').css('font-family', 'FocusLight').removeClass('serif');
    }
};



/**
 *  Performs extra processing when sound preference is toggled.
 *  Toggles appearance of sound icon in menu.
 */

st.toggleSound = function () {
	st.debug('toggleSound()');
    
    var off, on;
    
    if (st.Env.hasTouch) {
        off = 'fa-volume-off';
        on = 'fa-volume-up';
        
    } else {
        off = 'fa-volume-up';
        on = 'fa-volume-off';
    }
    
    const $LABEL = $('#menu-sound .label'),
        $ICON = $('#menu-sound .fa');

    if (st.Prefs.sound) {
        $LABEL.html('Turn Sound Off');
        $ICON.removeClass(off);
        $ICON.addClass(on);
        //typeof Howler !== 'undefined' && Howler.unmute();
        
    } else {
        $LABEL.html('Turn Sound On');
        $ICON.removeClass(on);
        $ICON.addClass(off);
        //typeof Howler !== 'undefined' && Howler.mute();
    }
};
