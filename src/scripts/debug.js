'use strict';


console.event = function(logInfo) {
    console.log('%c%s', 'color: #090', logInfo);
};

console.function = function(logInfo) {
    console.log('%c%s', 'color: grey', logInfo);
};

console.heading = function(logInfo) {
    console.log('%c%s', 'color: black; font-weight: bold', logInfo);
};

console.var = function(logInfo) {
    console.log('%c%s', 'color: purple', logInfo);
};

console.warning = console.warn;



/**
 *  Outputs debug info to console and debug divs.
 *  @param {string} [logInfo] - Info to output to log
 *  @param {string} [type=function] - Type of info
 *  @param {boolean} [verbose=false] - Indicates info is verbose
 */

st.debug = function (logInfo, type = 'function', verbose = false) {

    // Displays current Env properties on debug-env div
    function debugEnv() {
        var envInfo = '<span class="heading">Environment Properties</span>' +
                '<br>----------------------<br>';
        
        // Adds formatted properties to envInfo string
        // Takes optional array of props to skip
        function addProps(object, skip) {
            $.each(object, (name, value) => {
                if (!skip || !skip.includes(name)) {
                    envInfo += `${name}: <span class="${value ? 'truthy' : 'falsy'}">${value}</span><br>`;
                }
            });
        }

        addProps(st.Env);
        
        if (st[st.Env.current]) {
            envInfo += `<br><span class="heading">${st.Env.current} Properties</span><br>`;
            
            for (let i = 0; i < st.Env.current.length; i++) {
                envInfo += '-';
            }
            
            envInfo += '-----------<br>';
            addProps(st[st.Env.current], ['flow']);
        }
            
        st.Cache.debugEnv.html(envInfo);
    }
    
    // Displays log info on debug-log div
    function debugLog() {
        var classes = type,
            prefix = (type === 'event') ? 'Event: ' : '';

        console[type](logInfo);

        verbose && (classes += ' verbose');

        if (!st.debug.show.includes(type) || (verbose && !st.debug.show.includes('verbose'))) {
            classes += ' hide';
        }

        logInfo = `<div class="${classes}">${prefix}${logInfo}</div>`;
        st.Cache.debugLog.append(logInfo);

        st.debug.scroll && $.Velocity && st.Cache.debugLog.velocity('scroll', {
            duration: 0,
            offset: 5000,
            container: st.Cache.debugLog
        });
    }

    !verbose && st.Env && debugEnv();
    logInfo && debugLog();
};



// These properties track states in the debug view
st.debug.scroll = true;
st.debug.selected = false;
st.debug.show = ['error', 'event', 'function', 'heading', 'info', 'var', 'warning'];



/**
 *  Toggles display of info type in debug-log div.
 *  @param {string} type - Info type/class
 */

st.debugToggle = function (type) {
    var isShowing, $messages;

    st.Cache.DebugMenu[type].toggleClass('show');

    // For better grammar and to avoid using keywords as property names
    // we used the plural form of some types but now we need singular form
    if (type.charAt(type.length - 1) === 's') {
        type = type.substring(0, type.length - 1);
    }

    isShowing = st.debug.show.includes(type);
    $messages = $('#debug-log .' + type);

    if (isShowing) {
        st.removeFromArray(type, st.debug.show);

    } else {
        st.debug.show.push(type);
    }

    if (type === 'verbose' && !isShowing) {
        $messages.each(function () {
            var $message = $(this);

            $.each(st.debug.show, (index, className) => {
                if (className === 'heading' || className === 'verbose') {
                    return true;   // continue

                } else if ($message.hasClass(className)) {
                    $message.removeClass('hide');
                    return false;  // break
                }
            });
        });

    } else {
        if (type !== 'verbose' && !st.debug.show.includes('verbose')) {
            $messages = $messages.not('.verbose');
        }

        $messages[(isShowing ? 'add' : 'remove') + 'Class']('hide');
    }
};



/**
 *  Initializes gesture listeners related to debug.
 */

st.initDebugGestures = function () {
    st.debug('initDebugGestures()');

    st.Gestures.debugMenuIcon = new Hammer(document.getElementById('debug-menu-icon')),

    st.Gestures.debugMenuIcon.on('press', () => {
        st.debug('press on debug menu icon', 'event', true);
        st.vibrate(30);
        st.toggleDebug();

    }).on('tap', () => {
        st.debug('tap on debug menu icon', 'event', true);
        st.vibrate();
        st.playSound('menu');
        st.toggleDebugMenu();
    });

    st.Gestures.badge.on('press', () => {
        st.debug('press on badge', 'event', true);
        st.vibrate();
        st.toggleDebug('show');
    });
};



/**
 *  Initializes event listeners on debug divs and icons.
 */

st.initDebugListeners = function () {
    st.debug('initDebugListeners()');

    // Close debug divs on click
    st.Cache.debugEnv.add(st.Cache.debugLog).click(() => {
        st.debug('click on debug-env', 'event', true);
        st.toggleDebug('hide');
    });

    st.Cache.debugScrIcon.click(() => {
        st.debug('click on debug-scroll-icon', 'event', true);
        st.vibrate();

        st.Cache.debugScrIcon.toggleClass('selected');
        st.Cache.debugEnv.toggleClass('no-pointer');
        st.Cache.debugLog.toggleClass('no-pointer');
    });

    // Respond to clicks on debug menu items
    st.Cache.debugMenu.on('click', 'div', function (event) {
        var menuItem = this.id.substring(11, this.id.length);

        event.stopPropagation();
        st.debug(`click on debug-menu-${menuItem}`, 'event', true);

        switch (menuItem) {
            case 'scroll':
                st.debug.scroll = !st.debug.scroll;
                st.Cache.DebugMenu.scroll.toggleClass('show');
                break;

            case 'verbose':
            case 'functions':
            case 'vars':
            case 'info':
            case 'events':
            case 'warnings':
            case 'errors':
                st.debugToggle(menuItem);
                break;

            case 'env':
                st.toggleDebugMenu('hide');
                st.toggleDebug();
                break;

            case 'close':
                st.toggleDebug('hide');
                break;

            default:
                st.handleError('Click on invalid debug menu item', menuItem);
        }
    });
};



/**
 *  In debug builds the user can select text and send it as feedback.
 */

st.initDebugSelect = function () {
    st.debug('initDebugSelect()');

    st.Env.hasTouch && st.Cache.contextIcon
        .attr('title', 'Select Text')
        .addClass('fa-comment')

        .on('click.touch', () => {
            st.debug('click.touch on context-icon', 'event');
            st.vibrate();

            if (st.debug.selected) {
                st.Cache.contextIcon
                    .removeClass('fa-cloud-upload')
                    .addClass('fa-comment');

            } else {
                st.Cache.contextIcon.toggleClass('selected');

                st.Gestures.flow.set({
                    enable: !st.Cache.contextIcon.hasClass('selected')
                });
            }
        });

    $('#flow').on('mouseup.select touchend.select', event => {
        st.debug(event.type + ' on flow', 'event', true);

        var trunc, selected = window.getSelection().toString();

        if (selected && st.Env.isStory && st.Env.connection) {
            st.debug.selected = true;

            if (selected.length > 100) {
                trunc = selected.substring(0, 99) + ' …';
            }

            st.debug('selected: ' + (trunc || selected), 'var');
            st.Env.platform === 'Android' && window.getSelection().removeAllRanges();

            st.Cache.contextIcon
                .attr('title', 'Send Feedback')
                .addClass('fa-cloud-upload')
                .removeClass('fa-comment selected hide')

                .one('click.select', () => {
                    st.debug('click.select on context-icon', 'event');

                    st.showModal('feedback', {
                        message: `<span data-selected="${selected}">${trunc || selected}</span>`,
                        options: '<select id="comment" class="option"><option value="none">Tag as …</option><option value="spelling">Spelling</option><option value="grammar">Grammar</option><option value="unclear">Unclear</option><option value="superfluous">Superfluous</option><option value="boring">Boring</option><option value="interesting">Interesting</option><option value="funny">Funny</option></select><br>',

                        afterShow: () => {
                            st.Env.hasTouch || st.Cache.contextIcon
                                .addClass('hide')
                                .removeClass('fa-cloud-upload')
                                .attr('title', '');

                            st.debug.selected = false;
                        }
                    });
                });

            st.Gestures.flow.set({ enable: true });
        }
    });
};



/**
 *  Stores user-selected text in Feedback namespace.
 */

st.storeFeedback = function () {
    st.debug('storeFeedback()');

    var $center = $('#modal-feedback .center'),
        text = $center.find('.modal-message span').data('selected'),
        comment = $center.find('#comment').val(),
        timestamp = moment.utc().format('YYYYMMDDhhmmss');

    st.Feedback[timestamp] = { text, comment };
    st.storeData('Feedback');
};



/**
 *  Sends story feedback to the server.
 */

st.sendFeedback = function () {
    st.debug('sendFeedback()');
    
    function success() {
        st.debug('Feedback sent', 'info');
        st.restoreDefaults('Feedback');
        
        isModal && st.showModal('feedback-success', {
            message: 'Thanks! I’ll look it over.',
            speech: 'Word up!'
        });
    }
    
    function error() {
        isModal && st.showModal('feedback-error', {
            message: 'Couldn’t send feedback due to a network issue.',
            speech: st.Msgs.Speech.curse
        });
    }
    
    var isModal = st.Env.modal === 'feedback';

    if(!$.isEmptyObject(st.Feedback)) {
        if (st.Env.connection) {
            isModal && st.updateSpeech(st.Msgs.Speech.aSec);

            $.ajax(st.SERVER + 'Feedback', {
                type: 'POST',
                data: JSON.stringify(st.Feedback),
                contentType: 'application/json',
                success: success,
                error: error
            });

        } else {
            st.debug(st.Msgs.Warnings.connection, 'warning');
            error();
        }

    } else {
        st.debug('No feedback to send', 'info');
    }
};



/**
 *  Tests rndInt function and logs distribution results.
 *  @param {number} [min=0] - Integer specfying bottom of range
 *  @param {number} [max=99] - Integer specifying top of range
 *  @param {number} [iterations=1000] - Number of calls to rndInt
 */

st.testRndInt = function (min = 0, max = 99, iterations = 1000) {
    st.debug(`testRndInt(${min}, ${max}, ${iterations})`);
    
    var results = [];
    
    for (let i = min; i <= max; i++) {
        results.push(0);
    }
    
    for (let j = 0; j < iterations; j++) {
        results[st.rndInt(min, max)]++;
    }
    
    st.debug(results, 'info');
};



/**
 *  Toggles between debug environment and debug log.
 *  Displays debug log if no debug info showing.
 *  Hides all debug info if specified by argument.
 *  @param {string} [action] - show or hide
 */

st.toggleDebug = function (action) {
    st.debug(`toggleDebug(${action})`, 'function', true);
    
    if (action === 'show' || (st.Cache.debugEnv.hasClass('hide') && st.Cache.debugLog.hasClass('hide'))) {
        st.Cache.debugLog
            .css('will-change', 'scroll-position, contents')
            .removeClass('hide');

        st.Cache.DebugMenu.icon.removeClass('hide');
        st.Cache.debugScrIcon.removeClass('hide');
    
    } else if (action === 'hide') {
        st.toggleDebugMenu(true);
        
        st.Cache.debugLog
            .addClass('hide')
            .css('will-change', 'unset');

        st.Cache.debugEnv.addClass('hide');
        st.Cache.DebugMenu.icon.addClass('hide');
        st.Cache.debugScrIcon.addClass('hide');

    } else {
        st.Cache.debugEnv.toggleClass('hide');
        st.Cache.debugLog.toggleClass('hide');

        // Change label for debug-menu-env
        setTimeout(() => {
            var log = st.Cache.debugEnv.hasClass('hide'),
                label = log ? 'Environment' : 'Messages',
                action = log ? 'removeClass' : 'addClass';
            
            st.Cache.DebugMenu.env.find('.label').html(label);
            st.Cache.debugMenu.find('.log-option')[action]('hide');
        }, 250);
    }
};



/**
 *  Toggles display of the debug menu.
 *  @param {boolean} [hide] - Only perform hide action
 */

st.toggleDebugMenu = function (hide) {
    st.debug(`toggleDebugMenu(${hide})`, 'function', true);
    
    var display = st.Cache.debugMenu.css('display');
    
    if (display === 'none') {
        if (!hide) {
            st.Cache.DebugMenu.icon.addClass('bottom-straight');
            
            setTimeout(() => {
                st.Cache.debugMenu.velocity('slideDown', 200);
            }, 100);
        }
        
    } else {
        st.Cache.debugMenu.velocity('slideUp', 200);
        
        setTimeout(() => {
            st.Cache.DebugMenu.icon.removeClass('bottom-straight');
        }, 200);
    }
};
