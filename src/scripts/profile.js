'use strict';


/**
 *  Updates Profile (and modal) with specified answer.
 *  @param {string} question
 *  @param {string} answer
 *  @param {string} [modal] - Next question
 */

st.updateProfile = function (question, answer, modal) {
    st.debug(`updateProfile(${question}, ${answer}, ${modal})`);
    
    var profile, speech, callback;
    
    st.Profile[question] = answer;
    st.storeData('Profile', true);
    
    // Determine what group the question is from
    if (['name', 'age', 'gender'].includes(question)) {
        profile = 'Basics';
        
    } else if (['color', 'food', 'animal'].includes(question)) {
        profile = 'Fav';
        
        // Change mascot to new favourite animal
        if (question === 'animal') {
            callback = function () {
                st.Cache.mascot.css('background', `url(/images/animal/${answer}.svg) center top no-repeat`);
            };
        }
        
    } else if (['region', 'density', 'building'].includes(question)) {
        profile = 'Live';
        
        if (question === 'region') {
            let live, animal = st.Profile.animal || 'owl';

            st.debug('animal: ' + animal, 'var', true);

            speech = 'I live in ';

            switch (animal) {
                case 'giraffe':
                case 'leopard':
                case 'lion':
                case 'rhino':
                    live = 'Africa';
                    break;

                case 'elephant':
                case 'goat':
                case 'monkey':
                case 'panda':
                    live = 'Asia';
                    break;

                case 'owl':
                case 'squirrel':
                    live = 'North America';
                    break;

                case 'cow':
                    live = 'Australasia';
                    break;

                case 'penguin':
                    live = 'Antarctica';
                    break;

                default:
                    live = 'a zoo';
            }

            st.debug('live: ' + live, 'var', true);

            speech += live;
            answer === live && (speech += ' too!');
        }
    }
    
    // If showing profile view then update group description
    if (st.Env.current === 'profile') {
        profile && st['profile' + profile] && st['profile' + profile]();
    }
    
    if (modal && st.Env.modal) {
        st.showModal(modal, {
            speech: speech
        });
        
    } else {
        st.closeModal(undefined, callback);
    }
};
