'use strict';


/**
 *  Appends a story’s Catalog entry to flow.
 *  @param {string} key - name of Catalog object
 *  @param {object} story - Catalog object containing story info
 */

st.appendCatalog = function (key, story) {
    st.debug(`appendCatalog(${key})`);
    
    const $FLOW = $('#flow'),
        STATUS = st.storyStatus(story);

    var $story, Glass,
        click = 'loadStory';
    
    if (STATUS === 'reading') {
        Glass = {
            message: 'There is a bookmark for this story',

            buttons: [{
                click: `st.loadStory('${key}')`,
                icon: 'bookmark',
                text: 'Continue from bookmark'
            }, {
                click: `st.restartStory('${key}')`,
                icon: 'refresh',
                text: 'Restart from beginning'
            }]
        };
        
    } else if (STATUS.includes('$')) {
        Glass = {
            message: 'Purchase this story for ' + STATUS,

            buttons: [{
                click: `st.buyStory('${key}')`,
                icon: 'money',
                text: 'Purchase for ' + STATUS
            }, {
                click: `st.useCredit('${key}')`,
                icon: 'money',
                text: `Use Credit <span style="white-space: nowrap">(${st.User.credits} left)</span>`
            }]
        };
        
    } else if (STATUS === 'soon') {
        Glass = {
            message: 'This story is not available yet',

            buttons: [{
                click: `st.loadStory('${key}')`,
                icon: 'eye',
                text: 'Read Preview'
            }]
        };
    }
    
    if (Glass) {
        click = 'showGlass';
    }
    
    $FLOW.append(`<div id="catalog-${key}" class="view catalog"><div class="catalog-image"><img src="/images/${key}.png" alt="" width="100%"></div><div class="catalog-details"><div class="catalog-title">${story.title}</div><div class="catalog-desc short">${story.shortDesc}</div><div class="catalog-desc long">${story.longDesc}</div><div class="catalog-extra"><span class="catalog-age">${story.ageRating}+</span><span class="catalog-level"><span class="fa fa-cogs rotate-90"></span>${story.readLevel}</span><span class="catalog-time"><span class="fa fa-clock-o"></span>${story.readTime}</span><span class="catalog-status">${STATUS}</span></div></div></div>`);
    
    $story = $('#catalog-' + key);
    Glass && st.addGlass($story, Glass, key);
    
    if (typeof story.ribbon === 'string') {
        const SPANS = $.map(story.ribbon.split(''), letter => `<span>${letter}</span>`).join('');
        $story.append(`<div class="catalog-ribbon letters-${story.ribbon.length}">${SPANS}</div>`);
    }
    
    $story.click(event => {
        event.stopPropagation();
        st[click] && st[click](key);

    }).css('opacity', 1);
};



/**
 *  Manages Catalog initialization including appending stories.
 */

st.loadCatalog = function () {
    st.debug('loadCatalog()');
    
    // Append each story entry in Catalog namespace to flow
    $.each(st.Catalog, (key, story) => {
        st.appendCatalog(key, story);
    });
    
    // Add listener that triggers vibrate when Catalog story selected
    st.Env.hasVibration && $('.catalog').on('click.vibrate', st.vibrate);
};



/**
 *  Removes the slider and its configuration from the flow.
 */

st.removeSlider = function () {
    st.debug('removeSlider()');

    st.Env.isSlider = false;

    st.Env.current === 'catalog' && $('#flow')
        .slick('unslick')
        .removeClass('slider')
        .css('top', '80px')

        .find('.filler')
            .css('height', '50px')
            .removeClass('hide')
            .end()

        .find('.message')
            .removeClass('hide')
            .end()

        .find('.catalog')
            .css('opacity', 1);
};



/**
 *  Resizes elements in the catalog view (if necessary).
 */
 
st.resizecatalog = function () {
    st.debug('resizecatalog()');

    const WIDTH = window.innerWidth,
        HEIGHT = window.innerHeight;
    
    if (WIDTH > HEIGHT * 1.3) {
        const $FLOW = $('#flow'),
            $TITLES = $FLOW.find('.catalog-title');
        
        // Apply slider mode if it isn’t already
        if (!st.Env.isSlider) {
            $FLOW.find('.filler, .message')
                .addClass('hide').end()
                .addClass('slider')

                .slick({
                    centerMode: false,
                    slidesToShow: 4,
                    infinite: false
                });

            st.Env.isSlider = true;
        }
        
        // Make sure slider is in center of window
        $FLOW.css('top', (HEIGHT - $FLOW.height()) / 2 + 'px');
        
        // Resize story titles when in slider mode
        $TITLES.css('font-size', $TITLES.width() / 9.6 + 'px');
        
    } else if (st.Env.isSlider) {
        st.removeSlider();

        $('#flow .catalog-title')
            .css('font-size', (WIDTH < 481 ? '0.85' : '0.9') + 'em');
    }
};



/**
 *  Determines the status of a story.
 *  @param {string} [story] - Default is current story
 *  @return {string} Status of story
 */

st.storyStatus = function (story) {
    st.debug(`storyStatus(${story})`);
    
    var status;

    if (!story) {
        story = st.Catalog[st.Env.current];
        
    } else if (typeof story === 'string') {
        story = st.Catalog[story];
    }
    
    // If part-way through story then right to read is inferred
    if (st.User.reading.includes(story.key)) {
        status = 'reading';
    
    // If story not yet released then it will be coming soon
    } else if (story.ribbon === 'soon') {
        status = 'soon';
    
    // Check if story has been purchased
    } else if (st.User.purchases.includes(story.key)) {
        status = 'purchased';
    
    // If story not purchased then check if free to play
    } else if (story.free) {
        status = 'free';
    
    // If story is not free or purchased then show its price
    } else if (story.price) {
        status = '$' + story.price;
    }

    return status;
};
