'use strict';


/**
    Adds an item to a story’s inventory array.
    @param {string} item
    @param {string} [story] - Default is current story
*/

st.addItem = function (item, story = st.Env.current) {
    st.debug(`addItem(${item}, ${story})`);
    
    st[story].inventory.push(item);
};



/**
    Appends a decision to the flow (if its HTML file exists).
    @param {string} decision - Name of decision to append
    @param {function} [callback]
*/

st.appendDecision = function (decision, callback) {
    st.debug(`appendDecision(${decision})`);
    
    const STORY = st.Env.current;
    var html, $decision;
    
    // Processes the decision HTML and flow
    function processDecision() {
        const $FLOW  = $('#flow'),
            USE_DEFAULT_SCRIPT = !html.includes('<script>');

        var fillerHeight;
        
        // If decision data doesn’t have script then append default script
        if (USE_DEFAULT_SCRIPT) {
            html += `<script>$('.decision .option').click(function () { st.vibrate(10); st.loadSection(this.id, '${decision}'); });</script>`;
        }

        $FLOW.append(`<div id="decision-${decision}" class="decision"><div class="decision-inner">${html}</div></div>`);
        $decision = $('#decision-' + decision);

        //WEB_BEGIN
        // Add prefetch links for each decision option
        if (USE_DEFAULT_SCRIPT) {
            const IMAGES = st.Catalog[STORY].images;
            let prefetchLinks = '';            

            $decision.find('.option').each(function () {
                const ID = this.id;
                prefetchLinks += `<link class="prefetch" rel="prefetch" href="${st.Env.currentPath}sections/${ID}.html">`;

                if (IMAGES && IMAGES.includes(ID)) {
                    prefetchLinks += `<link class="prefetch" rel="prefetch" href="${st.Env.currentPath}images/${ID}.png">`;
                }
            });

            st.Cache.head
                .find('.prefetch').remove().end()
                .append(prefetchLinks);
        }
        //WEB_END

        fillerHeight = (window.innerHeight / 2) - ($decision.outerHeight() / 2) - 50;
        //fillerHeight = fillerHeight - parseFloat($FLOW.css('font-size'));  //DEBUG
        $FLOW.append(`<div id="filler" class="filler" style="padding-bottom: ${fillerHeight}px"></div>`);
    }
    
    // Gives decision to callback for further processing or just shows it now
    function showDecision() {
        if (callback) {
            callback($decision);

        } else if ($decision) {
            $decision.css('opacity', 1);
            st.saveProgress();
            //st.removeFromArray(decision, st.Env.loading);
        }
    }
    
    // Gets decision HTML file via AJAX and continues processing if successful
    function getDecisionHTML() {
        $.get(`${st.Env.currentPath}decisions/${decision}.html`).done(data => {
            html = data;
            processDecision();
                   
        }).fail(() => {
            st.handleError(st.Msgs.Errors.file, decision);

        }).always(() => {
            showDecision();
        });
    }

    // Save decision name so can add to flow later if flow restored
    if (st.Env.doSave) {
        st[STORY].decision = decision;
    }
    
    // Get decision’s HTML from Decisions namespace if it
    // exists there, otherwise try and get it from a file
    if (st.Decisions[STORY][decision]) {
        html = st.Decisions[STORY][decision];
        processDecision();
        showDecision();
        
    } else {
        getDecisionHTML();
    }
};



/**
    Appends a section to the flow. (HTML file must exist.)
    @param {string} section - Name of section to append
    @param {string} [decision] - Name of decision to append (Default is section)
    @param {function} [callback]
*/

st.appendSection = function (section, decision = section, callback = null) {
    st.debug(`appendSection(${section}, ${decision})`);
    
    // Get section’s markdown/HTML with an AJAX request and process in callback
    $.get(`${st.Env.currentPath}sections/${section}.md`, data => {
        const $FLOW = $('#flow'),
            CURRENT = st.Env.current,
            IMAGE = `${st.Env.currentPath}images/${section}.png`;

        var $section;
        
        st[CURRENT].section = section;

        // Run story-specific routines for section
        st['section' + CURRENT] && st['section' + CURRENT](section);

        $FLOW.append(`<div id="section-${section}" class="section">${data}</div>`);
        $section = $('#section-' + section);

        // If the section has an image then add its src
        $section.find('.image').attr({
            src: IMAGE,
            alt: section
        });
        
        // If user prefers British English then toggle appropriate words
        st.Prefs.british && $section.find('.dialect').each(function () {
            this.innerHTML = st.toggleDialect(this, 'gb');
        });
        
        //DEBUG_BEGIN
        const COUNT = st.countWords($section);
        
        $section.append(`<div class="name">${section}</div><div class="count">${COUNT}</div>`);
        st[CURRENT].wordCount = (section === 'intro') ? COUNT : st[CURRENT].wordCount + COUNT;
        //DEBUG_END
        
        $FLOW.find('.filler').remove();
        
        // Check story’s list of decisions to determine if passed
        // decision is valid and if so then call appendDecision,
        // which passes back a jQuery object for further processing
        if (st.Catalog[CURRENT].decisions.includes(decision)) {
            st.appendDecision(decision, $decision => {
                if (callback) {
                    callback($section, $decision);
                    
                } else {
                    $section.css('opacity', 1);
                    $decision && $decision.css('opacity', 1);
                    st.saveProgress();
                    st.removeFromArray(section, st.Env.loading);
                }
            });
            
        } else {
            if (callback) {
                callback($section);

            } else {
                $section.css('opacity', 1);
                st.removeFromArray(section, st.Env.loading);
            }
        }
        
    }).fail(() => {
        st.handleError(st.Msgs.Errors.file, section);
    });
};



/**
    Checks if user has right to restart story for free and returns
    true if so, otherwise asks if user wants to spend a credit to restart.
    @param {string} [story] - Default is current story
    @return {boolean} Restart
*/

st.checkRestart = function (story = st.Env.current) {
    st.debug(`checkRestart(${story})`);
    
    var restart;
    
    // If story is free, a preview, or purchased then restarting is free
    if (st.Catalog[story].free || st.User.purchases.includes(story) ||
                st.Catalog[story].ribbon === 'soon') {

        restart = true;
    
    } else {
        // If credit used and story already restarted then give
        // option via modal to use another credit to restart
        if (st.User.restarted.includes(story)) {
            restart = false;

            st.showModal('restart', {
                beforeShow: function () {
                    $('#credits').html(st.User.credits);
                }
            });
       
        // If credit was used and story has not already been
        // restarted then record the restart and allow
        } else {
            restart = true;
            st.User.restarted.push(story);
            st.storeData('User', true);
        }
    }
    
    return restart;
};



/**
    Clears a story’s namespace in memory and localStorage.
    @param {string} story - Default is current story
*/

st.clearStory = function (story = st.Env.current) {
    st.debug(`clearStory(${story})`);
    
    delete st[story];
    st.Env.hasStorage && localStorage.removeItem(story);

    st.removeFromArray(story, st.User.reading);
    st.storeData('User', true);

    st.Env.doClear = false;
};



/**
    Deactivates a decision.
    @param {string} [decision] - Default is active decision
    @param {string} [chosen] - Name of chosen option
*/

st.deactivateDecision = function (decision, chosen) {
    st.debug(`deactivateDecision(${decision}, ${chosen})`);
    
    var $decision;
    
    if (decision) {
        $decision = $('#decision-' + decision + ' .decision-inner');
        
    } else {
        $decision = $('#flow .decision:last');
    }
    
    chosen && $decision.find('#' + chosen).addClass('chosen');
    
    $decision
        .find('.option')
            .addClass('option-deactivated no-pointer')
            .removeClass('option')
            .end()

        .addClass('deactivated')

        // TODO: Use glass to allow reader to change mind
        .append('<div class="glass"></div>');
};



/**
    Should be called by the last decision when a story is finished.
    @param {boolean} [credit] - Give a credit reward
*/

st.finishStory = function (credit) {
    st.debug(`finishStory(${credit})`);
    
    var $decision;
    const STORY = st.Env.current;

    st.Env.doSave = false;
    st.Env.doClear = true;

    //st.Stats.finishes++;
    
    // Ensure credit is not rewarded more than once per story
    if (credit && !st.User.rewarded.includes(STORY)) {
        st.User.credits++;
        st.User.rewarded.push(STORY);
        st.storeData('User');

        $decision = $('.decision-inner:last');
        $decision.append('<hr class="hr-center">For outstanding play you have received a credit.');
    }
    
    if (st[STORY].defs) {
        st.Defs[STORY] = st.Defs[STORY] || {};
        st.Defs[STORY] = st[STORY].defs;

        $decision = $decision || $('.decision-inner:last');
        $decision.append('<hr class="hr-center"><div class="options"><span id="review" class="option multi" onclick="st.reviewDefs()">Review Definitions</span></div>');
    }
};



/**
    Checks if an item is in a story’s inventory array.
    @param {string} item
    @param {string} [story] - Default is current story
*/

st.hasItem = function (item, story = st.Env.current) {
    st.debug(`hasItem(${item}, ${story})`);
    
    return ~st[story].inventory.indexOf(item);
};



/**
    Initializes the current story.
*/

st.initStory = function () {
    st.debug('initStory()');
    
    // Attaches event listeners to decision options
    function attachOptionListers() {
        st.debug('attachOptionListers()', 'function', true);
        st.Cache.Menu.restart.addClass('hide');
        
        $('.decision-inner .option').one('click.init', () => {
            st.debug('Click on first decision option', 'event');
            
            // Activate progress saving
            st.Env.doSave = true;   
            
            //st.Stats.starts++;

            // Deduct credit if necessary
            if (st.Env.doDeduct) {  
                st.Env.doDeduct = false;
                st.User.credits--;
            }
            
            st.Cache.Menu.restart.removeClass('hide');
            
            // Add/move story to end of reading list
            if (st.User.reading.includes(st.Env.current)) {
                st.removeFromArray(st.Env.current, st.User.reading);
            }
            
            st.User.reading.push(st.Env.current);
            st.storeData('User', true);
        });
    }

    // Deactivate progress saving
    st.Env.doSave = false;

    st.loadSection('intro');
    
    // Check if intro (including first decision) has fully loaded
    st.initStory.interval = setInterval(() => {
        if ($('.decision-inner').length) {
            clearInterval(st.initStory.interval);
            attachOptionListers();

        } else {
            st.debug('First decision not yet loaded', 'info', true);
        }
    }, 500);
};



/**
    Manages the loading of new sections and their associated decision.
    @param {string} newSection - Name of new section to load
    @param {string} [refDecision] - Decision that triggered load
    @param {string} [newDecision] - New decision to accompany new section
*/

st.loadSection = function (section, refDecision, newDecision) {
    st.debug(`loadSection(${section}, ${refDecision}, ${newDecision})`);
    
    st.Env.loading.push(section);
    refDecision && st.deactivateDecision(refDecision, section);
    st.Env.isMascot && !st.Env.modal && st.hideMascot();
    $('#filler').addClass('loading');  //WEB
    
    // Call appendSection, which passes back jQuery object(s) of
    // appended section and (maybe) decision for further processing
    st.appendSection(section, newDecision, ($section, $decision) => {
        $section.css('opacity', 1);
        $decision && $decision.css('opacity', 1);
        
        if (refDecision && st.Prefs.scroll) {
            var $refDecision = $('#decision-' + refDecision),
                offset = $refDecision.height() - 40;
            
            setTimeout(() => {
                st.userScroll(false);
                
                $refDecision.velocity('scroll', {
                    duration: 600,
                    offset: offset,

                    complete: () => {
                        st.userScroll(true);
                    }
                });
            }, 600);
        }
        
        $decision && st.saveProgress();
        st.removeFromArray(section, st.Env.loading);
    });
};



/**
    Manages all aspects of loading a story.
    @param {string} story - Short name of story to load
*/

st.loadStory = function (story) {
	st.debug(`loadStory(${story})`);

    const LAST = st.Env.current,
        IS_INIT = (LAST === null) && st.Env.loading.includes('init'),
        WAS_VIEW = st.VIEWS.includes(LAST),
        TITLE = st.Catalog[story].title,
        $BODY = $('body'),
        $FLOW = $('#flow');

    // Must be the last part of loadStory to run
    function loaded() {
        st.debug('loaded()');
        st.Env.hasTouch && st.Env.connection && st.Cache.contextIcon.removeClass('hide');  //DEBUG
        st.Env.isStory = true;
        st.storeData('User', true);

        if (WAS_VIEW || IS_INIT) {
            st.checkBottom.interval = setInterval(st.checkBottom, 300);

            st.Env.hasTouch && $FLOW.on('touchmove.story', () => {
                st.Env.isTouchmove = true;

            }).on('touchend.story touchcancel.story', () => {
                st.Env.isTouchmove = false;
            });
        }

        if (IS_INIT) {
            st.Init.app.resolve();

        } else {
            st.updateBadge(story);  //WEB
        }

        st.removeFromArray(story, st.Env.loading);
    }
    
    st.Env.loading.push(story);

    st.debug('LAST: ' + LAST, 'var', true);
    st.debug('IS_INIT: ' + IS_INIT, 'var', true);
    st.debug('WAS_VIEW: ' + WAS_VIEW, 'var', true);

    if (!IS_INIT) {
        st.updateBadge('gears', 'svg');  //WEB
        
        // Hide menu if showing
        st.toggleMenu(true);
    
        // If coming from a view then do extra processing
        if (WAS_VIEW) {
            st.Cache.sky.addClass('hide');
            $FLOW.addClass('hide story ' + st.Prefs.text);
            
            if (st.Env.isSlider) {
                st.Env.isSlider = false;
                $FLOW.slick('unslick').removeClass('slider').css('top', '80px');
            }

            setTimeout(() => {
                st.clearSky();
                st.Gestures.flow.get('press').set({ enable: true });

                // If coming from a view then adjust menu entries
                st.Cache.Menu.cont.addClass('hide');
                
                if (LAST === 'catalog' || LAST === 'prefs') {
                    st.Cache.Menu[LAST].removeClass('hide');
                }
            }, 400);
        }
        
        st.Cache.titleBg.velocity('fadeOut', 0);

    } else {
        //st.Cache.sky.addClass('hide');
        $FLOW.addClass('story ' + st.Prefs.text);
    }

    history.state === story || history[IS_INIT ? 'replaceState' : 'pushState'](story, TITLE, `/${story}/`);  //WEB
    
    // Update window and header titles
    document.title = TITLE;  //WEB
    st.updateBadge(story);  //APP
    st.Cache.title.html(TITLE);
    st.Cache.badge.attr('title', 'Map');
	
    // Remove custom elements of last story/view
    st.Cache.head.find('.custom, .prefetch').remove();  //WEB
    $BODY.find('.custom').remove();

    // Clear last story’s space
    st.Env.doClear && st.clearStory();
	
    // Update Environment
    st.Env.isView = false;
    st.Env.current = story;
    st.Env.currentPath = `/android_asset/www/stories/${story}/`;  //APP
    st.Env.currentPath = `/stories/${story}/`;  //WEB

    //WEB_BEGIN
    // If story has map then add prefetch link for image file
    if (st.Catalog[story].images && st.Catalog[story].images.includes('map')) {
        st.Cache.head.append(`<link rel="prefetch" class="custom" href="${st.Env.currentPath}images/map.jpg">`);
    }
    //WEB_END
    
    // Get HTML containing story’s resources then do further init in callback
	$.get(`${st.Env.currentPath}${story}.html`, data => {
        $BODY.append(data);
        st['resize' + story] && st['resize' + story]();

        // Title slides in after its background-color is set
        !IS_INIT && st.Cache.titleBg.velocity('transition.slideLeftBigIn', {
            display: 'table',
            duration: 300
        });
        
        // If story’s values aren’t initialized then do it
        st[story] || st.initData(story);
        st[story].hasOwnProperty('british') || (st[story].decisions = st.Prefs.british);
        st[story].hasOwnProperty('decisions') || (st[story].decisions = null);
        st[story].hasOwnProperty('flow') || (st[story].flow = null);
        
        // Simple decisions are loaded now if they aren’t already
        // and complex decisions are loaded on-demand by AJAX
        st.Decisions[story] ||
            (st['init' + story + 'Decisions'] &&
             st['init' + story + 'Decisions']());
        
        st.Msgs[story] ||
            (st['init' + story + 'Msgs'] &&
             st['init' + story + 'Msgs']());
        
        st[story].wordCount = st[story].wordCount || 0;  //DEBUG
        
        // If story has a flow stored in memory then restore it
        if (st[story].flow) {

            // Activate progress saving
            st.Env.doSave = true;

            //st.Stats.continues++;
            
            setTimeout(() => {
                st.Cache.Menu.restart.removeClass('hide');
            }, 400);
            
            // Add/move story to end of reading list
            if (st.User.reading.includes(story)) {
                st.removeFromArray(story, st.User.reading);
            }
            
            st.User.reading.push(story);
            $FLOW.html(st[story].flow);

            // If user has changed dialect preference since starting story
            // then toggle appropriate words in the stored flow
            if ((st.Prefs.british && typeof st[story].british === 'undefined') ||
                    (st[story].british !== st.Prefs.british &&
                    typeof st[story].british !== 'undefined')) {

                let dialect = st.Prefs.british ? 'gb' : 'us';

                st.debug('Prefs.british: ' + st.Prefs.british, 'var', true);
                st.debug(`${story}.british: ${st[story].british}`, 'var', true);
                st.debug('dialect: ' + dialect, 'var', true);

                $FLOW.find('.dialect').each(function () {
                    this.innerHTML = st.toggleDialect(this, dialect);
                });

                st[story].british = st.Prefs.british;
            }

            const $LAST = $FLOW.find('.decision:last');

            // Display flow before filler height is calculated
            $FLOW.velocity({ opacity: 0 }, 0).removeClass('hide');

            // Active decision is not part of stored flow so append it
            st.appendDecision(st[story].decision);

            // Move to last deactivated decision
            $LAST.velocity('scroll', {
                duration: 0,
                offset: -116,

                complete: () => {
                    $FLOW.velocity({ opacity: 1 }, 300);
                }
            });

            loaded();
        
        // If story has no stored flow then init and start from intro
        } else {
            IS_INIT || $FLOW.empty().removeClass('hide');

            // Re-init story’s values if they exist
            st[story] && st.initData(story, true); 

            st.initStory();
            
            if (!IS_INIT && !st.User.seen.includes('tips')) {
                let beforeShow = () => {
                    function closeModal() {
                        st.closeModal($('#modal-tips'), loaded);
                    }

                    st.updateBadge();  //WEB

                    $('#got-it').one('click.option', () => {
                        st.User.seen.push('tips');
                        closeModal();
                    });

                    $('#remind').one('click.option', closeModal);
                };

                if (st.Modals.tips) {
                    st.showModal('tips', {
                        beforeShow,
                        afterClose: loaded
                    });
                    
                } else {
                    st.loadModal('tips', () => {
                        st.showModal('tips', {
                            beforeShow,
                            afterClose: loaded,

                            afterShow: () => {
                                st.Cache.Menu.icon.velocity({
                                    scaleX: 1.05,
                                    scaleY: 1.05
                                }, {
                                    loop: true,
                                    duration: 500
                                });

                                st.Cache.badge.addClass('shine');

                                setTimeout(() => {
                                    st.Cache.badge.removeClass('shine');
                                }, 1000);
                            },

                            beforeClose: () => {
                                st.Cache.Menu.icon
                                    .velocity('stop', true)
                                    .velocity({ scaleX: 1, scaleY: 1 });
                            }
                        });
                    });
                }

            } else {
                loaded();
            }
        }
        
	}).fail(() => {
        st.handleError(st.Msgs.Errors.file, story);
    });
};



/**
    Removes an item from a story’s inventory array.
    @param {string} item
    @param {string} [story] - Default is current story
*/

st.removeItem = function (item, story = st.Env.current) {
    st.debug(`removeItem(${item}, ${story})`);
    
    st.removeFromArray(item, st[story].inventory);
};



/**
    Resizes the filler div underneath the last decision in the flow.
*/

st.resizeFiller = function () {
    st.debug('resizeFiller()');

    const $DECISION = $('#flow .decision:last'),
        HEIGHT = (window.innerHeight / 2) - ($DECISION.outerHeight() / 2) - 50;

    //height = height - parseFloat($('#flow').css('font-size'));  //DEBUG
    st.debug('height: ' + HEIGHT, 'var', true);
    $('#filler').css('padding-bottom', HEIGHT + 'px');
};



st.resizeMap = function () {
    st.debug('resizeMap()');

    st.Cache.map.panzoom('resetDimensions');
};



/**
    Restarts a story from the beginning.
    @param {string} [story] - Default is current story
    @param {boolean} [restart] - Bypass modal and force restart
*/
 
st.restartStory = function (story = st.Env.current, restart = st.checkRestart(story)) {
    st.debug(`restartStory(${story}, ${restart})`);
    
    st.toggleMenu(true);
    
    // Right to restart is verified or forced
    if (restart) {

        // Story to restart is currently open
        if (st.Env.current === story) {
            $('#' + st.Env.current.toLowerCase()).empty();
            $('#flow').empty();
            
            st.clearStory(story);
            st.initData(story, true);
            st.initStory();
            
        // Story to restart is not currently open but has bookmark
        } else if (st.User.reading.includes(story)) {
            st.clearStory(story);
            st.loadStory(story);
            
        // Story has no bookmark and not currently open so not really
        // 'restart' (called from 'use credit' button in catalog view)
        } else {
            st.loadStory(story);
        }
    }
};



/**
    Saves progress through the current story.
*/

st.saveProgress = function () {
    st.debug('saveProgress()');
    
    if (st.Env.doSave) {
        const $FLOW = $('#flow').clone()            // Clone flow
            .find('.filler').remove().end()         // Remove filler
            .find('.decision:last').remove().end()  // Remove decision
            .find('script').remove().end().html();  // Remove scripts
        
        st[st.Env.current].flow = $FLOW;            // Save to space
        st.storeData(st.Env.current, true);         // localStorage
    }
};



/**
    Shows inventory for the current story in a modal.
    @param {object} Args
*/

st.showInventory = function (Args = {}) {
    st.debug('showInventory()');

    /* jshint ignore:start */
    var { inventory, hiddenItems, message, buttons, select, afterSelect } = Args;

    inventory = inventory || st[st.Env.current].inventory;

    if (inventory) {
        const INV_CLONE = $.extend(true, [], inventory);

        message = message || (select ? 'Choose an item:' : '<h2>Inventory</h2>');

        // Remove hidden items from local inventory array
        Array.isArray(hiddenItems) && $.each(hiddenItems, (index, item) => {
            st.removeFromArray(item, INV_CLONE);
        });
        
        // If select mode create button definitions
        if (select) {
            buttons = buttons || [];

            $.each(INV_CLONE, (index, item) => {
                buttons.push({
                    click: `st.Queen.item = '${item}'; $('.modal').click()`,
                    text: item
                });
            });

        } else {
            message += INV_CLONE.join('<br>');
        }

        st.showModal('inventory', {
            message, buttons,

            beforeClose: () => {
                typeof afterSelect === 'function' && afterSelect();
            }
        });

    } else {
        st.debug('Inventory doesn’t exist or not specified', 'warning');
    }
    /* jshint ignore:end */
};



/**
    Shows the current story’s map in a modal.
*/

st.showMap = function () {
    st.debug('showMap()');
    
    const STORY = st.Env.current;
    var map = st[STORY].map;

    if (map) {
        typeof map === 'boolean' && (map = 'map.jpg');

        let $header = $('#header'),
            content = `<div id="map-${STORY}" class="map" style="background-image: url(${st.Env.currentPath}images/${map})"></div>`,

            beforeClose = () => {
                st.Cache.map.panzoom('destroy');
                $('#close-icon').remove();

                //DEBUG_BEGIN
                if (st.Cache.contextIcon.attr('class').includes('fa-')) {
                    st.Cache.contextIcon.removeClass('hide');
                }
                //DEBUG_END
            },

            afterClose = () => {
                $header.removeClass('on-top');
            };

        // Store modal cleanup functions for execution when closeModal runs
        st.Env.beforeClose.push(beforeClose);
        st.Env.afterClose.push(afterClose);

        $header.addClass('on-top');

        st.showModal('map', {
            content,
            beforeClose,
            afterClose,
            center: false,

            beforeShow: () => {
                var map = document.getElementById('modal-map').firstChild,
                    mapGestures = new Hammer.Manager(map);

                st.Cache.map = $(map);

                // Create close-icon only if it doesn’t already exist
                if (!$('#close-icon').length) {
                    $header.append('<div id="close-icon" class="icon fa fa-times" title="Close" role="button"></div>')
                        .find('#close-icon').one('click', () => {
                            st.vibrate();
                            //beforeClose();
                            //st.closeModal(undefined, afterClose);
                            st.closeModal();
                        });
                }

                st.Cache.map.panzoom({
                    duration: 150,
                    increment: 0.4,
                    maxScale: 3,
                    minScale: 0.5

                    // Default panzoom properties
                    //contain: false,
                    //cursor: 'move',
                    //disablePan: false,
                    //disableZoom: false,
                    //easing: 'ease-in-out',
                    //eventNamespace: '.panzoom',
                    //rangeStep: 0.05,
                    //startTransform: undefined,
                    //transition: true,

                    // Elements on which to bind zoom controls
                    //$reset: $(),
                    //$zoomIn: $(),
                    //$zoomOut: $(),
                    //$zoomRange: $(),

                    // Equivalent to $elem.on('panzoomstart', () => ...
                    //onStart: undefined,
                    //onChange: undefined,
                    //onZoom: undefined,
                    //onPan: undefined,
                    //onEnd: undefined,
                    //onReset: undefined

                }).on('click.modal', event => {
                    event.stopPropagation();
                });

                // Recognize doubletap event on map
                mapGestures.add(new Hammer.Tap({
                    event: 'doubletap',
                    taps: 2,
                    threshold: 10,
                    posThreshold: 20
                }));

                // Zoom in/out when map is double-tapped
                mapGestures.on('doubletap', (event) => {
                    var $map = st.Cache.map;

                    st.debug('doubletap on map', 'event');
                    //event.stopPropagation();

                    if (typeof $map.isZoomed === 'undefined') {
                        $map.isZoomed = false;
                    }

                    $map.panzoom('zoom', $map.isZoomed ? 1 : 3);
                    $map.isZoomed = !$map.isZoomed;
                });

                st.Cache.contextIcon.addClass('hide');  //DEBUG
            }
        });

    } else {
        st.debug(st.Env.current + ' doesn’t have a map to show', 'warning');
    } 
};