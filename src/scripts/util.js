'use strict';


/**
 *  Shim of the ES7 Array.prototype.includes method.
 */

Array.prototype.includes || (Array.prototype.includes = function (searchElement, fromIndex) {
    return Array.prototype.indexOf.call(this, searchElement, fromIndex) !== -1;
});



/**
 *  Shim of the ES6 String.prototype.includes method.
 */

String.prototype.includes || (String.prototype.includes = function (searchString, position) {
    return String.prototype.indexOf.call(this, searchString, position) !== -1;
});



/**
 *  Capitializes the first letter of a string.
 *  @param {string} string
 *  @return {string} Capitialized string
 */

st.capitalize = function (string) {
    st.debug(`capitalize(${string})`);
    
    return string.charAt(0).toUpperCase() + string.slice(1);
};



/**
 *  Determines the height of the document.
 *  @return {number} Document height in px
 */
/*
st.docHeight = function () {
    //st.debug('docHeight()', 'function', true);
    
    return Math.max(
        document.documentElement.scrollHeight,
        document.documentElement.offsetHeight,
        document.documentElement.clientHeight,
        document.body.scrollHeight,
        document.body.offsetHeight,
        document.body.clientHeight,
        window.innerHeight
    );
};
*/



/**
 *  Escapes certain characters in an HTML string.
 *  @param {string} html
 *  @return {string} Escaped HTML
 */

st.escapeHtml = function (html) {
    st.debug('escapeHtml()');

    const ENTITY_MAP = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#39;',
        '/': '&#x2F;'
    };

    return String(html).replace(/[&<>"'\/]/g, match => ENTITY_MAP[match]);
};



/**
 *  Removes an item from an array.
 *  @param {*} item
 *  @param {Array} array
 */

st.removeFromArray = function (item, array) {
    st.debug(`removeFromArray(${item})`, 'function', true);
    
    Array.isArray(array) && array.splice(array.indexOf(item), 1);
};



/**
 *  Reverses the characters in a string.
 *  @param {string} string
 *  @return {string} Reversed string
 */

st.reverse = function (string) {
    st.debug(`reverse(${string})`);
    
    return string.split('').reverse().join('');
};



/**
 *  Picks a random integer within range of min and max.
 *  @param {number} [min=0] - Integer specifying bottom of range
 *  @param {number} [max=9] - Integer specifying top of range
 *  @return {number} Random integer
 */

st.rndInt = function (min = 0, max = 9) {
    st.debug(`rndInt(${min}, ${max})`, 'function', true);
    
    return Math.floor(Math.random() * (max - min + 1) + min);
};



/**
 *  Picks a random item from an array or collection of arguments.
 *  If the first arg is an array then subsequent args are ignored.
 *  @param {Array|...*} items - Multiple items to pick from
 *  @return {*} Random item
 */

st.rndItem = function (items) {
    st.debug(`rndItem(${items})`, 'function', true);

    var item;

    if (Array.isArray(items)) {
        item = items[st.rndInt(0, items.length - 1)];

    } else {
        item = arguments[st.rndInt(0, arguments.length - 1)];
    }

    return item;
};



/**
 *  Constructs a new Timer or manipulates an existing Timer.
 *  A new Timer must be started with the 'start' method.
 *  @constructor {Object} Timer
 *  @param {number} delay - Milliseconds to delay
 *  @param {function} callback - Called after delay
 */

st.Timer = function (delay, callback) {
    st.debug(`Timer(${delay})`, 'function', true);
    
    var id, started, running,
        remaining = delay;
    
    this.start = function () {
        running = true;
        started = new Date();
        id = setTimeout(callback, remaining);
    };
    
    this.pause = function () {
        running = false;
        clearTimeout(id);
        remaining -= new Date() - started;
    };
    
    this.timeLeft = function () {
        if (running) {
            this.pause();
            this.start();
        }

        return remaining;
    };
    
    this.timePassed = function () {
        if (running) {
            this.pause();
            this.start();
        }
        
        return delay - remaining;
    };
    
    this.state = function () {
        return running;
    };
};



/**
 *  Determines which viewport (window) dimension is shorter.
 *  @return {string} innerWidth or innerheight
 */

st.vShorter = function () {
    st.debug('vShorter()');
    
    return window.innerWidth < window.innerHeight ? 'innerWidth' : 'innerHeight';
};



/**
 *  Determines px measurement of the shorter viewport (window) dimension.
 *  @return {number} Measurement in px
 */

st.vShorterPx = function () {
    st.debug('vShorterPx()');
    
    return window[st.vShorter()];
};



/**
 *  Determines the word in an element at the point coordinates.
 *  @param elem - Element containing word
 *  @param {number} x - Horizontal coordinate in element
 *  @param {number} y - Vertical coordinate in element
 *  @return {?string} Word at coordinates
 */

st.wordAtPoint = function (elem, x, y) {
    st.debug(`wordAtPoint(${elem}, ${x}, ${y})`);

    // Like Range.expand('word'), expands range to a single word
    function expandRange() {
        st.debug('expandRange()');

        const START_CHARS = [' ', '‘', '“'],
            END_CHARS = [' ', '.', ',', '?', '!', '–', '—', '’', '”'];

        var startOffset = 1,
            endOffset = 1,
            chars = range.toString();

        while (!START_CHARS.includes(chars.charAt(0))) {
            range.setStart(elem, pos - startOffset++);
            chars = range.toString();

            if (startOffset > 20) {
                st.debug('Can’t determine start of word', 'warning');
                break;
            }

            st.debug('chars: ' + chars, 'var', true);
        }

        while (!END_CHARS.includes(chars.charAt(chars.length - 1))) {
            range.setEnd(elem, pos + endOffset++);
            chars = range.toString();
            
            if (endOffset > 20) {
                st.debug('Can’t determine end of word', 'warning');
                break;
            }

            st.debug('chars: ' + chars, 'var', true);
        }

        range.setStart(elem, pos - startOffset + 2);
        range.setEnd(elem, pos + endOffset - 2);
    }


    var range, pos, endPos;

    if (elem.nodeType === elem.TEXT_NODE) {
        range = elem.ownerDocument.createRange();
        range.selectNodeContents(elem);

        pos = 0;
        endPos = range.endOffset;

        while (pos + 1 < endPos) {
            range.setStart(elem, pos);
            range.setEnd(elem, pos + 1);

            if (range.getBoundingClientRect().left <= x && range.getBoundingClientRect().right  >= x &&
                    range.getBoundingClientRect().top  <= y && range.getBoundingClientRect().bottom >= y) {
                
                // Range.expand() is deprecated
                try {
                    range.expand('word');

                } catch (e) {
                    expandRange();
                }

                let word = range.toString(),
                    lastChar = word.charAt(word.length - 1),
                    punctuation = ['.', ',', '?', '!', '–', '—', '’', '”'],
                    span = document.createElement('span');

                st.debug('word: ' + word, 'var', true);
                st.debug('lastChar: ' + lastChar, 'var', true);

                if (punctuation.includes(lastChar)) {
                    word = word.substring(0, word.length - 2);
                    range.setEnd(elem, range.endOffset - 1);
                }

                // This DOM manipulation stops the function being just a util
                // but it’s easier to do here as we already have the range
                span.id = 'word-' + word; 
                document.body.appendChild(span);

                range.surroundContents(span);
                range.detach();

                return word;
            }

            pos++;
        }

    } else {
        for (let i = 0; i < elem.childNodes.length; i++) {
            range = elem.childNodes[i].ownerDocument.createRange();
            range.selectNodeContents(elem.childNodes[i]);

            if (range.getBoundingClientRect().left <= x && range.getBoundingClientRect().right  >= x &&
                    range.getBoundingClientRect().top  <= y && range.getBoundingClientRect().bottom >= y) {
                
                range.detach();

                return st.wordAtPoint(elem.childNodes[i], x, y);

            } else {
                range.detach();
            }
        }
    }

    return null;
};
