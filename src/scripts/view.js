'use strict';


/**
 *  Manages all aspects of loading a view (that isn’t a story).
 *  @param {string} view - Name of view to load
 */

st.loadView = function (view) {
	st.debug(`loadView(${view})`);

    const LAST = st.Env.current,
        IS_INIT = (LAST === null) && st.Env.loading.includes('init'),
        TITLE = 'StoryTime',
        $FLOW = $('#flow');

    clearInterval(st.checkBottom.interval);
    st.Env.loading.push(view);

    st.debug('LAST: ' + LAST, 'var', true);
    st.debug('IS_INIT: ' + IS_INIT, 'var', true);

    if (!IS_INIT) {
        st.updateBadge('gears', 'svg');  //WEB

        // Hide menu (if showing) and adjust menu entries
        st.toggleMenu(true, () => {
            st.Cache.Menu.restart.addClass('hide');

            if (view === 'catalog' || view === 'prefs') {
                st.Cache.Menu[view].addClass('hide');
            }

            if ((LAST === 'catalog' || LAST === 'prefs') && LAST !== view) {
                st.Cache.Menu[LAST].removeClass('hide');
            }

            st.User.reading.length && st.Cache.Menu.cont.removeClass('hide');
        });

    } else {
        st.User.reading.length && st.Cache.Menu.cont.removeClass('hide');
    }

    st.Env.isSlider && st.removeSlider();
    $FLOW.empty();
	LAST && $('#style-' + LAST).remove();

    // Clear last story’s space
    st.Env.doClear && st.clearStory();

    // Extra processing if not coming from another view
    if (!st.VIEWS.includes(LAST)) {
        
        // Don’t need to do these on init
        if (!IS_INIT) {
            //clearInterval(st.checkBottom.interval);
            st.Env.isMascot && st.hideMascot();

            // Disable gestures on flow that are only used in story
            st.Gestures.flow.get('pan').set({ enable: false });
            st.Gestures.flow.get('press').set({ enable: false });

            if (st.Env.hasTouch) {
                st.Cache.contextIcon.addClass('hide');  //DEBUG
                $FLOW.off('touchmove.story touchend.story touchcancel.story');
            }
            
            $FLOW.removeClass('story ' + st.Prefs.text);

            // Remove elements specific to last story
            $('.custom').remove();
        }

        st.Cache.head.find('.prefetch').remove();  //WEB
        
        // Update header badge and title
        st.updateBadge('catalog');  //APP
        st.Cache.title.html(TITLE);
        st.Cache.badge.removeAttr('title');

        st.initSky(undefined, !IS_INIT);
        st.Cache.sky.removeClass('hide');
    }

    document.title = `${TITLE} ${st.capitalize(view)}`;  //WEB
    history.state === view || history[IS_INIT ? 'replaceState' : 'pushState'](view, TITLE, `/${view}/`);  //WEB

    // Update environment
    st.Env.isStory = false;
    st.Env.current = view;
    st.Env.currentPath = '/android_asset/www/';  //APP
    st.Env.currentPath = '/';  //WEB

    // Get HTML file containing view’s elements and add to flow
    $.get(`/views/${view}.html`, data => {
        const FILLER_TOP = '<div id="filler-top" class="filler" style="height: 50px; visibility: hidden"></div>',
            FILLER_BOTTOM = '<div id="filler-bottom" class="filler" style="height: 50px; visibility: hidden"></div>';

        data = FILLER_TOP + data;

        $FLOW.append(data)
            .append(FILLER_BOTTOM)
            .find('script').remove();

        // Add queued messages to flow
        $.each(st.Env.messages, (index, def) => {
            st.showMessage(def);
        });
        
        st['resize' + view] && st['resize' + view]();
        st.Env.isView = true;

        if (IS_INIT) {
            st.Init.app.resolve();

        } else {
            st.updateBadge('catalog');  //WEB
        }

        st.removeFromArray(view, st.Env.loading);

    }).fail(() => {
        st.handleError(st.Msgs.Errors.file, view);
    });
};



/**
 *  Conditionally loads a view according to validity and app state.
 *  @param {string} view - Name of view to try
 *  @param {boolean} [playClick] - Play click sound before loading view
 */

st.tryView = function (view, playClick = false) {
    st.debug(`tryView(${view})`);

    if (st.Env.loading.length) {
        st.invalidAction();

    } else if (st.Env.current === view) {
        st.playSound('invalid');

    } else if (st.VIEWS.includes(view)) {
        playClick && st.playSound('click');
        st.loadView(view);

    } else {
        st.handleError('Could not load view', view);
    }
};
