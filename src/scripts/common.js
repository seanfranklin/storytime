'use strict';


/**
    Manages the process of buying credits with Google In-app Billing.
    @param {number} credits - positive integer denoting credits to buy
*/

st.buyCredits = function (credits) {
    st.debug(`buyCredits(${credits})`);
    
    // If credit count is displayed on a catalog glass button then update it
    function updateGlassButton() {
        var $glassButton = $('.glass:displayed .multi span:last');

        if ($glassButton.html().charAt(0) === '(') {
            $glassButton.html(`(${st.User.credits} left)`);
        }
    }

    var $options = $('.modal .option'),
        $message = $('.modal-message');
    
    // Cannot transact if there is no Internet connection
    if (!st.Env.connection) {
        st.debug(st.Msgs.Warnings.connection, 'warning');
        $message.html(st.Msgs.Modal.c01);
    
    // If have connection and using Android then do inappbilling
    } else if (st.Env.platform === 'Android') {

        // Send request to Google to buy specified number of credits.
        inappbilling.buy(() => {
            st.debug(`inappbilling: bought ${credits} credits`, 'info');

            // Consume and provision immediately in success callback
            inappbilling.consumePurchase(() => {
                st.User.credits += credits;
                st.storeData('User', true);
                
                $options.css('display', 'none');
                $message.html(`Transaction successful!<br>You have ${st.User.credits} credits.<br>Thanks for your support.`);
                
                st.Env.current === 'catalog' && updateGlassButton();

                st.debug('inappbilling: consumed credits', 'info');
                
            }, () => {
                st.handleError(credits + ' credits bought but not consumed');
                
            }, credits + '_credits');  // Item to consume
            
        }, () => {
            $message.html(st.Msgs.Modal.c02);
            
        }, credits + '_credits');      // Item to buy
    
    // If have connection but not on Android then simulate
    } else {
        st.User.credits += credits;
        st.storeData('User', true);
        
        $options.css('display', 'none');
        $message.html(st.Msgs.Modal.c03);

        st.Env.current === 'catalog' && updateGlassButton();
    }
};



/**
    Manages the process of buying a story with Google In-app Billing.
    @param {string} story - Short name of story to buy
*/

st.buyStory = function (story) {
    st.debug(`buyStory(${story})`);
    
    // Cannot transact if there is no Internet connection
    if (!st.Env.connection) {
        st.debug(st.Msgs.Warnings.connection, 'warning');

        st.showModal('purchase', {
            message: st.Msgs.Modal.c01
        });
    
    // If have connection and using Android then do IAB
    } else if (st.Env.platform === 'Android') {
        st.showModal('purchase', {
            content: '<div class="modal-message"></div>'
        });
        
        inappbilling.buy(() => {
            st.User.purchases.push(story);
            st.storeData('User', true);
            
            $(document).trigger('click.glass');
            
            $('.modal-message').html(st.Msgs.Modal.c05);

            if (st.Env.current === 'catalog') {
                $('#catalog-' + story).off('click')
                    .on('click', () => {
                        st.loadStory(story);
                    }).end()

                    .find('.catalog-status')
                        .html(st.storyStatus(story));
            }
            
        }, () => {
            st.showModal('purchase', {
                message: st.Msgs.Modal.c02
            });
            
        }, story.toLowerCase());
    
    // If have connection but not on Android then simulate
    } else {
        st.User.purchases.push(story);
        st.storeData('User', true);
        
        if (st.Env.current === 'catalog') {
            $('#catalog-' + story).off('click')
                .on('click', () => {
                    st.loadStory(story);
                }).end()

                .find('.catalog-status')
                    .html(st.storyStatus(story));
        }
        
        st.showModal('purchase', {
            message: st.Msgs.Modal.c03
        });

        $(document).trigger('click.glass');
    }
};



/**
    Determines if scroll is at bottom of document and sets 'pan' listener.
*/

st.checkBottom = function () {
    //st.debug('checkBottom()', 'function', true);

    if ((window.scrollY || document.documentElement.scrollTop) + window.innerHeight >= (document.documentElement.scrollHeight || document.body.scrollHeight)) {
        if (!st.Env.isBottom) {
            st.debug('bottom of document', 'event');
            st.Env.isBottom = true;
        }

        // If at bottom then check if user is reading story without hindrance
        if (st.Env.isStory && !st.Env.isMascot && !st.Env.modal && st.Env.isScroll && !st.Env.loading.length) {
            st.User.seen.includes('mascot') || st.showMascot(undefined, () => {
                st.updateSpeech('If you need advice …');
                
                // User has now seen how to summon the mascot, even if
                // they dismiss mascot now before speech is complete
                st.User.seen.push('mascot');
                st.storeData('User');
            
                setTimeout(() => {
                    if (!st.Env.modal && st.Env.isMascot) {
                        st.updateSpeech('… drag up to summon me');

                        setTimeout(() => {
                            st.Env.isMascot && st.updateSpeech((st.Env.hasTouch ?
                                    'Tap' : 'Click') + ' me if I’m in the way');
                        }, 7000);
                    }
                }, 3000);
            });

            // If reading story then enable 'panup' gesture that shows mascot
            !st.Env.isTouchmove && st.Gestures.flow.get('pan').set({ enable: true });
        }

    // If scroll not at document bottom then deactivate 'panup' gesture
    } else {
        st.Env.isBottom && st.Gestures.flow.get('pan').set({ enable: false });
        st.Env.isBottom = false;
    }
};



/**
    Closes the app or prompts the user to close the tab/window.
*/

st.closeApp = function () {
    st.debug('closeApp()');
    
    if (st.Env.platform === 'Web') {

        // Installable web apps can be closed programmatically
        window.close();

        let saved = st.Env.hasStorage ? 'Your data is saved.' : 'No data has been saved.';
        
        st.showModal('close', {
            message: saved + '<br>You can close the tab/window.',
            speech: 'See you later!'
        });
        
    } else {
        navigator.app.exitApp();
    }
};



/**
    Counts number of characters and words in an element.
    @param {Object} [$elem] - Default is flow
    @return {number} Word count
*/

st.countWords = function ($elem = $('#flow')) {
    st.debug(`countWords(${$elem})`);
    
    var text = $elem.text(),
        words = text.replace(/[^\w ]/g, '').split(/\s+/).length;
    
    st.debug(`${$elem.attr('id')}: ${text.length} chars, ${words} words`, 'info');

    return words;
};



/**
    Creates an HTML string of selectable buttons (aka multi options).
    @param {Object[]} buttons - Array of objects defining buttons
    @param {boolean} bgPulse - Button backgrounds pulse on hover/focus
    @return {string} HTML defining buttons
*/

st.createButtons = function (buttons, bgPulse = true) {
    st.debug('createButtons(buttons)');
    
    const IS_ANIMAL_MODAL = (st.Env.modal === 'animal'),
        BACKGROUND_SIZE = IS_ANIMAL_MODAL ? 'contain' : 'cover';

    var options = '';

    /* jshint ignore:start */
    for (let i = 0; i < buttons.length; i++) {
        let { id = '', classes = '', click = '#', image, text, icon } = buttons[i],
            optionClasses = bgPulse ? 'bg-pulse option' : 'option',
            style = image ? `background: url(/images/${st.Env.modal}/${text}.${image}) center center no-repeat; background-size: ${BACKGROUND_SIZE}` : '';

        if (!IS_ANIMAL_MODAL || (IS_ANIMAL_MODAL && !image)) {
            optionClasses += ' multi';
        }

        options += `<div id="${id}" class="${optionClasses} ${classes}" role="button" onclick="${click}" style="${style}">`;
        
        if (icon) {
            options += `<span class="fa fa-${icon}"></span>`;
        }

        if (text) {
            options += `<span class="${image && IS_ANIMAL_MODAL ? 'button-text' : ''}">${buttons[i].text}</span>`;
        }
        
        options += '</div>';
    }
    /* jshint ignore:end */
    
    return options;
};



/**
    Handles error reporting. Must be specifically called from a function.
    @param {string} message - Description of error
    @param {string} [resource]
*/

st.handleError = function (message, resource) {
    st.debug(message + (resource ? ` (${resource})` : ''), 'error');
    
    var timestamp = moment.utc().format('YYYYMMDDhhmmss');

    st.playSound('error');  //DEBUG
    
    st.Errors[timestamp] = message + (resource ? ` (${resource})` : '');
    st.storeData('Errors');
    
    //DEBUG_BEGIN
    typeof st.showModal === 'function' && st.showModal('error', {
        message, speech: st.Msgs.Speech.curse
    });
    //DEBUG_END
    
    st.sendErrors();
};



st.invalidAction = function () {
    st.debug('invalidAction()');

    st.playSound('invalid');

    if (st.Env.loading.includes(st.Env.current)) {
        st.Cache.badge.addClass('bright').velocity('callout.pulse');

        setTimeout(() => {
            st.Cache.badge.removeClass('bright');
        }, 200);
    }
};



/**
    Loads a view or story and restores its state (if necessary).
    This is needed to load from a pathname or history state.
    @param {string} state - View or story that may have state
*/

//WEB_BEGIN
st.loadState = function (state = location.pathname.replace(/\//g, '').toLowerCase() || 'catalog') {
    st.debug(`loadState(${state})`);

    const IS_INIT = (st.Env.current === null) && st.Env.loading.includes('init');

    if (st.VIEWS.includes(state)) {
        if (IS_INIT) {
            st.loadView(state);

        } else {
            st.tryView(state);
        }

    } else {
        state = st.capitalize(state);

        if (Object.keys(st.Catalog).includes(state)) {
            if (!IS_INIT && st.Env.loading.length) {
                st.invalidAction();

            } else {
                st.loadStory(state);
            }

        } else {
            location.pathname = '/404.html';
        }
    }
};
//WEB_END



/**
    Navigates backwards in the user interface.
    Destination depends on current location.
*/

st.navBack = function () {
    st.debug('navBack()');

    //DEBUG_BEGIN
    if (!st.Cache.DebugMenu.icon.hasClass('hide')) {
        st.toggleDebug('hide');
        return;
    }
    //DEBUG_END
    
    if (st.Env.modal) {
        st.closeModal();
        
    } else if (st.Env.loading.length) {
        st.invalidAction();

    } else if (st.Env.current === 'profile' || st.Env.current === 'legal') {
        st.loadView('prefs');
        
    } else if (st.Env.current === 'catalog') {
        st.closeApp();
        
    } else {
        st.loadView('catalog');
    }
};



/**
    Pauses timers and animations.
    @param {string} sound
*/

st.pause = function () {
    st.debug('pause()');

    st.Env.isStory && clearInterval(st.checkBottom.interval);
    st.Env.isView && st.clearSky();  //st.pauseSky();
};



/**
    Plays a sound if sound is enabled.
    @param {string} sound
*/

st.playSound = function (sound) {
    st.debug(`playSound(${sound})`);
    
    st.Prefs.sound && st.Sounds && st.Sounds[sound] && st.Sounds[sound].play();
};



/**
    Open app in a popup window.
    @param {number} width
    @param {number} height
*/

//WEB_BEGIN
st.popupApp = function (width, height) {
    st.debug(`popupApp(${width}, ${height})`);

    height = height || window.outerHeight * 0.8;
    width = width || height * 3 / 4;

    const LEFT = (screen.width / 2) - (width / 2),
        TOP = (screen.height / 2) - (height / 2),
        WINDOW_FEATURES = `width=${width},height=${height},top=${TOP},left=${LEFT},centerscreen=1,chrome=1,location=0,menubar=0,personalbar=0,scrollbars=1,status=0,toolbar=0`,
        STORYTIME = window.open(location.href, 'StoryTime', WINDOW_FEATURES),
        message = STORYTIME ? 'Popup opened. You can close this tab/window.' : 'Popup could not open. Please check your browser settings.',
        speech = STORYTIME ? 'Happy reading!' : st.Msgs.speech.curse;

    st.showModal('close', {
        message, speech,

        beforeShow: () => {
            if (STORYTIME) {
                st.pauseSky();
                $('#modal-close').off('click');
                $(window).off('keydown');
                $('body').addClass('no-pointer');

                setTimeout(() => {
                    var menuPopup = STORYTIME.document.getElementById('menu-popup');
                    $(menuPopup).remove();
                }, 2000);
            }
        }
    });
};
//WEB_END



/**
    Manages resizing of elements and text.
*/

st.resize = function () {
    st.debug('resize()');

    st.Env.doResize = false;  //WEB
    st.resizeFont();
    st.Env.vmin && st.resizeVmin();
    st['resize' + st.Env.current] && st['resize' + st.Env.current]();
    st.Env.isStory && st.resizeFiller();

    if (typeof st.Env.modal === 'string') {
        let resizeModal = 'resize' + st.capitalize(st.Env.modal);
        st[resizeModal] && st[resizeModal]();
    }
    
    if (window.innerWidth < 320 && !$('#message-width').length) {
        st[(st.Env.isView ? 'show' : 'queue') + 'Message']('width');
    }
};



/**
    Changes font size of the body based on window width.
*/
 
st.resizeFont = function () {
    st.debug('resizeFont()');
    
    var size, width = window.innerWidth;
    
    if (width < 481) {
        size = width / 300;
        
    } else if (width < 769) {
        size = width / 312;

    } else if (width < 1025) {
        size = width / 325;
        
    } else {
        size = 3.15;
    }
    
    $('body').css('font-size', size + 'em');
};



/**
    Resizes certain elements, such as the mascot.
    Only needed for browsers that don’t support CSS vmin unit.
*/

st.resizeVmin = function () {
    st.debug('resizeVmin()');
    
    var dimension, yCoord;
    
    st.Env.vmin = st.vShorterPx() / 100;
    dimension = st.Env.vmin * 20 + 'px';
    yCoord = st.Env.isMascot ? '-' + dimension : '0';
    
    st.Cache.mascot.css({
        bottom: '-' + dimension,
        width: dimension,
        height: dimension,
        transform: `translateY(${yCoord})`
    });
};



/**
    Resumes (re-inits) timers and animations and updates data.
    @param {string} sound
*/

st.resume = function () {
    st.debug('resume()');

    st.Env.isView && st.initSky();  //st.resumeSky();

    if (st.Env.isStory) {
        st.checkBottom.interval = setInterval(st.checkBottom, 300);
    }

    // If sync pref is on and sync modal not open then try sync with server
    if (st.Prefs.sync && (!st.Env.modal || !st.Env.modal.includes('sync'))) {
        st.toggleSync(true);
    }
};



/**
    Toggles display of the menu.
    @param {boolean} [hide] - Only perform hide action
    @param {function} [callback] - Function to run when toggle is complete
*/

st.toggleMenu = function (hide, callback) {
	st.debug(`toggleMenu(${hide})`);
    
    function complete() {
        typeof callback === 'function' && callback();
    }

    if (st.Cache.menu.css('display') === 'none') {
        if (!hide) {
            st.Cache.Menu.icon.addClass('bottom-straight');
            
            setTimeout(() => {
                st.Cache.menu.velocity('slideDown', {
                    duration: 200,
                    complete
                });
            }, 100);

        } else {
            complete();
        }
        
    } else {
        st.Cache.menu.velocity('slideUp', {
            duration: 200,

            complete: () => {
                st.Cache.Menu.icon.removeClass('bottom-straight');
                complete();
            }
        });
        
        /*
        setTimeout(() => {
            st.Cache.Menu.icon.removeClass('bottom-straight');
        }, 200);
        */
    }
};



/**
    Updates the badge image.
    @param {string} [image] - Default is current story/view
*/

st.updateBadge = function (image = st.Env.current, ext = 'png') {
    st.debug(`updateBadge(${image}, ${ext})`);

    st.Cache.badge.css('background-image', `url(/images/${image}.${ext}), radial-gradient(#cc9, #bb7)`);
};



/**
    Manages the process of using a credit to read a story.
    Deducts a credit and loads the story or opens the credits modal.
    @param {string} [story] - Default is current story
*/

st.useCredit = function (story) {
    st.debug(`useCredit(${story})`);
    
    if (!story) {
        if (st.Env.current === 'catalog') {
            story = $('#flow').find('.glass:displayed').attr('id').replace('glass-', '');
            
        } else {
            story = st.Env.current;
        }
    }
    
    if (st.User.credits > 0) {
        st.Env.doDeduct = true;
        st.storeData('User', true);
        st.restartStory(story, true);
        
    } else {
        st.showModal('credits', {
            beforeShow: () => {
                $('#credits').html(st.User.credits);
            }
        });
    }
};



/**
    Enables or disables window scrolling by the user.
    @param {boolean} scroll - Indicates enable or disable
*/

st.userScroll = function (scroll) {
    st.debug(`userScroll(${scroll})`);
    
    var events = 'mousewheel.noscroll touchmove.noscroll';
    
    if (scroll) {
        $(window).off(events);
        
    } else {
        $(window).on(events, event => {
            event.preventDefault();
        });
    }
    
    st.Env.isScroll = scroll;
};



/**
    Vibrates the device if the platform allows it.
    @param {number} [ms=20] - Milliseconds to vibrate
*/

st.vibrate = function (ms = 20) {
    st.debug(`vibrate(${ms})`, 'function', true);

    st.Env.hasVibration && navigator.vibrate(ms);
};
