'use strict';


/**
 *  Adds glass layer to an element as described by passed object.
 *  @param {Object} $elem - jQuery DOM object to receive glass layer
 *  @param {Object} Glass - Object describing glass message/buttons
 *  @param {string} [id] - Element id for glass layer
 */

st.addGlass = function ($elem, Glass, id = $elem.attr('id')) {
    st.debug(`addGlass(${id})`);

    $elem.prepend(`<div id="glass-${id}" class="glass"><div class="center"></div></div>`);
    const $CENTER = $elem.find('.center');

    Glass.message && $CENTER.append(`<div class="glass-message">${Glass.message}</div>`);
    Glass.buttons && $CENTER.append(`<div class="options">${st.createButtons(Glass.buttons)}</div>`);
};



/**
 *  Hides all glass and menus then shows glass layer of story.
 *  @param {string} story - Short name of story to show glass for
 */

st.showGlass = function (story) {
    st.debug(`showGlass(${story})`);

    const $FLOW = $('#flow'),
        $STORY = $('#catalog-' + story),
        $RIBBON = $STORY.find('.catalog-ribbon'),
        $GLASS = $('#glass-' + story),
        $IMAGE = $STORY.find('img').addClass('no-transform');

    st.playSound('glass');

    // Ribbon still clickable so disable pointer events
    $RIBBON.addClass('no-pointer');

    // Listen for clicks on document and hide glass on click
    $(document).add($GLASS).one('click.glass', event => {
        event.stopPropagation();

        $GLASS.velocity({ opacity: 0 }, {
            display: 'none',

            complete: () => {
                $RIBBON.removeClass('no-pointer');
                $IMAGE.removeClass('no-transform');
            }
        }, 200);
    });

    // Ensure listener above isn’t triggered by click on glass options
    $GLASS.find('.option').one('click.glass', event => {
        event.stopPropagation();
    });

    // Hide glass on all non-selected stories
    $FLOW.find('.glass').not($GLASS).velocity({ opacity: 0 }, { display: 'none'}, 200);

    // If menu is showing then hide it
    st.toggleMenu(true);

    $GLASS.velocity({ opacity: 1 }, { display: 'inherit' }, 200);
};
