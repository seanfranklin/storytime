'use strict';


/**
 *  Updates modal-review according to the word chosen by the user.
 *  @param {string} word
 */

st.chooseWord = function (word) {
    st.debug(`chooseWord(${word})`);

    const $MODAL = $('#modal-review'),
        $BUTTON = $MODAL.find('#word-' + word),
        DEF = $MODAL.find('#def').text();

    st.debug('word: ' + word, 'var', true);
    st.debug('def: ' + DEF, 'var', true);
    st.debug(`Defs.${st.Env.current}.${word}: ${st.Defs[st.Env.current][word]}`, 'var', true);

    if (st.Defs[st.Env.current][word] === DEF) {
        $BUTTON.addClass('option-deactivated no-pointer');

        const $OPTION = $MODAL.find('.option').not('.option-deactivated');

        if ($OPTION.length) {
            st.updateSpeech('Nice one!');
            st.nextDef();

        } else {
            st.showModal('review', {
                message: 'All definitions have been reviewed.',
                speech: 'Good job!'
            });
        }

    } else {
        $BUTTON.velocity({
            backgroundColor: '#ee003b',
            borderColor: '#c03'

        }, {
            complete: () => {
                $BUTTON.velocity('reverse');
            }
        });
        
        st.updateSpeech(st.rndInt(0, 1) ? 'Nope. Try again.' : 'That’s not right');
    }
};



/**
 *  Retrieves word definitions from Glosbe API.
 *  @param {string} word
 */

st.defineWord = function (word) {
    st.debug(`defineWord(${word})`);

    const $DEF = $('#modal-define #def');
    var defs = [];
    
    // Updates modal-define with the passed message
    function badResult(message) {
        if (st.Env.modal === 'define') {
            $DEF.html(message);
            
            setTimeout(() => {
                st.updateSpeech('Sorry about that');
            }, 400);
        }
    }

    // Returns an array of definitions extracted from passed object
    function extractDefs(object, propName = 'text') {
        st.debug(`extractDefs(object, ${propName})`, 'function', true);

        for (let prop in object) {
            if (object.hasOwnProperty(prop)) {
                if (typeof object[prop] === 'object') {
                    extractDefs(object[prop]);

                } else if (prop === propName) {
                    let reason, def = object[prop];
                    
                    if ((def.length > 100 && (reason = 'long')) ||
                        (defs.includes(def) && (reason = 'duplicate')) ||
                        (def.includes('&') && (reason = 'unclean')) ||
                        (def.includes('(archaic') && (reason = 'archaic')) ||
                        (def.includes('(obsolete') && (reason = 'obsolete')) ||
                        (def.includes('participle of') && (reason = 'confusing')) ||
                        (def.includes('indicative form of') && (reason = 'confusing'))) {

                        st.debug('Discarded ' + reason + ' definition', 'info', true);

                    } else {
                        st.debug('def: ' + def, 'var', true);
                        defs.push(def);
                    }
                }
            }
        }
    }

    $.ajax({
        url: 'https://glosbe.com/gapi/translate?from=eng&dest=eng&format=json&callback=null&phrase=' + word,
        type: 'GET',
        jsonp: 'callback',
        dataType: 'jsonp',

        success: (data) => {
            st.debug('Received data from glosbe.com', 'info');

            extractDefs(data);
            const LENGTH = defs.length;
            st.debug('defs: ' + defs, 'var', true);

            if (LENGTH) {
                let speech = `Found ${LENGTH} definition`;
                $DEF.text(defs[0]).data('defs', defs);

                $('#modal-define .center').append('<div class="options">' + st.createButtons([{
                    click: 'st.markDef()',
                    text: 'Mark for Review'

                }, {
                    classes: LENGTH === 1 ? 'hide' : '',
                    click: 'st.nextDef()',
                    text: 'Next Definition'

                }]) + '</div>').find('.option').on('click.modal', event => {
                    event.stopPropagation();
                    st.debug('click on modal option', 'event');
                });

                LENGTH > 1 && (speech += 's');
                st.updateSpeech(speech);
                
            } else {
                badResult('Could not find any definitions.');
            }
        },

        error: (jqXHR, status, error) => {
            const MESSAGES = {
                abort: 'aborted the lookup',
                error: 'returned an error',
                parseerror: 'returned bad results',
                timeout: 'isn’t responding'

            }, MESSAGE = status ? MESSAGES[status] : MESSAGES.timeout;

            st.debug('glosbe.com ' + MESSAGE + (error ? ': ' + error : ''));
            badResult(`The online dictionary service ${MESSAGE}.`);
        }
    });
};



/**
 *  Marks the current definition in modal-define for later review.
 */

st.markDef = function () {
    st.debug('markDef()');

    const $MODAL = $('#modal-define'),
        WORD = $MODAL.find('#word').text(),
        DEF = $MODAL.find('#def').text();

    st.debug('word: ' + WORD, 'var', true);
    st.debug('def: ' + DEF, 'var', true);

    if (WORD && DEF) {
        st[st.Env.current].defs = st[st.Env.current].defs || {};
        st[st.Env.current].defs[WORD] = DEF;

        $('#word-' + WORD).addClass('marked').click(() => {
            st.debug('click on word', 'event');
            st.showDef(WORD, true);
        });

        st.updateSpeech('Definition marked!');

        // Options are hidden because removing them closes the modal
        $MODAL.find('.options')
                .addClass('hide')
                .end()

            .find('.modal-message')
                .append('<br>You can review this definition at the end of the story.');
    }
};



/**
 *  Updates the modal to show the next definition.
 */

st.nextDef = function () {
    st.debug('nextDef()');

    const $DEF = $('#modal-' + st.Env.modal + ' #def'),
        DEFS = $DEF.data('defs'),
        CURRENT_DEF = $DEF.text(),
        CURRENT_INDEX = DEFS.indexOf(CURRENT_DEF),
        LAST_INDEX = DEFS.length - 1,
        NEXT_INDEX = CURRENT_DEF === DEFS[LAST_INDEX] ? 0 : CURRENT_INDEX + 1;

    st.debug('CURRENT_DEF: ' + CURRENT_DEF, 'var', true);
    st.debug('CURRENT_INDEX: ' + CURRENT_INDEX, 'var', true);
    st.debug('LAST_INDEX: ' + LAST_INDEX, 'var', true);
    st.debug('NEXT_INDEX: ' + NEXT_INDEX, 'var', true);

    $DEF.text(DEFS[NEXT_INDEX]);
};



/**
 *  Allows the user to review marked definitions via a modal.
 */

st.reviewDefs = function () {
    st.debug('reviewDefs()');

    const DEFS = st.Defs[st.Env.current];

    var buttons = [],
        data = [];

    for (let word in DEFS) {
        if (DEFS.hasOwnProperty(word) && typeof DEFS[word] === 'string') {
            data.push(DEFS[word]);

            buttons.push({
                id: 'word-' + word,
                text: word,
                click: `st.chooseWord('${word}')`
            });
        }
    }

    st.showModal('review', {
        message: `<span id="def">${data[0]}</span>`,
        speech: 'Choose the correct word',
        shuffle: true,
        buttons,

        beforeShow: () => {
            $('#modal-review #def').data('defs', data);
        }
    });
};



/**
 *  Shows word definitions in a modal.
 *  @param {string} word
 *  @param {boolean} marked - Show marked def if available
 */

st.showDef = function (word, marked, afterShow) {
    st.debug('showDef()');

    const DEFS = st[st.Env.current].defs,
        DEF = marked ? (DEFS && DEFS[word] ? DEFS[word] : '') : '',
        SPEECH = DEF ? 'This is your marked definition' : 'I’m looking that up';

    st.showModal('define', {
        message: `<div id="word">${word}</div><br><div id="def" role="definition">${DEF}</div>`,
        speech: SPEECH,
        afterShow
    });
};
