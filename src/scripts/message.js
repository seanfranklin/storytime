'use strict';


/**
 *  Closes the specified message in the view.
 *  @param {string} id - Message identifier
 *  @param {string} [direction] - Horizontal direction to animate
 *  @param {function} callback - Function to run after message closes
 */

st.closeMessage = function (id, direction, callback) {
    st.debug(`closeMessage(${id})`);

    const $MESSAGE = $('#message-' + id);

    function complete() {
        st.Gestures.Messages[id].off('swipe').destroy();
        delete st.Gestures.Messages[id];
        $MESSAGE.remove();

        // Remove message id or definition from queue
        $.each(st.Env.messages, (index, def) => {
            if (def === id || (def.id && def.id === id)) {
                st.Env.messages.splice(index, 1);
                return false;  // break
            }
        });

        typeof callback === 'function' && callback();
    }

    if (direction === 'left' || direction === 'right') {
        direction = st.capitalize(direction);

        $MESSAGE.velocity(`transition.slide${direction}BigOut`, {
            display: 'none',
            complete
        });

    } else {
        $MESSAGE.velocity({
            height: 0,
            marginBottom: 0,
            opacity: 0,
            paddingBottom: 0,
            paddingTop: 0
        }, {
            display: 'none',
            complete
        });
    }
};



/**
 *  Queues a message to be displayed on next view load.
 *  @param {string} id - Message id
 *  @param {string} [message] - Overrides default message
 */

st.queueMessage = function (id, message = st.Msgs.Warnings[id]) {
    st.debug(`st.queueMessage(${id}, ${message})`);

    st.Env.messages.push({ id, message });
};



/**
 *  Shows the warning message in the view.
 */

st.showMessage = function (Args) {
    st.debug('showMessage()');

    var isValid, id, type, message, animate, click, afterShow, afterClose, $message;

    // Displays the created message according to animate value
    function displayMessage() {
        st.debug('displayMessage()', 'function', true);

        if (animate) {
            let height = parseFloat($message.css('font-size')) * 3.532 + 'px',
                marginBottom = $message.css('margin-bottom');

            $message.velocity({
                height: 0,
                marginBottom: 0,
                opacity: 0,
                paddingBottom: 0,
                paddingTop: 0
            }, {
                duration: 0,

                complete: () => {
                    $message.removeClass('hide').velocity({
                        height, marginBottom,
                        opacity: 1,
                        paddingBottom: '10px',
                        paddingTop: '10px'
                    }, {
                        display: 'block',

                        complete: () => {
                            $message.css('height', 'inherit');
                            typeof afterShow === 'function' && afterShow();
                        }
                    });
                }
            });

        } else {
            $message.removeClass('hide');
        }
    }

    // Creates the message HTML and event listeners and shows it
    function createMessage() {
        st.debug('createMessage()', 'function', true);

        var className = `view message ${type} hide ${click ? ' pointer' : ''}`;
        
        $('#filler-top').after(
           `<div id="message-${id}" class="${className}">
                <div role="alert">${message}</div>
            </div>`
        );

        $message = $('#message-' + id);
        $message.append(`<div id="message-close" class="close fa fa-times" role="button"></div>`);  //WEB
        st.Gestures.Messages[id] = new Hammer(document.getElementById('message-' + id));

        // Respond to horizontal swipe gestures on message
        st.Gestures.Messages[id].on('swipe', event => {
            st.debug('swipe on message', 'event');
            const CLOSE_DIRECTION = (event.direction === Hammer.DIRECTION_LEFT) ? 'left' : 'right';
            st.closeMessage(id, CLOSE_DIRECTION, afterClose);

        // The default swipe velocity is not suitable for dismissing message
        }).get('swipe').set({ velocity: 0.1 });
        
        // Respond to tap gesture on message (if necessary)
        if (typeof click === 'function') {
            st.Gestures.Messages[id].on('tap', (event) => {
                //WEB_BEGIN
                if (event.target.id === 'message-close') {
                    st.debug('tap on message-close', 'event');
                    st.closeMessage(id, null, afterClose);

                } else {
                //WEB_END
                    st.debug('tap on message', 'event');
                    st.vibrate();
                    st.Gestures.Messages[id].off('tap');
                    click();
                }  //WEB
            });
        }
        //WEB_BEGIN
        // Even if no click function was passed, message-close needs a click listener
        else {
            let $messageClose = $message.find('#message-close');

            $messageClose.length && $messageClose.click(() => {
                st.debug('click on message-close', 'event');
                st.closeMessage(id, null, afterClose);
            });
        }
        //WEB_END
    }

    if (typeof Args === 'string') {
        id = Args;

        if (st.Msgs.Warnings[id]) {
            type = 'warning';
            message = st.Msgs.Warnings[id];

        } else {
            type = 'info';
            message = st.Msgs.Info[id];
        }

        isValid = true;

    } else if (typeof Args === 'object') {
        id = Args.id || st.rndInt(100, 999);
        type = Args.type || (st.Msgs.Warnings[id] ? 'warning' : 'info');
        message = Args.message || (type === 'warning' ? st.Msgs.Warnings[id] : st.Msgs.Info[id]);
        animate = Args.animate;
        click = Args.click;
        afterShow = Args.afterShow;
        afterClose = Args.afterClose;
        isValid = true;

    } else {
        st.handleError('Message definition is invalid.');
    }

    st.debug('id: ' + id, 'var', true);
    st.debug('type: ' + type, 'var', true);
    st.debug('message: ' + message, 'var', true);

    if (isValid) {
        createMessage();
        
        if (!st.Env.isSlider && st.Env.current === 'catalog') {
            displayMessage();
        }
    }
};
