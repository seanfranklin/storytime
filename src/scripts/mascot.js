'use strict';


/**
 *  Hides the mascot and its speech.
 */

st.hideMascot = function () {
    st.debug('hideMascot()');
    
    st.Cache.speech.addClass('hide');
    st.Cache.mascot.css('transform', 'translateY(0)');
    st.Env.isMascot = false;
};



/**
 *  Show mascot’s tip for the current story’s current decision.
 */

st.showDecisionTip = function (left) {
    st.debug(`showDecisionTip(${left})`);

    st.showMascot(left, () => {
        const STORY = st.Env.current;
        var speech;

        // Show explicit decision tip if one exists
        if (st.Msgs[STORY] && st[STORY] && st[STORY].decision && st.Msgs[STORY][st[STORY].decision]) {
            speech = st.Msgs[STORY][st[STORY].decision];

        // Story is finished
        } else if (st.Env.doClear) {
            speech = st.Msgs.Speech.enjoyed;

        // Story is at first decision
        } else if ($('.decision').length === 1) {
            speech = st.Msgs.Speech.first;

        // No explicit tip
        } else {
            speech = st.Msgs.Speech.sorry;
        }

        st.updateSpeech(speech);
    });
};



/**
 *  Shows the mascot at the bottom of the screen.
 *  @param {number|String} [left] - Horizontal position with optional units (default units is percent)
 *  @param {function} [callback]
 */

st.showMascot = function (left = st.rndInt(5, 75), callback = null) {
    st.debug(`showMascot(${left})`);

    const ON_LEFT = st.Cache.speech.hasClass('on-left'),
        TRANSLATE = st.Env.vmin ? '-' + (st.Env.vmin * 20) + 'px' : '-20vmin';

    var leftPercent;

    st.Env.isMascot = true;
    
    if (typeof left === 'number') {
        leftPercent = left;
        left = left + '%';

    } else {
        if (left.includes('px')) {
            leftPercent = window.innerWidth / 100 * parseInt(left);

        } else if (left.includes('%')) {
            leftPercent = parseInt(left);

        } else {
            st.handleError('Invalid mascot position', left);
        }
    }

    // Note that left has already been set if showing modal with mixed buttons
    st.Cache.mascot.css({ left });

    // Toggle side of mascot speech bubble is on (if necessary)
    if ((ON_LEFT && leftPercent < 40) || (!ON_LEFT && leftPercent > 40)) {
        st.Cache.speech.toggleClass('on-left');
    }

    setTimeout(() => {
        st.Cache.mascot.css('transform', `translateY(${TRANSLATE})`);
    }, st.Env.vmin ? 10 : 1);
    
    typeof callback === 'function' && setTimeout(callback, 300);
};



/**
 *  Updates the text in mascot’s speech bubble.
 *  @param {string} [speech]
 */

st.updateSpeech = function (speech) {
    st.debug(`updateSpeech(${speech})`);
    
    const BUBBLE = st.Cache.speech,
        HIDDEN = BUBBLE.hasClass('hide') ? true : false;
    
    if (speech && speech.length > 15) {
        BUBBLE.addClass('long');

    } else {
        BUBBLE.removeClass('long');
    }

    BUBBLE.html(speech);

    if (speech && HIDDEN) {
        if (st.Env.isMascot) {
            BUBBLE.removeClass('hide');
            
        } else {
            st.showMascot(undefined, () => {
                BUBBLE.removeClass('hide');
            });
        }
        
    } else if (!speech && !HIDDEN) {
        BUBBLE.addClass('hide');
    }
};
